# -*- coding: utf-8 -*-
"""Main app"""

from __future__ import unicode_literals

from django.apps import AppConfig


class RicoConfig(AppConfig):
    """Rico app"""

    name = 'rico'
