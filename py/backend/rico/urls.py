"""url routing"""

from django.conf.urls import url

from rico import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^add$', views.add, name='add'),
    url(r'^result$', views.result, name='result'),
    url(r'^reset$', views.error_reset, name='reset robot'),
    url(r'^target$', views.target_postion, name='Set position module target'),
    url(r'^devices$', views.devices, name='Find CAN devices'),
    url(r'^modes$', views.modes, name='Check C2000 mode'),
    url(r'^connect$', views.connect, name='Make a connection'),
    url(r'^disconnect$', views.disconnect, name='Make a disconnection'),
    url(r'^debug$', views.debug, name='Continue debug robot operation'),
    url(r'^cancel$', views.cancel, name='Cancel task from uuid'),
    url(r'^homes$', views.homes, name='Home all axes in position module mode'),
]
