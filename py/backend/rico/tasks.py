"""Celery tasks"""

from __future__ import absolute_import, unicode_literals
from celery import shared_task


@shared_task
def add(x, y):
    """Add function

    Args:
        x (integer): x value
        y (integer): y value

    Returns:
        integer: result
    """

    out = {'x': x, 'y': y, 'result': x+y}
    return out


@shared_task
def mul(x, y):
    """Multiply function

    Args:
        x (integer): x value
        y (integer): y value

    Returns:
        integer: result
    """

    out = {'x': x, 'y': y, 'result': x*y}
    return out


@shared_task
def xsum(numbers):
    """Summary function

    Args:
        numbers (list): list of integer

    Returns:
        dict:
            numbers (list): input
            sum (integer): result
    """

    out = {'numbers': numbers, 'sum': sum(numbers)}
    return out


@shared_task
def robot_start(args):
    """Start moving robot

    Args:
        args (dict):
            action (string): connect / disconnect
            east (integer): east port number
            west (integer): west port number
            options (dict): run mode, break points

    Returns:
        dict: request (dict): input
              response (boolean): break status
              status (string): one of ['success', 'break', 'alarm', 'running']
    """

    from xo288.cmd.api import robot_initial_start

    return robot_initial_start(args)


@shared_task
def robot_debug_continue(args):
    """Continue moving robot

    Args:
        args (dict):
            action (string): connect / disconnect
            east (integer): east port number
            west (integer): west port number
            options (dict): current_sequence, break points

    Returns:
        dict:
            request (dict): input
            response (boolean): break status
            error (string): error message
            status (string): one of ['success', 'break', 'alarm', 'running']
    """

    from xo288.cmd.api import robot_continue

    return robot_continue(args)


@shared_task
def robot_error_reset(robot_id, continue_mode):
    """Reset robot after error occurs

    Args:
        robot_id (integer): robot id
        east (integer): east port number
        west (integer): west port number
        options (dict): current_sequence, break points

    Returns:
        status (string): status code
        error (string): error message
        info (dict):
            can_id (int): CAN ID
            axis (int): motor axis
            home (bool): home returned
            busy (bool): motor is moving
            error_code (int): 0 -> no error
    """

    from xo288.cmd.error_reset import reset_robot

    return reset_robot(robot_id, continue_mode)


@shared_task
def position_module_error_reset(axis):
    """Reset position module after error occurs

    Args:
        axis (string): one of AXIS_CODE

    Returns:
        dict:
            status (string): status code
            error (string): error message
            response (dict):
                can_id (int): CAN ID
                data (list): raw data from can message
    """

    from xo288.cmd.error_reset import reset_pm
    from xo288.cmd.client import AXIS_CODE

    if axis not in AXIS_CODE:
        return {'status': 'error', 'error': 'Incorrect axis'}

    return reset_pm(axis)


@shared_task
def target_pm(axis, position):
    """Move position module to target position

    Args:
        target (integer): target position in pulse
        axis_code (string): one of AXIS_CODE
        can_channel (string): can interface

    Returns:
        dict:
            status (string): status code
            response (dict):
                can_id (integer): can id
                data (list): raw data from can message
    """

    from xo288.cmd.client import target_pm

    res = target_pm(target=position, axis_code=axis)
    data = []
    for byte in res.data:
        data.append(byte)
    obj = {
        'can_id': res.arbitration_id,
        'data': data,
    }
    return {'status': 'success', 'response': obj}


@shared_task
def devices():
    """Get CAN ID on bus

    Returns:
        dict:
            status (string): success
            id (list): can ids
    """

    from xo288.cmd.client import get_can_ids
    import can

    bus = can.interface.Bus(channel='can0', bustype='socketcan_ctypes')

    out = {'status': 'success', 'id':  get_can_ids(bus)}
    return out


@shared_task
def modes():
    """Get C2000 current mode

    Returns:
        dict:
            status (string): success
            c2000 (list): status
    """

    from xo288.cmd.client import get_c2000_mode
    import can

    bus = can.interface.Bus(channel='can0', bustype='socketcan_ctypes')
    out = {'status': 'success', 'c2000':  get_c2000_mode(bus)}

    return out


@shared_task
def homes():
    """Home all axes in position module mode

    Returns:
        dict:
            status (string): success
            start (datetime): start time
            end (datetime): end time
            runtime (float): time usage in second
    """

    from xo288.cmd.api import home

    return home()
