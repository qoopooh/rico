#!/usr/bin/env python
"""Test specific motor
"""


import sys
import readline
from time import sleep
from motor import Motor
from completer import MyCompleter
import package
import can

AXIS_CODE = ('rotate', 'arm_up', 'arm_extension', 'gripper', 'rollback', 'robot')

CAN_ID1 = 0x019
CAN_ID2 = 0x01A

#CAN_ID1 = 0x009
#CAN_ID2 = 0x00a

AXIS_TABLE = {
    'rotate': {'name':'Rotate', 'can_id':CAN_ID1, 'axis_no':0,
        'backlash':0, 'home_direction':'ccw', 'encoder_ratio':0,
        'speed': 5000, 'acceleration': 700},
    'arm_up': {'name':'Arm up/down', 'can_id':CAN_ID1, 'axis_no':1,
        'backlash':0, 'home_direction':'cw', 'encoder_ratio':2,
        'speed': 10000, 'acceleration': 500},
    'arm_extension': {'name':'Extend arm', 'can_id':CAN_ID1, 'axis_no':2,
        'backlash':0, 'home_direction':'ccw', 'encoder_ratio':4,
        'speed': 500, 'acceleration': 100},
    'rollback': {'name':'Rollback', 'can_id':CAN_ID2, 'axis_no':0,
        'backlash':0, 'home_direction':'ccw', 'encoder_ratio':2,
        'speed': 300, 'acceleration': 100},
    'gripper': {'name':'Gripper', 'can_id':CAN_ID2, 'axis_no':1,
        'backlash':0, 'home_direction':'ccw', 'encoder_ratio':34,
        'speed': 500, 'acceleration': 100}}

info = AXIS_TABLE['rotate']      # Select a motor according to AXIS_CODE
robot_mode = False

def get_can_ids(bus, robot_id=None):
    """Get CAN ID on bus

    Args:
        bus (Bus): CAN bus
        robot_id (int): One of can id of the robot

    Returns:
        list: can id
    """

    can_ids = []
    msg = package.broadcast_can_devices()
    bus.send(msg)

    if robot_id:
        robot_no = package.robot_no(robot_id)
    msg = bus.recv(3)
    while msg:
        can_id = msg.arbitration_id
        if can_id not in can_ids:
            if robot_id:
                if robot_no == package.robot_no(can_id):
                    can_ids.append(msg.arbitration_id)
                else:
                    print 'other c2000:', msg.arbitration_id, msg.data
            else:
                can_ids.append(msg.arbitration_id)
        msg = bus.recv(2)

    return can_ids


def get_c2000_mode(bus, robot_id=CAN_ID1):
    """Get C2000 current mode

    Args:
        bus (Bus): CAN bus
        robot_id (int): One of can id of the robot

    Returns:
        list: c2000 status
    """

    devices = []
    can_ids = get_can_ids(bus, robot_id)

    if len(can_ids) < 1:
        return devices
    print 'can_ids', can_ids

    motor = Motor(name='dummy', can_id=can_ids[-1], axis_no=-1)
    msgs = package.broadcast_c2000_act(motor)
    for msg in msgs:
        bus.send(msg)
        rcv = bus.recv(4)
        out = package.broadcast_c2000_act_response(rcv)
        devices.append(out)

    return devices

def __init_robot(bus, motor):

    devices = get_c2000_mode(bus)
    if devices[0]['mode'] == 'R':
        return

    # Switch mode to robot (standalone) mode
    msgs = package.broadcast_c2000_modes(motor)
    for msg in msgs:
        print 'SEND:\t\tRobot mode'
        print 'SEND:\t\t', msg
        bus.send(msg)
        print 'RECEIVED:\t', bus.recv()

def __init_pm(bus, motor):

    devices = get_c2000_mode(bus)
    if devices[0]['mode'] != 'P':
        # Switch mode to position module
        msgs = package.broadcast_c2000_modes(motor, mode='P')
        for msg in msgs:
            print 'SEND:\t\tPosition Module'
            print 'SEND:\t\t', msg
            bus.send(msg)
            print 'RECEIVED:\t', bus.recv()

    # Turn on motor
    msg = package.motor_activate(motor, turn_on=True)
    print 'SEND:\t\tTurning ON'
    print 'SEND:\t\t', msg
    bus.send(msg)
    msg = bus.recv()
    print 'RECEIVED:\t', msg
    print '\n\tTurn on {0}\n'.format(motor)

    # Set backlash
    backlash = motor.backlash
    if backlash > 0:
        msg = package.backlash_setting_request(motor, backlash)
        print 'SEND:\t\tBacklashing'
        print 'SEND:\t\t', msg
        bus.send(msg)
        msg = bus.recv()
        print 'RECEIVED:\t', msg
        print '\n\tSet backlash to', backlash, '\n'

    # Set home direction
    clockwise = motor.home_cw
    if clockwise:
        direction = 'clockwise'
    else:
        direction = 'counterclockwise'

    if motor.name == 'Gripper':
        msg = package.home_direction_request(motor, clockwise=clockwise, gripper=0x08)
    else:
        msg = package.home_direction_request(motor, clockwise=clockwise)
    print 'SEND:\t\tHome directioning'
    print 'SEND:\t\t', msg
    bus.send(msg)
    msg = bus.recv()
    print 'RECEIVED:\t', msg
    print '\n\tSet home direction of {0} to {1}\n'.format(motor, direction)

    # Set encoder ratio NOTE: has to do after homing
    encoder_ratio = motor.encoder_ratio
    if encoder_ratio > 0:
        msg = package.encoder_ratio_request(motor, encoder_ratio)
        print 'SEND:\t\tEncoding'
        print 'SEND:\t\t', msg
        bus.send(msg)
        msg = bus.recv()
        print 'RECEIVED:\t', msg
        print '\n\tSet encoder ratio to', encoder_ratio, '\n'

    msg = package.home_request(motor)
    print 'SEND:\t\tHoming'
    print 'SEND:\t\t', msg
    bus.send(msg)
    msg = bus.recv()
    print 'RECEIVED:\t', msg
    print '\n\t', motor.name, 'at home\n'


    # Get current position
    tx_msg = package.current_position_request(motor)
    print 'SEND:\t\t', tx_msg
    bus.send(tx_msg)
    rx_msg = bus.recv()
    print 'RECEIVED:\t', rx_msg
    print '\n\t', package.current_position_response(rx_msg), '\n'

def home_pm(axis_code, can_channel='can0'):
    """Home position module

    Args:
        axis_code (string): one of AXIS_CODE
        can_channel (string): can interface
    """

    bus = can.interface.Bus(channel=can_channel, bustype='socketcan_ctypes')

    info = AXIS_TABLE[axis_code]
    motor = Motor(name=info['name'],
                    can_id=info['can_id'],
                    axis_no=info['axis_no'],
                    backlash=info['backlash'],
                    encoder_ratio=info['encoder_ratio'],
                    home_cw=info['home_direction'] == 'cw')

    #print 'home_pm', axis_code, motor

    __init_pm(bus, motor)


def target_pm(target, axis_code='arm_extension', can_channel='can0'):
    """Move position module to target position

    Args:
        target (integer): target position in pulse
        axis_code (string): one of AXIS_CODE
        can_channel (string): can interface

    Returns:
        Message: can message
    """

    bus = can.interface.Bus(channel=can_channel, bustype='socketcan_ctypes')

    info = AXIS_TABLE[axis_code]
    motor = Motor(name=info['name'],
                    can_id=info['can_id'],
                    axis_no=info['axis_no'],
                    backlash=info['backlash'],
                    encoder_ratio=info['encoder_ratio'],
                    home_cw=info['home_direction'] == 'cw')


    __init_pm(bus, motor)

    tx_msg = package.positioning_request(motor,
                                              control_medthod='abs',
                                              pulse=target,
                                              speed=info['speed'],
                                              acceleration=info['acceleration'])
    print 'SEND:\t\t', tx_msg
    bus.send(tx_msg)
    rx_msg = bus.recv()
    print 'RECEIVED:\t', rx_msg
    print '\n\t', package.current_position_response(rx_msg), '\n'

    tx_msg = package.positioning_start_request(motor)
    print 'SEND:\t\t', tx_msg
    bus.send(tx_msg)
    rx_msg = bus.recv()
    print 'RECEIVED:\t', rx_msg

    tx_msg = package.current_position_request(motor)
    print 'SEND:\t\t', tx_msg
    bus.send(tx_msg)
    rx_msg = bus.recv()
    print 'RECEIVED:\t', rx_msg
    print '\n\t', package.current_position_response(rx_msg), '\n'

    return rx_msg


if __name__ == "__main__":


    bus = can.interface.Bus(channel='can0', bustype='socketcan_ctypes')

    if len(sys.argv) > 1 and sys.argv[1] in AXIS_CODE:
        if sys.argv[1] == 'robot':
            robot_mode = True
        else:
            info = AXIS_TABLE[sys.argv[1]]         # User input argument
    else:
        completer = MyCompleter(AXIS_CODE)
        readline.set_completer(completer.complete)
        readline.parse_and_bind('tab: complete')
        while True:
            msg = "Select motor {0}: ".format(AXIS_CODE)
            response = raw_input(msg)
            if response in AXIS_CODE:
                if response == 'robot':
                    robot_mode = True
                else:
                    info = AXIS_TABLE[response]     # User selected
                break
            elif len(response) < 1:
                continue
            else:
                print '  Wrong motor\n'

    __motor = Motor(name=info['name'],
                    can_id=info['can_id'],
                    axis_no=info['axis_no'],
                    backlash=info['backlash'],
                    encoder_ratio=info['encoder_ratio'],
                    home_cw=info['home_direction'] == 'cw')

    print __motor

    # Run continue of cancellation for robot mode
    if robot_mode:

        __init_robot(bus, __motor)

        instruction = '1. continue, 2. restart sequence, 3. reload all parameters, 4. error reset, 5. motor start (debug): '
        choices = ('1','2','3','4','5')
        completer = MyCompleter(choices)
        readline.set_completer(completer.complete)
        readline.parse_and_bind('tab: complete')
        while True:
            response = raw_input(instruction)
            if response in choices:
                break

        code = int(response)
        if code == 1:
            print 'SEND:\t\tRobot continue'
            msg = package.broadcast_cancel(__motor, code=code)
        elif code == 2:
            print 'SEND:\t\tRobot restart'
            msg = package.broadcast_cancel(__motor, code=code)
        elif code == 3:
            print 'SEND:\t\tRobot reload all parameters'
            msg = package.broadcast_cancel(__motor, code=code)
        elif code == 4:
            print 'SEND:\t\tRobot error reset'
            msg = package.broadcast_error_reset(__motor)
        elif code == 5:
            print 'SEND:\t\tRobot start'
            msg = package.broadcast_start(__motor)
        else:
            print 'Wrong input'
            sys.exit()

        print 'SEND:\t\t', msg
        bus.send(msg)
        sys.exit()


    __init_pm(bus, __motor)

    # clean up autocomplete
    completer = MyCompleter([])
    readline.set_completer(completer.complete)
    readline.parse_and_bind('tab: complete')

    while True:
        INSTRUCTION = "Set target position (pulse): "
        response = raw_input(INSTRUCTION).strip()
        if len(response) < 1:
            continue
        elif not response.isdigit() and response[0] != '-':
            break
        TARGET_POSITION = int(response)

        #tx_msg = package.positioning_request(__motor, 'abs', TARGET_POSITION)
        tx_msg = package.positioning_request(__motor,
                                                  control_medthod='abs',
                                                  pulse=TARGET_POSITION,
                                                  speed=info['speed'],
                                                  acceleration=info['acceleration'])
        print 'SEND:\t\t', tx_msg
        bus.send(tx_msg)
        rx_msg = bus.recv()
        print 'RECEIVED:\t', rx_msg
        print '\n\t', package.current_position_response(rx_msg), '\n'

        raw_input("Type enter to start:")

        #if __motor.name == 'Gripper':
            #grip = TARGET_POSITION > 0
            #tx_msg = package.gripper_start_request(__motor, grip)
        #else:
            #tx_msg = package.positioning_start_request(__motor)

        tx_msg = package.positioning_start_request(__motor)
        print 'SEND:\t\t', tx_msg
        bus.send(tx_msg)
        rx_msg = bus.recv()
        print 'RECEIVED:\t', rx_msg

        tx_msg = package.current_position_request(__motor)
        print 'SEND:\t\t', tx_msg
        bus.send(tx_msg)
        rx_msg = bus.recv()
        print 'RECEIVED:\t', rx_msg
        print '\n\t', package.current_position_response(rx_msg), '\n'
