#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Dummy wooey
"""

import argparse
import sys
from time import sleep

# Set argument parser
parser = argparse.ArgumentParser(description='Set dummy action')
parser.add_argument('-a', '--action', help='"connect" or "disconnect"',
        type=str, default='connect',
        choices=['connect', 'disconnect'], required=True)
parser.add_argument('-e', '--east', help='East port number',
        type=int, default=11, required=True) # cannot use range, it's too long.
parser.add_argument('-w', '--west', help='West port number',
        type=int, default=12, required=True)
parser.add_argument('-b', '--breaks',
        help='Type break sequence number to debug (e.g. 3,8,10)')

def param():
    """Set all parameters on the command execution
    """

    args = parser.parse_args()
    print(args)
    print

    options = {'run': False, 'stops':[]}

    if args.breaks == None:
        options['run'] = True
    else:
        if args.breaks.isdigit():
            options['stops'] = [args.breaks,]
        else:
            arr = args.breaks.split(',')
            for a in arr:
                if a.isdigit():
                    options['stops'].append(a)

    return args.action, args.east, args.west, options


if __name__ == "__main__":
    ACTION, SRC, DEST, OPTS = param()

    sleep(1)
    print('ACTION: ' + ACTION)
    sleep(1)
    print('EAST: ' + str(SRC))
    sleep(1)
    print('WEST: ' + str(DEST))
    sleep(1)
    print('OPTS: ' + str(OPTS))
    sleep(1)
    sys.exit(0)

