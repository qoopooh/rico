#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Alpha Test

This is main module to run host application on alpha phase
"""

import argparse
import sys
import os

path = os.path.expanduser('~') + '/git/rico/py/cmd'
print path
sys.path.append(path)

from alpha import *

# Set argument parser
parser = argparse.ArgumentParser(description='Set dummy action')
parser.add_argument('-a', '--action', help='"connect" or "disconnect"',
        type=str, default='connect',
        choices=['connect', 'disconnect', 'random'], required=True)
parser.add_argument('-e', '--east', help='East port number',
        type=int, default=1, required=True) # cannot use range, it's too long.
parser.add_argument('-w', '--west', help='West port number',
        type=int, default=2, required=True)
parser.add_argument('-b', '--breaks',
        help='Type break sequence number to debug (e.g. 3,8,10)')

def param():
    """Set all parameters on the command execution
    """

    args = parser.parse_args()
    if args.action not in ACTIONS:
        return { 'error': 'Please type one of actions: ["connect", "disconnect"]' }
    if args.east > MAX_PORT_NUMBER:
        return { 'error': 'Invalid east port: port number must be less than '
            + str(MAX_PORT_NUMBER) }
    elif args.east < 1:
        return { 'error': 'Invalid east port: port number must be greater than 0' }
    #elif args.east not in SMU_6:
        #return { 'error': 'Available port:' + str(SMU_6) }

    if args.west > MAX_PORT_NUMBER:
        return { 'error': 'Invalid west port: port number must be less than '
            + str(MAX_PORT_NUMBER) }
    elif args.west < 1:
        return { 'error': 'Invalid west port: port number must be greater than 0' }
    #elif args.west not in SMU_6:
        #return { 'error': 'Available port:' + str(SMU_6) }

    options = {'run': False, 'stops':[]}

    if args.breaks == None:
        options['run'] = True
    else:
        if args.breaks.isdigit():
            options['stops'] = [args.breaks,]
        else:
            arr = args.breaks.split(',')
            for a in arr:
                if a.isdigit():
                    options['stops'].append(a)

    out = { 'action': args.action,
        'east': args.east,
        'west': args.west,
        'options': options
    }
    return { 'result': out }


if __name__ == "__main__":
    HOSTNAME = socket.gethostname()
    print 'on', HOSTNAME

    resp = param()
    if 'error' in resp:
        print resp['error']
        sys.exit()
    r = resp['result']
    print r
    ACTION, SRC, DEST, OPTS = r['action'], r['east'], r['west'], r['options']

    if ACTION == 'random':
        while True:
            act = randint(0, 1)

            ## Full map
            #inp = randint(1, 144)
            #outp = randint(1, 144)

            ## 6 SMUs
            length = len(SMU_6) - 1
            inp = SMU_6[randint(0, length)]
            outp = SMU_6[randint(0, length)]

            send_sequence(action=ACTIONS[act], src=inp, dest=outp, opts={'run':True})

    elif ACTION == 'test':
        for test in TEST_PATTEN:
            start_sequence(action=test[0], src=test[1], dest=test[2], opts={'run':True})

    else:
        send_sequence(action=ACTION, src=SRC, dest=DEST, opts=OPTS)
