#!/usr/bin/env python
"""Translate can bus message
"""

import argparse
import sys
from motor import Motor
import package
import can

MAIN_CAN_ID = 0x019
ROBOT_ID = package.robot_no(MAIN_CAN_ID)

AXIS_CODE = ('rotate', 'arm_up', 'arm_extension', 'gripper', 'rollback')
AXIS_TABLE = {
    'rotate': {'name':'Rotate', 'can_id':MAIN_CAN_ID, 'axis_no':0, 'backlash':0},
    'arm_up': {'name':'Arm up/down', 'can_id':MAIN_CAN_ID, 'axis_no':1, 'backlash':0},
    'arm_extension': {'name':'Extend arm', 'can_id':MAIN_CAN_ID, 'axis_no':2, 'backlash':0},
    'rollback': {'name':'Rollback', 'can_id':MAIN_CAN_ID + 1, 'axis_no':0, 'backlash':0},
    'gripper': {'name':'Gripper', 'can_id':MAIN_CAN_ID + 1, 'axis_no':1, 'backlash':0}}

AXIS = AXIS_TABLE['rotate']      # Select a motor according to AXIS_CODE

broadcast_seq_table = []

# Set argument parser
parser = argparse.ArgumentParser(description='Read message on can bus')
parser.add_argument('-c', '--can0', help='listen on can0 interface', action='store_true')
parser.add_argument('-m', '--mask', help='bitmask can id')

def list_to_int(dat):
    value = 0
    for d in dat:
        value = d
        value <<= 8

    return value

def get_motor(can_id, axis_no):
    for code in AXIS_TABLE:
        a = AXIS_TABLE[code]
        if can_id == a['can_id'] and axis_no == a['axis_no']:
            return Motor(a['name'], a['can_id'], a['axis_no'])

def get_position_resp(dat):
    position_type = 'Target position'

    if dat[0] == 0x43:
        position_type = 'Current position'
    elif dat[0] == 0x45:
        position_type = 'Encoder position'

    pulse = package.twos_comp_reverse_tuple(dat[2:])

    return position_type, pulse

def get_positioning_req(dat):
    axis, method, pulse, speed, acc = 0, '?', 0, 0, 0
    if len(dat) < 8:
        return axis, method, pulse, speed, acc

    axis = dat[0] >> 4
    method = package.CONTROL_METHODS[dat[0] & 0x0F]
    pulse = package.twos_comp_reverse_tuple(dat[1:])
    speed = dat[4]<<8 | dat[5]
    acc = dat[6]<<8 | dat[7]


    return axis, method, pulse, speed, acc

def host_request(motor, cmd, data):
    if cmd == 'D':
        print '\t', motor, "=> Home Direction"

    elif cmd == 'G':
        if data[0]:
            if data[1]:
                print '\t', motor, "=> grip"
            else:
                print '\t', motor, "=> ungrip"
        else:
            print '\t', motor, "=> release"

    elif cmd == 'H':
        print '\t', motor, "=> Home"

    elif cmd == 'N':
        print '\t', motor, "=> encoder ratio", data[0]

    elif cmd == 'R':
        print '\t', motor, "=> Error Reset"

    elif cmd == 'S':
        if data[0]:
            print '\t', motor, "=> Motor ON"
        else:
            print '\t', motor, "=> Motor OFF"


def broadcast(motor, cmd, data, broadcast_seq_table):
    """Broadcast message
    """

    if cmd == '*':
        if len(data) == 8:
            breaks = []
            for j in range(0, 5):
                for i in range(0, 8):
                    if ((data[7 - j] >> i) & 0x01) == 0x01:
                        breaks.append((j * 8) + i + 1)
            print 'start with breaks:', breaks
        else:
            print 'start'

    elif cmd == 'C':
        no = data[3]
        if len(broadcast_seq_table) >= no:
            print 'sequence completed', no, broadcast_seq_table[no - 1]
        else:
            print 'sequence completed', no

    elif cmd == 'c':
        request_type = data[3]
        if request_type == 1:
            print 'Continue last break'
        elif request_type == 2:
            print 'Restart sequence'
        elif request_type == 3:
            print 'Reload all parameters'
        else:
            print 'Broadcast cancellation'

    elif cmd == 'E':
        print 'Error reset'

    elif cmd == 'S':
        no = data[3]
        can_id = (data[4] << 8) | data[5]
        print 'Sequence:', data
        print '\tno:', no, 'can id:', can_id
        if data[1] > 1:
            print '\tmulti axes no:', data[1]
        broadcast_seq_table.append((no, can_id))

    elif cmd == 'T': # start sequence table
        broadcast_seq_table = []

    elif cmd == 'I':
        print 'Alignment east'
        print '\tup:', (data[1] << 16) | (data[3] << 8) | data[4], 'rotate:', (data[5] << 16) | (data[6] << 8) | data[7]

    elif cmd == 'O':
        print 'Alignment west'
        print '\tup:', (data[1] << 16) | (data[3] << 8) | data[4], 'rotate:', (data[5] << 16) | (data[6] << 8) | data[7]

    elif cmd == 'P':
        print 'Motor parameter:', motor
        print '\tcan id:', (data[3] << 8) | data[4], 'axis:', data[1]

        p_home = data[5]
        h_order = p_home >> 4
        h_mechanical_stopper = p_home & 0x08
        if (p_home & 0x01) == 0:
            h_dir = 'ccw'
        else:
            h_dir = 'cw'
        print '\thome order:', h_order, 'dir:', h_dir, 'stopper:', h_mechanical_stopper

        p_encoder = data[6]
        e_ratio = p_encoder & 0x1F
        if (p_encoder & 0x80) == 0:
            e_overload = 'true'
        else:
            e_overload = '-'
        if (p_encoder & 0x40) == 0:
            e_torque_attempt = 'true'
        else:
            e_torque_attempt = '-'
        if (p_encoder & 0x20) == 0:
            e_torque_watch = 'true'
        else:
            e_torque_watch = '-'
        print '\tencoder:', e_ratio, 'overload:', e_overload, 'torque_attempt:', e_torque_attempt, 'torque_watch:', e_torque_watch

        p_backlash = data[7]
        b_val = p_backlash & 0x3F
        if (p_backlash & 0x80) == 0:
            b_reduction = 'true'
        else:
            b_reduction = '-'
        if (p_backlash & 0x40) == 0:
            b_over_position = 'true'
        else:
            b_over_position = '-'
        print '\tbacklash:', b_val, 'reduction:', b_reduction, 'over_position:', b_over_position



def ascii_data(data):
    """Return array of ascii
    """
    ch = []
    for d in data:
        ch.append(chr(d))
    return ch

def read_package(dat, broadcast_seq_table):
    motor = None
    data = []
    data_length = 0
    can_id = 0
    axis_no = 0
    i = 0

    # Find can id
    tmp = dat[i].strip().replace("L", "")
    if '<' in tmp:
        can_id = int(tmp[1:-1], 16)
        i += 1
    elif 'x' in tmp:
        can_id = int(tmp, 16)
        i += 1

    # Find data length
    tmp = dat[i].strip()
    if '[' in tmp:
        data_length = int(tmp[1:-1])
        i += 1

    int_val = False

    print 'dat[i:]', dat[i:],

    # find data in integer format
    for d in dat[i:]:
        has_alpha = False
        for ch in d:
            if ch.isalpha():
                print ch, 'isalpha'
                has_alpha = True
                break

        if has_alpha:
            break
        elif len(d) == 3:
            int_val = True
            print d, 'is int'
            break

    # add data to list
    if int_val:
        # e.g. 63, 1, 67, 0, 0, 0, 200, 18
        while i < len(dat):
            b = dat[i]
            i += 1
            data.append(int(b))
    else:
        # e.g. 3F 01 54 00 00 00 C8 12
        while i < len(dat):
            b = dat[i]
            i += 1
            if '<' in b or '[' in b:
                continue
            data.append(int(b, 16))

    print 'Raw:', can_id, data_length, data, ascii_data(data)

    if can_id > 0:
        if len(data) > 1:
            axis_no = int(data[1])

        motor = get_motor(can_id, axis_no)

    if len(data) < 1:
        return

    # Check type of data
    data_type = data[0]
    if len(data) > 2:
        cmd_type = chr(data[2])

    if data_type == ord('?'):
        print 'C2000 Response:', cmd_type, data
        if cmd_type == 'C' or cmd_type == 'T' or cmd_type == 'E':
            position_type, pulse = get_position_resp(data[2:])
            if motor != None:
                motor.position = pulse
                print "Motor:", motor
            print position_type + ':', pulse
        elif cmd_type == 'I': # information
            if motor:
                print "Motor:", motor
            else:
                print "Unknown Motor !!!"

            if data[3]: # home
                print "\tHome status: returned"
            else:
                print "\tHome status: never"

            if data[4]: # busy
                print "\tMotor is moving"

            if data[5]: # position completed
                print "\tMotor position is completed"

            err = data[6]
            if err == 0:
                pass
            elif err == 1:
                print "\tOver lower limit position"
            elif err == 2:
                print "\tOver upper limit position"
            elif err == 3:
                print "\tMotor driver error"
            elif err == 4:
                print "\tCommunication error"
            elif err == 5:
                print "\tEncoder error"
            elif err == 6:
                print "\tHome error"
            elif err == 7:
                print "\tOverload error"
            elif err == 8:
                print "\tTorque reach limit error"
            else:
                print "\tSome error happens:", err

        elif cmd_type == 'A':
            if len(data) == 8:
                print 'data', data
                axes = []
                for s in data[4:7]:
                    data = { 'home': s & 0x1, 'busy': (s >> 1) & 0x1, 'error': (s >> 2) & 0x1 }
                    axes.append(data)

                #resp = { 'mode': data[3], 'axes': axes,
                    #'state': data[7] & 0x1F,
                    #'substate': (data[7] >> 5) }


        elif data[2] == 0:
            if motor != None:
                print "Motor:", motor
            print 'No error'

    elif data_type == ord('|'):
        print 'Host request:', cmd_type
        host_request(motor, cmd_type, data[3:])
    elif data_type == ord('B'):
        print 'Broadcast:', cmd_type
        broadcast(motor, cmd_type, data, broadcast_seq_table)
    elif data_type in ( 0x00, 0x01, 0x02, 0x03,
            0x10, 0x11, 0x12, 0x13,
            0x20, 0x21, 0x22, 0x23 ):
        if motor != None:
            print "Motor:", motor
        pos_info = get_positioning_req(data)
        print 'Positioning:', pos_info
        idx = len(broadcast_seq_table) - 1
        if idx >= 0:
            info = broadcast_seq_table[idx]
            broadcast_seq_table[idx] = (info, pos_info)
    else:
        print 'Unknown type of data'
        if motor != None:
            print "Motor:", motor

if __name__ == "__main__":

    args = parser.parse_args()

    if args.can0:
        BUS = can.interface.Bus(channel='can0', bustype='socketcan_ctypes')
        print 'Listening on', BUS

        if args.mask:
            print 'args.mask', args.mask
            if '0x' in args.mask:
                m = int(args.mask, 16)
            else:
                m = int(args.mask)
        else:
            m = None

        while True:
            msg = BUS.recv()
            if m and (msg.arbitration_id & m) != m:
                continue

            payload = [hex(msg.arbitration_id), '[' + str(msg.dlc) + ']']
            for d in msg.data:
                payload.append(hex(d))
            print msg
            read_package(payload, broadcast_seq_table)
            print ''

            if msg.arbitration_id == 0 and \
                    (type(msg.dlc) != tuple or len(msg.dlc) == 0):
                print 'There is nothing'
                sys.exit()


    elif len(sys.argv) > 1:
        read_package(sys.argv[1:], broadcast_seq_table)
    else:
        while True:
            msg = "\nPut can message: "
            response = raw_input(msg)
            if ',' in response:
                arr = response.split(',')
            else:
                arr = response.split()
            if len(arr) < 1:
                continue
            read_package(arr, broadcast_seq_table)

