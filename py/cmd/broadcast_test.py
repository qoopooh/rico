"""Broadcast protocol to run C2000 as standalone mode
    python -m unittest -v broadcast_test
"""
import unittest
from broadcast import Standalone, AUTO_HOME, HAS_ALIGNMENT


class TestStandalone(unittest.TestCase):
    """python -m unittest -v broadcast_test
    """

    def setUp(self):
        self.s = Standalone('connect', 1, 2, {})

    def test_full(self):
        s = self.s
        self.assertEqual(s.state, 's_idle')

        s.preparation()
        self.assertEqual(s.state, 's_preparation')
        s.setup()
        self.assertEqual(s.state, 's_motor_parameter')

        s.info()

        while len(s.parameter_queue) > 0:
            self.assertEqual(s.state, 's_motor_parameter')
            s.send_motor_param()

        while len(s.sequence_queue) > 0:
            self.assertEqual(s.state, 's_1st_part_sequence')
            s.send_1st_part_sequence()
            self.assertEqual(s.state, 's_2nd_part_sequence')
            s.send_2nd_part_sequence()
        self.assertEqual(s.state, 's_validation')

        s.validated()
        if AUTO_HOME:
            s.homes_completed()
        else:
            self.assertEqual(s.state, 's_homing')
            s.send_home()

        self.assertEqual(s.state, 's_ready')

        s.send_start()
        self.assertEqual(s.state, 's_clients_work')
        s.read_sequence_completed()
        self.assertEqual(s.state, 's_clients_work')

        s.motor_alarm() # event trigger
        self.assertEqual(s.state, 's_clients_alarm')

        s.send_cancellation_continue()
        self.assertEqual(s.state, 's_clients_work')

        s.motor_break() # event trigger
        self.assertEqual(s.state, 's_clients_break')

        s.send_start()
        self.assertEqual(s.state, 's_clients_work')
        s.read_sequence_completed()
        self.assertEqual(s.state, 's_clients_work')

        s.sequence_completed()
        if HAS_ALIGNMENT:
            self.assertEqual(s.state, 's_operation_report')
            s.read_alignment()
            self.assertEqual(s.state, 's_operation_report')
            s.done()

        self.assertEqual(s.state, 's_done')

        s.clean_up()
        self.assertEqual(s.state, 's_idle')

if __name__ == '__main__':
    unittest.main()

