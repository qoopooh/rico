#!/usr/bin/env python
"""Test specific motor
"""


import sys
import logging
import readline
from motor import Motor
from completer import MyCompleter
import package
import can

from client import AXIS_CODE, AXIS_TABLE, CAN_ID1, CAN_ID2

ROBOT_CONTINUE_MODE = [ 'continue', # continue sequence
                    'restart',      # restart all sequences
                    'reload' ]      # reload all parameters

AXIS = AXIS_TABLE['rotate']      # Select a motor according to AXIS_CODE
robot_mode = False

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger('error_reset')

# create a file handler
handler = logging.FileHandler('error_reset.log')
handler.setLevel(logging.INFO)

# create a logging format
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)

# add the handlers to the logger
logger.addHandler(handler)

def reset_pm(axis_code='rotate', can_channel='can0'):
    """Reset position module after error occurs

    Args:
        axis_code (string): one of AXIS_CODE
        can_channel (string): can interface

    Returns:
        status (string): status code
        response (dict):
            can_id (int): CAN ID
            data (list): raw data from can message
    """

    BUS = can.interface.Bus(channel=can_channel, bustype='socketcan_ctypes')

    a = AXIS_TABLE[axis_code]
    m = Motor(name=a['name'],
                  can_id=a['can_id'],
                  axis_no=a['axis_no']
                 )

    req = package.error_reset_request(m)
    BUS.send(req)
    resp = BUS.recv()

    data = []
    for b in resp.data:
        data.append(b)
    obj = {
        'can_id': resp.arbitration_id,
        'data': data,
    }

    logger.info('reset_pm: %s', axis_code)

    return { 'status': 'success', 'response': obj }


def reset_robot(robot_id, continue_mode='continue', can_channel='can0'):
    """Reset robot after error occurs

    Args:
        robot_id (integer): robot number
        continue_mode (string): one of ROBOT_CONTINUE_MODE
        can_channel (string): can interface

    Returns:
        status (string): status code
        error (string): error message
        info (dict):
            can_id (int): CAN ID
            axis (int): motor axis
            home (bool): home returned
            busy (bool): motor is moving
            error_code (int): 0 -> no error
    """

    if continue_mode not in ROBOT_CONTINUE_MODE:
        return { 'status': 'error', 'error': 'Wrong mode' }

    BUS = can.interface.Bus(channel=can_channel, bustype='socketcan_ctypes')

    msg = package.broadcast_error_reset(robot_id)
    BUS.send(msg)
    resp = package.read_status(BUS.recv())

    while resp['status'] == 'error' or resp['info']['home'] == False:
        resp = package.read_status(BUS.recv())

    code = ROBOT_CONTINUE_MODE.index(continue_mode)
    msg = package.broadcast_cancel(robot_id, code=code)
    logger.info('reset_robot: robot:%d code:%d', robot_id, code)
    BUS.send(msg)

    return resp


if __name__ == "__main__":

    BUS = can.interface.Bus(channel='can0', bustype='socketcan_ctypes')

    axis_code = None
    if len(sys.argv) > 1 and sys.argv[1] in AXIS_CODE:
        if sys.argv[1] == 'robot':
            robot_mode = True
        else:
            axis_code = sys.argv[1]
    else:
        completer = MyCompleter(AXIS_CODE)
        readline.set_completer(completer.complete)
        readline.parse_and_bind('tab: complete')
        while True:
            msg = "Select motor {0}: ".format(AXIS_CODE)
            response = raw_input(msg)
            if response in AXIS_CODE:
                if response == 'robot':
                    robot_mode = True
                else:
                    axis_code = sys.argv[1]
                break
            elif len(response) < 1:
                logger.info('No input')
                continue
            else:
                logger.error('Wrong motor: %s', response)

    if not robot_mode:
        print reset_pm(axis_code)
        return


    instruction = '1. continue, 2. restart sequence, 3. reload all parameters: '
    choices = ('1','2','3',)
    completer = MyCompleter(choices)
    readline.set_completer(completer.complete)
    readline.parse_and_bind('tab: complete')
    while True:
        response = raw_input(instruction)
        if response in choices:
            break

    code = int(response)

    msg = package.broadcast_error_reset(robot_id)
    BUS.send(msg)
    resp = package.read_status(BUS.recv())
    while resp['status'] == 'error' or resp['info']['home'] == False:
        resp = package.read_status(BUS.recv())
    logger.info('error_reset_request: robot id %d', robot_id)

    msg = package.broadcast_cancel(robot_id, code=code)
    if code == 1:
        print 'SEND:\t\tRobot continue'
    elif code == 2:
        print 'SEND:\t\tRobot restart'
    else:
        print 'SEND:\t\tRobot reload all parameters'
    BUS.send(msg)
    logger.info('broadcast_cancel: robot:%d code:%d', robot_id, code)

