"""Broadcast protocol to run C2000 as standalone mode
"""

import can
import logging
import package
import socket
import sys
import copy
from transitions import Machine
from sequence_factory import SequenceFactory
from motor_factory import MotorFactory
from time import sleep

MAX_PORT_NUMBER     = 144
CAN_RECV_TIMEOUT    = 1
MAX_ROTATE_POSITION = 14000     # from csv/rotate_position.csv
MIN_ROTATE_POSITION = -14340
OVER_ROTATION_PULSE = 3000     # SMU clear pulse of moving direction
CONNECTOR_OFFSET    = 130       # offset between rollback and connector
ADAPTER_OFFSET      = 50        # offset between rollback and adapter
AUTO_HOME           = True      # A9 just transfer data and then trig start
HAS_ALIGNMENT       = True      # Get aligning adjustment values after completed process
ROLLBACK_ROT_MAP    = (4755, 7383, 10376)   # DEMO4 rollback rotation position
HOSTNAME            = socket.gethostname()
NO_BUS              = 'local' in HOSTNAME or \
                      HOSTNAME in ('L412', 'jessie', 'xmini')

# set logger
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger('broadcast')

# create a file handler
handler = logging.FileHandler('broadcast.log')
handler.setLevel(logging.INFO)

# create a logging format
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)

# add the handlers to the logger
logger.addHandler(handler)

class Standalone(object):

    states = ('s_idle',
            's_preparation',
            's_motor_parameter',
            's_1st_part_sequence',
            's_2nd_part_sequence',
            's_validation',
            's_homing',
            's_autohome',
            's_ready',
            's_clients_work',
            's_clients_break',
            's_clients_alarm',
            's_operation_report',
            's_done',
            )

    motors = (
              'rotate',
              'arm_up',
              'rollback',
              'arm_extension',
              'gripper',
             )

    # 1 c2000
    #motors = (
              #'rotate',
              #'arm_up',
              #'arm_extension',
             #)

    def __init__(self, action, src, dest, opts):

        self.action = action
        self.src = src
        self.dest = dest

        self.bus = can.interface.Bus(channel='can0',
                bustype='socketcan_ctypes')

        self.validation_counter = 0
        self.autohome_counter = 0
        self.alignment_counter = 0
        self.total_sequence = 0
        self.home_queue = []
        self.parameter_queue = []
        self.sequence_queue = []
        self.sequence_completed_set = []
        self.breakpoints = []
        self.debug_all = False
        if opts:
            if 'stops' in opts and len(opts['stops']) > 0:
                self.breakpoints = self.load_breakpoints(opts['stops'])
            elif 'run' in opts and not opts['run']:
                self.debug_all = True
            print 'breaks', self.breakpoints

        self.rotate_motor = None
        self.robot_no = None
        self.rcv_queue = []

        self.machine = Machine(model=self,
                states=Standalone.states,
                initial='s_idle')

        self.machine.add_transition(trigger='preparation',
                source='s_idle',
                dest='s_preparation')

        self.machine.add_transition('motor_parameter',
                's_preparation', 's_motor_parameter',
                before='send_robot_mode_request')

        self.machine.add_transition(trigger='motor_continue',
                source='s_motor_parameter',
                dest='s_clients_work')

        self.machine.add_transition('motor_parameter_completed',
                's_motor_parameter', 's_1st_part_sequence',
                before='send_begin_table')

        self.machine.add_transition('sequence_2nd',
                's_1st_part_sequence', 's_2nd_part_sequence')

        self.machine.add_transition('sequence_1st',
                's_2nd_part_sequence', 's_1st_part_sequence')

        self.machine.add_transition('sequence_table_completed',
                's_2nd_part_sequence', 's_validation',
                after='send_end_table')

        if AUTO_HOME:
            self.machine.add_transition('validated',
                    's_validation', 's_autohome')
            self.machine.add_transition('homes_completed',
                    's_autohome', 's_ready')
        else:
            self.machine.add_transition('validated',
                    's_validation', 's_homing')
            self.machine.add_transition('homes_completed',
                    's_homing', 's_ready')


        self.machine.add_transition('motor_start',
                's_ready', 's_clients_work')

        self.machine.add_transition('motor_break',
                's_clients_work', 's_clients_break')

        self.machine.add_transition('motor_alarm',
                's_clients_work', 's_clients_alarm')

        self.machine.add_transition('motor_start',
                's_clients_break', 's_clients_work')

        self.machine.add_transition('motor_cancellation_continue',
                's_clients_alarm', 's_clients_work')

        if HAS_ALIGNMENT:
            self.machine.add_transition('sequence_completed',
                    's_clients_work', 's_operation_report')
        else:
            self.machine.add_transition('sequence_completed',
                    's_clients_work', 's_done')

        self.machine.add_transition('done',
                's_operation_report', 's_done')

        self.machine.add_transition('idle',
                's_done', 's_idle')


    def info(self):
        print '\nstate', self.state
        print 'home_queue', len(self.home_queue), self.home_queue
        print 'parameter_queue', len(self.parameter_queue)
        print 'sequence_queue', len(self.sequence_queue), '\n'

    def load_breakpoints(self, stops):
        print 'load_breakpoints', type(stops), stops
        if isinstance(stops, unicode) or isinstance(stops, str):
            stops = stops.split(',')
        print 'stops is ' + str(type(stops)) + ': ' + str(stops)

        bpl = []
        for s in stops:
            if len(s) < 1:
                continue
            bpl.append(int(s))
        return bpl

    def setup(self):
        self.validation_counter = 0
        self.autohome_counter = 0
        self.alignment_counter = 0


        s_factory = SequenceFactory()
        m_factory = MotorFactory()
        self.rotate_motor = m_factory.create_motor('rotate')
        #self.rotate_motor.can_id = 0x000F
        self.robot_no = package.robot_no(self.rotate_motor.can_id)


        self.home_queue = []
        for code in Standalone.motors:
            m = m_factory.create_motor(code)
            self.home_queue.append(package.home_request(m))

        # Motor parameter queue
        pq = []
        home_order = len(Standalone.motors)
        for code in Standalone.motors:
            m = m_factory.create_motor(code)
            if AUTO_HOME:
                if m.name == 'Gripper':
                    pq.append(package.broadcast_motor_parameters(m,
                                home_order=home_order,
                                mechanical_stopper=True))
                else:
                    pq.append(package.broadcast_motor_parameters(m, home_order=home_order))
                home_order -= 1
            else:
                pq.append(package.broadcast_motor_parameters(m))

        # Sequence queue
        sq = []
        steps = s_factory.create_sequence(
                self.action, east=(self.src-1), west=(self.dest-1))
                # return (motor code, position index, execution pending)

        first_connecting_rotation = True
        first_disconnecting_rotation = True
        east_position = None
        i = 0

        for s in steps:
            i += 1
            m = m_factory.create_motor(code=s[0], index_position=s[1])
            __pending = s[2]
            #print 'ACTION:\t {0} {1}<=>{2} no.{3}: {4}'.format(
                    #self.action, self.src, self.dest, i, m)

            # Offset pulse from rollback position
            if m.name == 'Rotate':
                if self.action == 'connect':
                    if first_connecting_rotation:
                        first_connecting_rotation = False
                        east_position = copy.copy(m)
                        print 'east_position', east_position
                    else:
                        west_position = copy.copy(m)
                        print 'west_position', west_position

                        #_m = self.__over_output_movement(east_position, west_position)
                        #if _m != None:
                            #sq.insert(0, package.broadcast_sequence_1st_part(_m, i))
                            #sq.insert(0, package.broadcast_sequence_2nd_part(_m))
                            #i += 1

                #elif self.action == 'disconnect':
                    #if first_disconnecting_rotation:
                        #first_disconnecting_rotation = False
                    #else:
                        #m.position = ROLLBACK_ROT_MAP[s[1]]

            #__start_motor(BUS, m)
            sq.insert(0, package.broadcast_sequence_1st_part(m, i))
            sq.insert(0, package.broadcast_sequence_2nd_part(m))

        self.parameter_queue = pq
        self.sequence_queue = sq
        self.total_sequence = len(sq) / 2
        if self.debug_all:
            s = range(self.total_sequence)
            s.remove(0)
            s.append(self.total_sequence)
            self.breakpoints = s
            print 'Set debug mode', s

        self.info()
        self.motor_parameter()

        print 'Setup NO_BUS:', NO_BUS

    def send_robot_mode_request(self):
        devices = []
        msgs = package.broadcast_c2000_act(self.rotate_motor)
        for msg in msgs:
            self.bus_send(msg)
            rcv_msg = self.bus_read()
            while not rcv_msg:
                rcv_msg = self.bus_read()
            out = package.broadcast_c2000_act_response(rcv_msg)
            devices.append(out)

        if devices[0]['mode'] != 'R':
            msgs = package.broadcast_c2000_modes(self.rotate_motor)
            for msg in msgs:
                self.bus_send(msg)
                rcv_msg = self.bus_read()
                print 'Robot:', package.broadcast_robot_mode_response(rcv_msg)

    def send_motor_param(self):
        if len(self.parameter_queue) <= 0:
            return

        msg = self.parameter_queue.pop()
        self.bus_send(msg)
        print 'send_motor_param', msg
        if len(self.parameter_queue) <= 0:
            self.motor_parameter_completed()

    def send_begin_table(self):
        msg = package.broadcast_start_table(self.rotate_motor, self.total_sequence)
        self.bus_send(msg)
        print 'send_begin_table', msg

    def send_end_table(self):
        msg = package.broadcast_end_table(self.rotate_motor, self.total_sequence)
        self.bus_send(msg)
        print 'send_end_table', msg

    def send_1st_part_sequence(self):
        msg = self.sequence_queue.pop()
        self.bus_send(msg)
        print '1st', msg
        self.sequence_2nd()

    def send_2nd_part_sequence(self):
        msg = self.sequence_queue.pop()
        self.bus_send(msg)
        print '2nd', msg
        if len(self.sequence_queue) <= 0:
            self.sequence_table_completed()
        else:
            self.sequence_1st()

    def read_validation_result(self):
        if len(self.rcv_queue) < 1:
            return

        rcv_msg = self.rcv_queue.pop()
        if rcv_msg.data[0] != ord('B'):
            print "Not B 'V'", rcv_msg
            return

        if rcv_msg.data[2] != ord('V'):
            print "Not 'V'", rcv_msg
            return

        self.validation_counter += 1
        print 'read_validation_result', self.validation_counter

        if self.validation_counter > 1:
            self.validated()

    def read_autohome_result(self):
        if len(self.rcv_queue) < 1:
            return

        rcv_msg = self.rcv_queue.pop()
        if rcv_msg.data[0] != ord('?'):
            print "Not ? 'I'", rcv_msg
            return

        if rcv_msg.data[2] != ord('I'):
            print "Not 'I'", rcv_msg
            return

        if rcv_msg.data[3] != 1:
            print "Home is incompleted", rcv_msg
            return

        self.autohome_counter += 1
        print 'read_autohome_result', self.autohome_counter

        if self.autohome_counter >= len(self.motors):
            self.homes_completed()

    def send_home(self):
        msg = self.home_queue.pop()
        print 'homing', msg
        rcv_msg = self.bus_send(msg, resp=True)
        if NO_BUS:
            self.homes_completed()
            return

        while not rcv_msg or (rcv_msg.data[3] != 1) or (msg.arbitration_id != rcv_msg.arbitration_id):
            print 'Not home of', msg.arbitration_id, ':', rcv_msg
            rcv_msg = self.bus_read()
        print 'homed', rcv_msg
        if len(self.home_queue) <= 0:
            self.homes_completed()

    def send_start(self, breakpoints=[]):
        if len(breakpoints) < 1:
            breakpoints = self.breakpoints
        msg = package.broadcast_start(self.rotate_motor, breakpoints)
        self.bus_send(msg)
        print 'send_start', msg
        if self.state == 's_ready' or self.state == 's_clients_break':
            self.motor_start()

    def send_cancellation_continue(self, choice=1):
        """Send motor cancel
        """

        msg = package.broadcast_cancel(self.rotate_motor, choice)
        self.bus_send(msg)
        print 'send_cancellation_continue', msg
        self.motor_cancellation_continue()

    def read_sequence_completed(self):
        """Read sequence completed package

        Returns:
            sequence (int): current sequence number
        """

        if len(self.rcv_queue) < 1:
            return

        msg = self.rcv_queue.pop()
        if msg.data[2] == ord('I'):
            error_msg = None
            err = msg.data[6]
            if err == 0:
                pass
            elif err == 1:
                error_msg = "Over lower limit position"
            elif err == 2:
                error_msg = "Over upper limit position"
            elif err == 3:
                error_msg = "Motor driver error"
            elif err == 4:
                error_msg = "Communication error"
            elif err == 5:
                error_msg = "Encoder error"
            elif err == 6:
                error_msg = "Home error"
            elif err == 7:
                error_msg = "Overload error"
            elif err == 8:
                error_msg = "Torque reach limit error"
            else:
                error_msg = "Something error"

            if error_msg:
                print '\t' + error_msg
                logger.error('broadcast error can_id:%d, %d:%s',
                        msg.arbitration_id,
                        err, error_msg)
                self.motor_alarm()
                print 'motor_alarm', self.state
                return

        if msg.data[2] != ord('C'):
            print "Not 'C'", msg
            return

        sequence = msg.data[3]
        self.sequence_completed_set.append(sequence)

        print 'sequence:' + str(sequence) + ' b:' + str(self.breakpoints)
        if (sequence + 1) in self.breakpoints:
            self.current_sequence = sequence
            self.motor_break()
            print 'motor_break', self.state
            return

        print 'read_sequence_completed', self.total_sequence, self.sequence_completed_set
        if sequence >= self.total_sequence:
            self.sequence_completed()
            logger.info('sequence completed: ' + self.state)
        return sequence

    def read_alignment(self):
        if len(self.rcv_queue) < 1:
            return

        rcv_msg = self.rcv_queue.pop()
        if rcv_msg.data[0] != ord('B'):
            print "Not B 'I / O'", rcv_msg
            return

        if rcv_msg.data[2] != ord('I') and rcv_msg.data[2] != ord('O'):
            print "Not 'I / O'", rcv_msg
            return

        self.alignment_counter += 1
        print 'read_alignment', self.alignment_counter

        if self.alignment_counter > 1:
            self.done()

    def clean_up(self):
        print 'cleanup'
        self.idle()

    def is_connect(self):
        return self.action == 'connect'

    def is_disconnect(self):
        return self.action == 'disconnect'

    def robot_continue(self, current_sequence):
        self.preparation()
        self.setup()
        self.motor_continue()

        self.current_sequence = current_sequence
        self.send_start()
        logger.info('robot_continue: ' + str(current_sequence))


    def bus_send(self, msg, resp=False):
        if NO_BUS:
            return

        self.bus.send(msg)

        if resp:
            rcv_msg = None
            if len(self.rcv_queue) > 0:
                rcv_msg = self.rcv_queue.pop()
            else:
                while not rcv_msg:
                    rcv_msg = self.bus_read()
            return rcv_msg


    def bus_read(self):
        if NO_BUS:
            return
        msg = self.bus.recv(CAN_RECV_TIMEOUT)
        if msg and self.robot_no:
            count = 0
            while self.robot_no != package.robot_no(msg.arbitration_id):
                msg = self.bus.recv(CAN_RECV_TIMEOUT)
                count += 1
                if not msg:
                    break
                if count % 10 == 0:
                    print 'looping', count, msg.arbitration_id, msg.data

        return msg

    def loop(self):

        s = self.state

        if s == 's_idle':
            self.preparation()
            return

        elif s == 's_preparation':
            self.setup()
            return

        elif s == 's_motor_parameter':
            self.send_motor_param()
            return

        elif s == 's_1st_part_sequence':
            self.send_1st_part_sequence()
            return

        elif s == 's_2nd_part_sequence':
            self.send_2nd_part_sequence()
            return

        elif s == 's_validation':
            if NO_BUS:
                sleep(1)
                self.current_sequence = 0
                self.validated()
            else:
                self.read_validation_result()

        elif s == 's_autohome':
            if NO_BUS:
                sleep(1)
                self.homes_completed()
            else:
                self.read_autohome_result()

        elif s == 's_homing':
            self.send_home()

        elif s == 's_ready':
            self.send_start(self.breakpoints)
            if 1 in self.breakpoints:
                print 'Resend start for breakpoint 1'
                self.send_start()

        elif s == 's_clients_work':
            if NO_BUS:
                sleep(1)

                self.current_sequence += 1
                factory = MotorFactory()
                m = factory.create_motor('rotate')
                msg = package.broadcast_dummy_sequence_completed(
                        m.can_id, m.axis_no, self.current_sequence)
                self.rcv_queue.insert(0, msg)

                #self.sequence_completed()
                self.read_sequence_completed()
                logger.info('s_clients_work: ' + str(msg))

            else:
                self.read_sequence_completed()

        elif s == 's_clients_break':
            out = { 'sequence': self.current_sequence,
                'breakpoints': self.breakpoints }
            print 's_clients_break: ' + str(out)
            logger.info('s_clients_break: ' + str(out))
            return out

        elif s == 's_clients_alarm':
            logger.info('s_clients_alarm')
            return True

        elif s == 's_operation_report':
            if NO_BUS:
                sleep(1)
                self.done()
            else:
                self.read_alignment()

        elif s == 's_done':
            self.clean_up()
            self.info()

        rcv_msg = self.bus_read()
        if rcv_msg:
            self.rcv_queue.insert(0, rcv_msg)

    def __over_output_movement(self, east, west):
        """Rotate over output position to pull out the fiber

            Args:
                east (Motor): Input position motor
                west (Motor): Output position motor
        """

        motor = west
        movement = west.position - east.position
        if movement > 0:
            target = west.position + OVER_ROTATION_PULSE
            if target > MAX_ROTATE_POSITION:
                target = MAX_ROTATE_POSITION
            motor.position = target
        elif movement < 0:
            target = west.position - OVER_ROTATION_PULSE
            if target < MIN_ROTATE_POSITION:
                target = MIN_ROTATE_POSITION
            motor.position = target

        return motor

