"""CAN message builder
"""


import logging
from can import Message

CONTROL_METHODS = ('none', 'abs', 'cw', 'ccw')
BROADCAST_CANCEL_CODES = {
    'continue': 1,
    'reset_pointer': 2,
    'clear_all':3,
}

ROBOT_CANMODE_MASK  = 0x0700
ROBOT_C2000_MASK    = 0x0007

## Log process

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger('package')

# create a file handler
handler = logging.FileHandler('package.log')
handler.setLevel(logging.INFO)

# create a logging format
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)

# add the handlers to the logger
logger.addHandler(handler)


# utilities

def twos_comp(val, bits):
    """compute the 2's compliment of int value val

    Args:
        val (integer): target value
        bits (integer): number of bits

    Returns:
        integer: 2's compliment value
    """

    if (val & (1 << (bits - 1))) != 0: # if sign bit is set e.g., 8bit: 128-255
        val = val - (1 << bits)        # compute negative value
    return val

def twos_comp_reverse(val):
    """compute the 2's compliment of int value val

    WARNING: this function was limited at 24 bits calculation!

    Args:
        val (integer): 2's compliment value

    Returns:
        integer: real value
    """

    msb = val>>16 & 0xFF
    middle = val>>8 & 0xFF
    lsb = val & 0xFF
    two_comp = (msb, middle, lsb)

    return twos_comp_reverse_tuple(two_comp)

def twos_comp_reverse_tuple(val):
    """compute the 2's compliment of tuple value val

    Args:
        val (tuple): 2's compliment value as 3 bytes
                     (MSB, middle, LSB)

    Returns:
        integer: real value
    """

    comp_value = (val[0]<<16) | (val[1]<<8) | val[2]
    #print 'val', format(a, '#x')

    if (comp_value & (1 << 23)) != 0:
        out = (1 << 24) - comp_value
        out *= -1
    else:
        out = comp_value
    return out

def robot_no(can_id):
    nr = (can_id & 0x00F8) >> 3
    return nr

def robot_can_id(robot_id):
    """Create robot CAN ID

    Args:
        robot_id (int): Robot number

    Returns:
        int: CAN ID
    """

    can_id = (robot_id << 3) | ROBOT_C2000_MASK

    return can_id


def mask_step_bits(opts=None):
    """Generate breakpoint steps to bitmask

        Args:
            opts (dict): options = {'run': False, 'stops':[]}

        Returns:
            5 bytes array of bitmask
                - breakpoint of step 1 => bit 0 of byte 4
                - breakpoint of step 40 => bit 8 of byte 0
    """

    bits = 0x0000000000
    out = [ 0, 0, 0, 0, 0 ]

    if not opts:
        return out

    if 'run' in opts and opts['run']:
        return out

    if 'stops' not in opts:
        return out

    for nr in opts['stops']:
        if not isinstance(nr, int):
            if len(nr) < 1:
                continue
            nr = int(nr)
        if nr > 0:
            bits |= 1 << (nr - 1)

    i = 4
    while bits > 0:
        out[i] = (bits & 0x00000000FF)
        i -= 1
        if i < 0:
            break
        bits >>= 8

    return out

def unmask_step_bits(data):
    """Generate breakpoint steps to bitmask

        Args:
            data (list): 5 bytes array of bitmask

        Returns:
            list: breakpoint steps
    """

    pass



# command package

def status_request(motor):
    """3.7.1 Request current status of motor

    Args:
        motor (Motor): target motor

    Returns:
        message (Message): CAN message
    """

    data = ['|', motor.axis_no, '?']
    return Message(arbitration_id=motor.can_id, data=data, extended_id=False)

def current_position_request(motor, position_type='C'):
    """3.7.2 Request current position

    Args:
        motor (Motor): target motor
        position_type (character): 'C' for pulse position,
            'E' for encoder position, 'T' for target position

    Returns:
        message (Message): CAN message
    """

    data = ['|', motor.axis_no, position_type]
    return Message(arbitration_id=motor.can_id, data=data, extended_id=False)

def current_position_response(msg):
    """Response current position

    Args:
        msg (Message): CAN message

    Returns:
        message (string): explaination
    """

    if msg.data[2] == 0x43:     # 'C'
        position = 'pulse position'
    elif msg.data[2] == 0x45:   # 'E'
        position = 'encoder position'
    elif msg.data[2] == 0x54:   # 'T'
        position = 'target position'
    else:
        msg = 'Cannot get position response'
        logger.error('current_position_response: %s', msg, exc_info=True)
        raise ValueError(msg)

    #val = msg.data[4]<<16 | msg.data[5]<<8 | msg.data[6]
    val = twos_comp_reverse_tuple(msg.data[4:7])

    out = { 'can_id': msg.arbitration_id,
        'axis': msg.data[1],
        'type': position,
        'position': val,
    }

    logger.info(out)
    return out

def motor_activate(motor, turn_on):
    """3.7.3 Motor ON/OFF state setting command

    Args:
        motor (Motor): target motor
        turn_on (bool): ON/OFF motor

    Returns:
        message (Message): CAN message
    """
    if turn_on:
        data = ['|', motor.axis_no, 'S', 1]
    else:
        data = ['|', motor.axis_no, 'S', 0]

    logger.info('motor_activate %s: %s', turn_on, motor)
    return Message(arbitration_id=motor.can_id, data=data, extended_id=False)

def positioning_request(motor, control_medthod,
                        pulse, speed=500, acceleration=100):
    """3.7.4 Request positioning

    Args:
        motor (Motor): target motor
        control_medthod (string): CONTROL_METHODS
        pulse (int): total pulse moves
        speed (int): setting speed (pulse per second)
        acceleration (int): acceleration/deceleration time (millisecond)

    Returns:
        message (Message): CAN message
    """
    axis = motor.axis_no<<4
    method = CONTROL_METHODS.index(control_medthod)
    control = axis | method
    data = [control,
            pulse>>16 & 0xFF, pulse>>8 & 0xFF, pulse & 0xFF,
            speed>>8 & 0xFF, speed & 0xFF,
            acceleration>>8 & 0xFF, acceleration & 0xFF]

    return Message(arbitration_id=motor.can_id, data=data, extended_id=False)


def positioning_start_request(motor):
    """Start motor

    Args:
        motor (Motor): target motor

    Returns:
        message (Message): CAN message
    """
    data = ['|', motor.axis_no, '*']

    return Message(arbitration_id=motor.can_id, data=data, extended_id=False)

def gripper_start_request(motor, grip=False):
    """Start motor

    Args:
        motor (Motor): target motor
        grip (boolean): grip direction

    Returns:
        message (Message): CAN message
    """
    if grip:
        data = ['|', 0x04, 'G', 0x01, 0x03]
    else:
        #data = ['|', 0x04, 'G', 0x01, 0x01]
        data = ['|', 0x04, 'G', 0x00]

    return Message(arbitration_id=motor.can_id, data=data, extended_id=False)

def error_reset_request(motor):
    """3.7.5 Send error reset request

    Args:
        motor (Motor): target motor

    Returns:
        message (Message): CAN message
    """
    data = ['|', motor.axis_no, 'R']

    return Message(arbitration_id=motor.can_id, data=data, extended_id=False)

def home_request(motor):
    """3.7.6 Home motor

    Args:
        motor (Motor): target motor

    Returns:
        message (Message): CAN message
    """

    #if motor.name == 'Gripper':
        #data = ['|', 0x04, 'G', 0x00]
    #else:
        #data = ['|', motor.axis_no, 'H']
    data = ['|', motor.axis_no, 'H']

    return Message(arbitration_id=motor.can_id, data=data, extended_id=False)

def home_direction_request(motor, clockwise=False, gripper=None):
    """Home direction setting

    Args:
        motor (Motor): target motor
        clockwise (bool): Clockwise direction
        gripper (int): specific value

    Returns:
        message (Message): CAN message
    """

    if gripper:
        data = ['|', motor.axis_no, 'D', gripper]
    elif clockwise:
        data = ['|', motor.axis_no, 'D', 1]
    else:
        data = ['|', motor.axis_no, 'D', 0]

    return Message(arbitration_id=motor.can_id, data=data, extended_id=False)

def backlash_setting_request(motor, value):
    """Set backlash value of the motor

    Args:
        motor (Motor): target motor
        value (int): backlash value

    Returns:
        message (Message): CAN message
    """
    data = ['|', motor.axis_no, 'B', value & 0xFF]

    return Message(arbitration_id=motor.can_id, data=data, extended_id=False)

def encoder_ratio_request(motor, value):
    """Set encoder ratio of the motor

    Args:
        motor (Motor): target motor
        value (int): encoder ratio

    Returns:
        message (Message): CAN message
    """
    data = ['|', motor.axis_no, 'N', value & 0xFF]

    return Message(arbitration_id=motor.can_id, data=data, extended_id=False)

def jog_request(motor, exit):
    """3.7.7 Jog control command

    Args:
        motor (Motor): target motor
        exit (bool): exit jog mode

    Returns:
        message (Message): CAN message
    """

    if exit:
        data = ('|', motor.axis_no, 'J', 'E')
    else:
        data = ('|', motor.axis_no, 'J',
            (motor.speed >> 8) & 0x00FF, motor.speed & 0x00FF)

    return Message(arbitration_id=motor.can_id, data=data, extended_id=False)

def jog_direction_request(motor, stop=True, cw=True):
    """3.7.7 Jog control command

    Args:
        motor (Motor): target motor
        stop (bool): stop jogging
        cw (bool): jog wit cw direction

    Returns:
        message (Message): CAN message
    """

    if stop:
        data = ('|', motor.axis_no, 'J', 'x')
    else:
        if cw:
            data = ('|', motor.axis_no, 'J', '+')
        else:
            data = ('|', motor.axis_no, 'J', '-')

    return Message(arbitration_id=motor.can_id, data=data, extended_id=False)

def positioning_alignment_request(motor):
    """3.7.8 Positioning with alignment sensor

    This command is similar to positioning_request(), but we will get 2 reponses from client:
        1. Feedback result: Position completed
        2. Alignment result: adjust value in pulse unit

    Args:
        motor (Motor): target motor

    Returns:
        message (Message): CAN message
    """
    data = ['|', motor.axis_no, '+']

    return Message(arbitration_id=motor.can_id, data=data, extended_id=False)


def broadcast_start(motor, steps=None):
    """Host issue start on standalone mode

    Args:
        motor (Motor): target motor

    Returns:
        message (Message): CAN message
    """

    data = ['B', 0, '*']
    if steps and len(steps) > 0:
        breakpoints = mask_step_bits({'stops':steps})
        data += breakpoints

    logger.info('Broadcast Robot ID %d: start', robot_no(motor.can_id))

    return Message(arbitration_id=motor.can_id | ROBOT_C2000_MASK,
            data=data, extended_id=False)

def broadcast_cancel(robot_id, code=1):
    """Host issue start on standalone mode

    Args:
        robot_id (int): robot number
        code (int): cancel reason code (BROADCAST_CANCEL_CODES)
                    1: simple break
                    2: reset sequence
                    3: reload all params

    Returns:
        message (Message): CAN message
    """

    data = ['B', 0, 'c', code]

    can_id = robot_can_id(robot_id)
    logger.info('Broadcast Robot ID %d: cancel %d',
            robot_id, code)

    return Message(arbitration_id=can_id, data=data, extended_id=False)

def broadcast_sequence_completed(msg):
    """Client broadcast sequence completed

    Args:
        msg (Message): CAN message

    Returns:
        info (dict): can_id (int), axis_no (int), sequence (int)
    """

    resp = {}
    resp['can_id'] = msg.arbitration_id
    resp['axis_no'] = msg.data[1]
    resp['sequence'] = msg.data[3]

    return resp

def broadcast_dummy_sequence_completed(can_id, axis_no, sequence):
    """Create dummy sequence completed package

    Args:
        can_id (int): CAN Message ID
        axis_no (int): axis number
        sequence (int): sequence number

    Returns:
        message (Message): CAN message
    """

    data = ('B', axis_no, 'C', sequence)
    return Message(arbitration_id=can_id | ROBOT_C2000_MASK,
            data=data, extended_id=False)



def broadcast_alignment(msg):
    """After robot finish operation, it will send aligment values at
        input / output ports back to host

    Args:
        msg (Message): CAN message

    Returns:
        info (dict): port (string), arm_up (int), rotate (int)
    """

    arm_up_arr = (msg.data[1], msg.data[3], msg.data[4])
    rotate_arr = (msg.data[5], msg.data[6], msg.data[7])

    resp = {}

    if msg.data[2] == ord('I'):
        resp['port'] = 'input'
    elif msg.data[2] == ord('O'):
        resp['port'] = 'output'

    resp['arm_up'] = twos_comp_reverse_tuple(arm_up_arr)
    resp['rotate'] = twos_comp_reverse_tuple(rotate_arr)

    return resp

def broadcast_motor_parameters(motor, home_order=0, mechanical_stopper=False,
        ignore_overload=False, torque_attempt=False, torque_watch=False,
        backlash_reduction=False, backlash_force_over_position=False):
    """Host provides motor parameters to client

    Args:
        motor (Motor):              target motor
        home_order (int):           order to make auto home
        mechanical_stopper (bool):  Home with mechanical stopper flag
        ignore_overload (bool):     ignore overload flag
        torque_attempt (bool):      Torque attempt flag
        backlash_reduction (bool):  Backlash reduction flag
        backlash_force_over_position (bool): Force over position flag

    Returns:
        message (Message): CAN message
    """

    home = (home_order << 4)
    if mechanical_stopper:
        home |= 0x08
    if motor.home_cw:
        home |= 1

    encoder = motor.encoder_ratio
    if ignore_overload:
        encoder |= 0x80
    if torque_attempt:
        encoder |= 0x40
    if torque_watch:
        encoder |= 0x20

    backlash = motor.backlash
    if backlash_reduction:
        backlash |= 0x80
    if backlash_force_over_position:
        backlash |= 0x40

    data = ('B', motor.axis_no, 'P',
        ((motor.can_id >> 8) & 0xFF),
        motor.can_id & 0xFF,
        home,
        encoder,
        backlash,
        )

    return Message(arbitration_id=motor.can_id | ROBOT_C2000_MASK,
            data=data, extended_id=False)

def broadcast_start_table(motor, total):
    """Host starts sending sequence table

    Args:
        motor (Motor): target motor
        total (int): total sequence number

    Returns:
        message (Message): CAN message
    """

    data = ('B', 0, 'T', total)

    return Message(arbitration_id=motor.can_id | ROBOT_C2000_MASK,
            data=data, extended_id=False)

def broadcast_end_table(motor, total):
    """Host ends sending sequence table

    Args:
        motor (Motor): target motor
        total (int): total sequence number

    Returns:
        message (Message): CAN message
    """

    data = ('B', 0, 't', total)
    logger.info('Broadcast Robot ID %d: end table %d',
            robot_no(motor.can_id), total)

    return Message(arbitration_id=motor.can_id | ROBOT_C2000_MASK,
            data=data, extended_id=False)

def broadcast_sequence_1st_part(motor, sequence, multiple=1):
    """Send first part of sequence

    Args:
        motor (Motor): target motor
        sequence (int): sequence number
        multiple (int): multi-axes driving

    Returns:
        message (Message): CAN message
    """

    data = ('B', multiple, 'S',
        sequence,
        ((motor.can_id >> 8) & 0xFF),
        motor.can_id & 0xFF,
        )

    return Message(arbitration_id=motor.can_id | ROBOT_C2000_MASK,
            data=data, extended_id=False)

def broadcast_sequence_2nd_part(motor,
        pulse=None,
        speed=None,
        acceleration=None):
    """Send second part of sequence

    Args:
        motor (Motor): target motor
        pulse (int): total pulse moves
        speed (int): setting speed (pulse per second)
        acceleration (int): acceleration/deceleration time (millisecond)

    Returns:
        message (Message): CAN message
    """

    if not pulse:
        pulse = motor.position
    if not speed:
        speed = motor.speed
    if not acceleration:
        acceleration = motor.acceleration

    data = ( motor.axis_no << 4 | 0x01,
            pulse>>16 & 0xFF, pulse>>8 & 0xFF, pulse & 0xFF,
            speed>>8 & 0xFF, speed & 0xFF,
            acceleration>>8 & 0xFF, acceleration & 0xFF,
           )

    return Message(arbitration_id=motor.can_id | ROBOT_C2000_MASK,
            data=data, extended_id=False)

def broadcast_validation_result(msg):
    """Client broadcast sequence completed

    Args:
        msg (Message): CAN message

    Returns:
        info (dict): robot_no (int), error_code (int), error_text (string)
                     axis_no (int), sequence (int)
    """

    error_texts = ( 'Passed',
            'Incomplete download',
            'Home dir incorrect',
            'Encoder incorrect',
            'Backlash incorrect',
            'Multi-axis setting incorrect',
            'Sequence order',
            'Axis no. incorrect',
            'Abs pos incorrect',
            'No speed setting',
            'No ac/dc setting')

    resp = {}
    resp['can_id'] = msg.arbitration_id
    resp['robot_no'] = msg.data[1]
    code = msg.data[3]
    resp['error_code'] = code

    if code < 0 and code >= len(error_texts):
        return resp
    resp['error_text'] = error_texts[code]

    if code < 2:
        return resp

    resp['can_id'] = msg.data[4] << 8 | msg.data[5]

    if code < 5:
        resp['axis_no'] = msg.data[6]
    else:
        resp['sequence'] = msg.data[6]

    return resp

def broadcast_can_devices():
    """Find C2000 devices on network

    Returns:
        message (Message): CAN message
    """

    data = ('B', 0, 'X')

    return Message(arbitration_id=0x100, data=data, extended_id=False)

def broadcast_c2000_act(motor, c2000_number=2):
    """Host requests C2000 mode status

    Args:
        motor (Motor): target motor
        c2000_number (int): number of c2000

    Returns:
        messages(tuple): CAN message tuple
    """

    can_id = (motor.can_id & ~ROBOT_C2000_MASK) + 1
    no = robot_no(motor.can_id)

    data = ('|', no, 'A')
    msgs = []
    for i in range(c2000_number):
        can_id += i
        logger.info('Broadcast Robot mode: %d', can_id)
        msg = Message(arbitration_id=can_id, data=data, extended_id=False)
        msgs.append(msg)

    return msgs

def broadcast_c2000_act_response(msg):
    """C2000 mode status
    """

    if len(msg.data) < 8 or msg.data[2] != ord('A'):
        return None

    axes = []
    for s in msg.data[4:7]:
        data = { 'home': s & 0x1,
            'busy': (s >> 1) & 0x1,
            'error': (s >> 2) & 0x1,
        }
        axes.append(data)

    resp = { 'can_id': hex(msg.arbitration_id),
            'mode': chr(msg.data[3]),
            'axes': axes,
            'state': msg.data[7] & 0x1F,
            'substate': (msg.data[7] >> 5),
    }
    logger.info('C2000 mode: %s', resp)

    return resp



def broadcast_c2000_modes(motor, mode='R', c2000_number=2):
    """Host requests C2000 mode as standalone

    Args:
        motor (Motor): target motor
        mode (String): 'R':robot, 'P':Position module
        c2000_number (int): number of c2000

    Returns:
        messages(tuple): CAN message tuple
    """

    if mode not in ('P', 'R'):
        return None

    can_id = (motor.can_id & ~ROBOT_C2000_MASK) + 1

    data = ('|', 0, 'M', mode)
    msgs = []
    for i in range(c2000_number):
        can_id += i
        logger.info('C2000 change mode request %s: %d', mode, can_id)
        msg = Message(arbitration_id=can_id, data=data, extended_id=False)
        msgs.append(msg)

    return msgs

def broadcast_robot_mode_response(msg):
    """C2000 mode changed

    Args:
        msg (Message): CAN message

    Returns:
        info (dict): robot_no (int), can_id (int)
    """

    if len(msg.data) < 6 or msg.data[3] != ord('A'):
        return None

    resp = {}

    if msg.data[0] == ord('?') and msg.data[1] == 0 and msg.data[2] == ord('M'): 
        can_id = msg.data[4] << 8 | msg.data[5]
        resp['can_id'] = can_id
        resp['robot_no'] = robot_no(can_id)
    else:
        resp['can_id'] = None

    return resp

def broadcast_error_reset(robot_id):
    """Reset C2000

    Args:
        robot_id (int): robot number

    Returns:
        message (Message): CAN message
    """

    data = ('B', 0, 'E')

    can_id = robot_can_id(robot_id)
    logger.info('Broadcast Robot ID %d: error reset', robot_id)

    return Message(arbitration_id=can_id, data=data, extended_id=False)

def read_status(msg):
    """Read motor status

    Args:
        msg (Message): CAN message

    Returns:
        status (string): status code
        error (string): error message
        info (dict):
            can_id (int): CAN ID
            axis (int): motor axis
            home (bool): home returned
            busy (bool): motor is moving
            error_code (int): 0 -> no error
    """

    out = {
        'status': 'error',
        'error': None
    }

    if msg is None:
        out['error'] = 'No message'
        return out

    info = { 'can_id': msg.arbitration_id,
        'axis': msg.data[1] }

    if msg.data[2] != ord('I'):
        out['info'] = info
        out['error'] = "It's not reponse of client"
        return out

    info['home'] = msg.data[3] > 0
    info['busy'] = msg.data[4] > 0
    info['error_code'] = msg.data[6]

    out['status'] = 'success'
    out['info'] = info

    return out


def read_error(message):
    """Read error byte

    Args:
        message (Message): CAN message
    """

    if message is None:
        return

    if message.data[2] != 0x49: # 'I' status information
        return

    error = message.data[6]     # error status
    if error == 0:
        # No error
        return

    if error == 1:
        msg = 'CAN ID %d axis %s: Over lower limit position' % (message.arbitration_id, message.data[1])
        logger.error(msg)
        raise ValueError(msg)

    if error == 2:
        msg = 'CAN ID %d axis %s: Over upper limit position' % (message.arbitration_id, message.data[1])
        logger.error(msg)
        raise ValueError(msg)

    if error == 3:
        msg = 'CAN ID %d axis %s: Motor driver error' % (message.arbitration_id, message.data[1])
        logger.error(msg)
        raise ValueError(msg)

    if error == 4:
        msg = 'CAN ID %d axis %s: Communication error' % (message.arbitration_id, message.data[1])
        logger.error(msg)
        raise ValueError(msg)

    if error == 5:
        msg = 'CAN ID %d axis %s: Encoder error' % (message.arbitration_id, message.data[1])
        logger.error(msg)
        raise ValueError(msg)

    if error == 6:
        msg = 'CAN ID %d axis %s: Home error' % (message.arbitration_id, message.data[1])
        logger.error(msg)
        raise ValueError(msg)

    if error == 7:
        msg = 'CAN ID %d axis %s: Overload error' % (message.arbitration_id, message.data[1])
        logger.error(msg)
        raise ValueError(msg)

    if error == 8:
        msg = 'CAN ID %d axis %s: Torque reach limit error' % (message.arbitration_id, message.data[1])
        logger.error(msg)
        raise ValueError(msg)


def read(motor, message_id, data):
    """Read generic CAN message
    """

    print motor, message_id, data

    if motor is None and data[0] == '?':
        __motor = motor('response', message_id, data[1])

    return __motor

def print_hex(package):
    """Print hex format
    """

    msg = 'Message ID: ' + format(package[0], '#04x') + ' Data:'
    for i in package[1]:
        msg += ' ' + format(i, '#04x')
    print msg

