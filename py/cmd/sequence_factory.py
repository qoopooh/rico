#!/usr/bin/env python
"""Robot sequence factory

It produces robot sequence as 'connect_sequence.csv' and 'disconnect_sequence.csv' files.
"""


import csv
import os

CSV_PATH = "../csv"
CONNECT_SEQUENCE_FILE = "connect_sequence.csv"
DISCONNECT_SEQUENCE_FILE = "disconnect_sequence.csv"
ROLLBACK_SEQUENCE_FILE = "rollback_sequence.csv"

class SequenceFactory(object):
    """Sequence factory class
    """


    actions = {'connect':(), 'disconnect':(), 'rollback':()}

    def __init__(self):

        self.folder = os.path.dirname(__file__)
        self.actions['connect'] = self.__load_sequence(CONNECT_SEQUENCE_FILE)
        self.actions['disconnect'] = self.__load_sequence(DISCONNECT_SEQUENCE_FILE)
        self.actions['rollback'] = self.__load_sequence(ROLLBACK_SEQUENCE_FILE)


    def __load_sequence(self, filename):
        """Load sequence from csv file

        Args:
            filename (string): csv filename

        Returns:
            tuple: (motor code, position index, execution pending)

        Raises:
            ValueError: Sequence order is incorrect
        """

        steps = []

        table_filename = os.path.join(self.folder, CSV_PATH, filename)
        with open(table_filename, 'rb') as csvfile:
            rows = csv.reader(csvfile)
            for row in rows:
                if not row[0].isdigit():
                    continue
                sequence_no = int(row[0])
                motor_code = row[1]
                position_index = int(row[2])
                execution_pending = False
                if sequence_no != len(steps) + 1:
                    raise ValueError('Sequence number of ' + filename + ' is incorrect')

                if len(row) > 3 and row[3]:
                    __pending = row[3]
                    if __pending.isdigit():
                        execution_pending = int(__pending) > 0
                    else:
                        execution_pending = True
                steps.append((motor_code, position_index, execution_pending))

        return tuple(steps)

    def create_sequence(self, action, east, west):
        """Create robot sequence

        Args:
            action (string): robot action
            east (int): east port index
            west (int): west port index

        Returns:
            tuple: (motor code, position index, execution pending)
        """

        if action not in self.actions:
            return None

        out = []
        steps = self.actions[action]
        for i in steps:
            if i[0] == 'rotate':
                if i[1] == -1:
                    out.append((i[0], east, i[2]))
                elif i[1] == -2:
                    out.append((i[0], west, i[2]))
            else:
                out.append(i)

        return tuple(out)

if __name__ == '__main__':
    SEQUENCE_FACTORY = SequenceFactory()
    SEQUENCE = SEQUENCE_FACTORY.create_sequence('connect', east=10, west=22)
    print SEQUENCE
