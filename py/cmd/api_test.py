"""API
    python -m unittest -v api_test
"""

import unittest
import copy

from api import *

class TestInitialStart(unittest.TestCase):

    def setUp(self):
        self.req = { 'action': 'connect',
            'east': 1,
            'west': 2,
            'options': { 'run': False, 'stops': ['3', '6', '9'] }
        }

    def no_test_input_error(self):
        req = copy.copy(self.req)
        req['action'] = None
        resp = robot_initial_start(req)
        self.assertTrue('error' in resp)

        req = copy.copy(self.req)
        req['east'] = 500
        resp = robot_initial_start(req)
        self.assertTrue('error' in resp)

        req = copy.copy(self.req)
        req['west'] = 0
        resp = robot_initial_start(req)
        self.assertTrue('error' in resp)

        req = copy.copy(self.req)
        resp = robot_initial_start(req)
        current_sequence = resp['response']['sequence']

        self.assertFalse('error' in resp)
        self.assertEquals('break', resp['status'])
        self.assertEquals(2, current_sequence)

    def no_test_break_and_continue(self):
        req = copy.copy(self.req)
        req['options']['stops'] = ['4', '8']
        resp = robot_initial_start(req)
        current_sequence = resp['response']['sequence']
        self.assertEquals(3, current_sequence)

        req['options']['current_sequence'] = current_sequence
        resp = robot_continue(req)
        current_sequence = resp['response']['sequence']
        self.assertEquals(7, current_sequence)

        req['options']['current_sequence'] = current_sequence
        resp = robot_continue(req)
        status = resp['status']
        self.assertEquals(status, 'success')

    def test_home(self):
        out = home()
        self.assertEquals(out['status'], 'success')

        #runtime = out['runtime']
        #self.assertTrue(runtime > 5)
        #self.assertTrue(runtime < 6)


if __name__ == '__main__':
    unittest.main()

