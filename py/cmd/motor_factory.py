#!/usr/bin/env python
"""Motor factory module

To build a motor instance
"""


import csv
import os

from motor import Motor

CSV_PATH = "../csv"

class MotorFactory(object):
    """Motor factory

    Load all csv files to build motor instance
    """


    motors = {}
    motor_position_table = {}

    def __init__(self):

        self.folder = os.path.dirname(__file__)
        motors_filename = os.path.join(self.folder, CSV_PATH, 'motors.csv')

        with open(motors_filename, 'rb') as csvfile:
            rows = csv.reader(csvfile)
            for row in rows:
                if row[0].isdigit():
                    self.__add_motor(row)

    def __add_motor(self, info):
        code = info[1]
        self.motors[code] = {
            'name':info[2],
            'can_id':int(info[3], 0),
            'axis_no':int(info[4]),
            'robot_id':int(info[0]),
            'backlash':int(info[5]),
            'home_direction':info[6],
            'encoder_ratio':int(info[7]),
            'mapping_filename':info[8],
        }
        self.motor_position_table[code] = self.__load_mapping_table(
            self.motors[code]['mapping_filename'])

        #print self.motor_position_table[code]

    def __load_mapping_table(self, filename):

        mapping_table = []

        table_filename = os.path.join(self.folder, CSV_PATH, filename)
        with open(table_filename, 'rb') as csvfile:
            rows = csv.reader(csvfile)
            for row in rows:
                if row[0] == 'index':
                    continue
                mapping_table.append({
                    'position': int(row[1]),
                    'speed': int(row[2]),
                    'acc': int(row[3]),
                })

        return mapping_table

    def create_motor(self, code, index_position=0):
        """Create instance of Motor

        Args:
            code (string): motor code name
            index_position (int): position index of mapping table

        Returns:
            Motor: motor
        """

        if code not in self.motors:
            return None
        info = self.motors[code]
        print code, index_position
        position_info = self.motor_position_table[code][index_position]

        __motor = Motor(name=info['name'],
                        can_id=info['can_id'],
                        axis_no=info['axis_no'],
                        backlash=info['backlash'],
                        encoder_ratio=info['encoder_ratio'],
                        home_cw=info['home_direction'] == 'cw',
                        position=position_info['position'],
                        speed=position_info['speed'],
                        acceleration=position_info['acc'])

        return __motor


if __name__ == '__main__':
    MOTOR_FACTORY = MotorFactory()
    MOTOR = MOTOR_FACTORY.create_motor('rotate', 1)
    print MOTOR
