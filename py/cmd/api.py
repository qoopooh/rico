"""api for celery
"""

import can
import logging
import package
import socket
import sys
import copy
from datetime import datetime
from time import sleep

from sequence_factory import SequenceFactory
from motor_factory import MotorFactory
from broadcast import MAX_PORT_NUMBER, Standalone
from alpha import SMU_6, ACTIONS
from client import home_pm

# set logger
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger('api')

# create a file handler
handler = logging.FileHandler('api.log')
handler.setLevel(logging.INFO)

# create a logging format
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)

# add the handlers to the logger
logger.addHandler(handler)

def __validate(req):
    """Validate input
        Returns error
    """

    action = req['action']
    east = req['east']
    west = req['west']

    # check action
    if action not in ACTIONS:
        return { 'error': 'Please type one of actions: ["connect", "disconnect"]' }

    # check east port
    if east > MAX_PORT_NUMBER:
        return { 'error': 'Invalid east port: port number must be less than '
            + str(MAX_PORT_NUMBER) }
    elif east < 1:
        return { 'error': 'Invalid east port: port number must be greater than 0' }
    elif east not in SMU_6:
        return { 'error': 'Available port:' + str(SMU_6) }

    # check west port
    if west > MAX_PORT_NUMBER:
        return { 'error': 'Invalid west port: port number must be less than '
            + str(MAX_PORT_NUMBER) }
    elif west < 1:
        return { 'error': 'Invalid west port: port number must be greater than 0' }
    elif west not in SMU_6:
        return { 'error': 'Available port:' + str(SMU_6) }

    if req['options']:
        # check current sequence
        if 'current_sequence' in req['options']:
            seq = req['options']['current_sequence']
            if type(seq) != int:
                return { 'error': 'Sequence has to be number ('+str(seq)+')' }

            if seq < 1 or seq > 100:
                return { 'error': 'Invalid sequence number (' +str(seq)+ ')' }

    return None

def __get_status(state):
    if state == 's_done':
        status = 'success'
    elif state == 's_clients_break':
        status = 'break'
    elif state == 's_clients_alarm':
        status = 'alarm'
    else:
        status = 'running'
    return status


def robot_initial_start(req):
    """Send sequence to C2000s as standalone mode

    Args: json
        action (string): connect / disconnect
        east (integer): east port number
        west (integer): west port number
        options (dict): run mode
    """

    result = __validate(req)
    if result:
        return { 'request': req, 'status': 'error', 'error': result['error'] }

    s = Standalone(action=req['action'], src=req['east'], dest=req['west'],
            opts=req['options'])

    resp = {}
    while s.state != 's_done':
        resp = s.loop()
        if resp:
            break


    out = { 'request': req,
        'response': resp,
        'status': __get_status(s.state)
    }
    logger.info(out)

    return out

def robot_continue(req):
    result = __validate(req)
    if result:
        return { 'request': req, 'status': 'error', 'error': result['error'] }

    s = Standalone(action=req['action'], src=req['east'], dest=req['west'],
            opts=req['options'])

    print 'req', req
    s.robot_continue(req['options']['current_sequence'])

    resp = {}
    while s.state != 's_done':
        resp = s.loop()
        if resp:
            break

    out = { 'request': req, 'response': resp,
        'status': __get_status(s.state)
    }
    logger.info(out)

    return out


def home():
    """Home all axes in position module mode

    Returns:
        dict:
            status (string): success
            start (datetime): start time
            end (datetime): end time
            runtime (float): time usage in second
    """

    start = datetime.now()

    for m in ('gripper', 'arm_extension', 'rollback', 'arm_up', 'rotate'):
        home_pm(m)
        #sleep(1)

    end = datetime.now()
    runtime = end - start

    out = {
        'status': 'success',
        'start': start,
        'end': end,
        'runtime': runtime.total_seconds(),
    }

    return out

