#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Alpha Test

This is main module to run host application on alpha phase
"""

import logging
import readline
import socket
import os
import sys
import json
from random import randint

import package

from completer import MyCompleter
from sequence_factory import SequenceFactory
from motor_factory import MotorFactory
from broadcast import Standalone
import can

CONNECTED_FILE = 'connected_cable.json'
MAX_PORT_NUMBER     = 144

CAN_RECV_TIMEOUT    = None
#CAN_RECV_TIMEOUT    = 1        # TODO: No recv timeout on host
MAX_ROTATE_POSITION = 14000     # from csv/rotate_position.csv
MIN_ROTATE_POSITION = -14340
OVER_ROTATION_PULSE = 2000      # SMU clear pulse of moving direction
CONNECTOR_OFFSET    = 130       # offset between rollback and connector
ADAPTER_OFFSET      = 50        # offset between rollback and adapter

ACTIONS = ("connect", "disconnect", "random", "rollback", "test")

#SMU_6 = (16, 40, 60, 80, 110, 135)
#SMU_PORTS =  ("16", "40", "60", "80", "110", "135")

SMU_6 = range(1, MAX_PORT_NUMBER + 1)
SMU_PORTS =  [ str(i) for i in SMU_6 ]

#TEST_PATTEN = (('connect', 5, 28),
        #('connect', 28, 49),
        #('connect', 49, 96),
        #('connect', 123, 5),
        #('disconnect', 28, 49),
        #('connect', 72, 72),
        #('disconnect', 5, 28),
        #('disconnect', 72, 72),
        #('disconnect', 49, 96), #('disconnect', 123, 5))

TEST_PATTEN = (
        ('connect', 1, 2),
        ('disconnect', 1, 2),
        ('connect', 2, 3),
        ('disconnect', 2, 3),
        ('connect', 3, 1),
        ('disconnect', 3, 1),
        )

connected_cable = {}
if os.path.isfile(CONNECTED_FILE):
    with open(CONNECTED_FILE, 'rb') as fp:
        connected_cable = json.load(fp)


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger('alpha')

# create a file handler
handler = logging.FileHandler('alpha.log')
handler.setLevel(logging.INFO)

# create a logging format
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)

# add the handlers to the logger
logger.addHandler(handler)




def save_connected():
    """Save connected position
    """

    with open(CONNECTED_FILE, 'wb') as fp:
        json.dump(connected_cable, fp)

def hello():
    """Talk with user
    """

    action = 'disconnect'
    source_port = -1
    destination_port = -1

    completer = MyCompleter(ACTIONS)
    readline.set_completer(completer.complete)
    readline.parse_and_bind('tab: complete')

    while True:
        msg = "Your action (connect / disconnect): "
        response = raw_input(msg)
        if response in ACTIONS:
            action = response
            break
        elif len(response) < 1:
            continue
        elif len(response) < 2: # one char
            if response == 'c':
                response = 'connect'
                break
            elif response == 'd':
                response = 'disconnect'
                break

        else:
            logger.error('Wrong action: %s', response)
            print '\n'

    # run continuously
    if action == 'random' or action == 'test':
        return action, None, None, None

    # clean up autocomplete
    completer = MyCompleter(SMU_PORTS)
    readline.set_completer(completer.complete)
    readline.parse_and_bind('tab: complete')

    while True:
        msg = "Source port number: "
        response = raw_input(msg)
        if response.isdigit():
            number = int(response)
            if number > MAX_PORT_NUMBER:
                msg = '  Invalid east port: port number must be less than'
                print msg, MAX_PORT_NUMBER, '\n'
                continue
            elif number < 1:
                print '  Invalid east port: port number must be greater than 0\n'
                continue

            #elif number not in SMU_6:
                #print '  Available port:', SMU_6, '\n'
                #continue

            source_port = number
            break
        elif len(response) < 1:
            continue
        else:
            print '  Invalid east port: integer only\n'

    while True:
        msg = "Destination port number: "
        response = raw_input(msg)
        if response.isdigit():
            number = int(response)
            if number > MAX_PORT_NUMBER:
                msg = '  Invalid west port: port number must be less than'
                print msg, MAX_PORT_NUMBER, '\n'
                continue
            elif number < 1:
                print '  Invalid west port: port number must be greater than 0\n'
                continue

            #elif number not in SMU_6:
                #print '  Available port:', SMU_6, '\n'
                #continue

            destination_port = number
            break
        elif len(response) < 1:
            continue
        else:
            print '  Invalid west port: integer only\n'

    if action == 'connect':
        notification = 'Connecting east port number %s to west port number %s'
        print notification.format(source_port, destination_port)
        logger.info(notification, source_port, destination_port)
        print '\n'
    elif action == 'disconnect':
        notification = 'Disconnecting west port number %s from east port number %s'
        print notification.format(destination_port, source_port)
        logger.info(notification, destination_port, source_port)
        print '\n'

    elif action == 'rollback':
        notification = '\n\nRollback connecotr of east port number {}\n'
        print notification.format(destination_port, source_port)

    options = {'run': False, 'stops':[]}
    instruction = """You can do:
        1) Press enter to debug step by step
        2) Type "run" and enter to excute a whole process
        3) Type sequences (e.g. 3,8,10,11) which you want to break, press enter to continue
: """
    execution = raw_input(instruction)
    if 'run' in execution.lower():
        options['run'] = True
    elif ',' in execution:
        options['stops'] = execution.split(',')
    elif execution.isdigit():
        options['stops'] = (execution,)


    return action, source_port, destination_port, options


def __turn_on_motor(bus, motor):
    """Activate motor

        Args:
            bus (Bus): CAN bus
            motor (Motor): target motor
    """
    msg = package.motor_activate(motor, turn_on=True)
    print 'SEND:\t\tTurning ON'
    print 'SEND:\t\t', msg
#bus.set_filters([{"can_id": motor.can_id, "can_mask": motor.can_id}])
    bus.send(msg)
    msg = bus.recv(CAN_RECV_TIMEOUT)
    print 'RECEIVED:\t', msg
    print '\n\tTurn on {0}\n'.format(motor)

def __set_backlash(bus, motor):
    """Set backlash

        Args:
            bus (Bus): CAN bus
            motor (Motor): target motor
    """
    backlash = motor.backlash
    if backlash < 1:
        return
    msg = package.backlash_setting_request(motor, backlash)
    print 'SEND:\t\tBacklashing'
    print 'SEND:\t\t', msg
#bus.set_filters([{"can_id": motor.can_id, "can_mask": motor.can_id}])
    bus.send(msg)
    msg = bus.recv(CAN_RECV_TIMEOUT)
    print 'RECEIVED:\t', msg
    print '\n\tSet backlash to', backlash, '\n'

def __set_home_direction(bus, motor):
    """Set home direction

        Args:
            bus (Bus): CAN bus
            motor (Motor): target motor
    """
    clockwise = motor.home_cw
    if clockwise:
        direction = 'clockwise'
    else:
        direction = 'counterclockwise'

    if motor.name == 'Gripper':
        # TODO: soft config for gripper
        msg = package.home_direction_request(motor, clockwise=clockwise, gripper=0x08)
    else:
        msg = package.home_direction_request(motor, clockwise=clockwise)

    msg = package.home_direction_request(motor, clockwise=clockwise)
    print 'SEND:\t\tHome directioning'
    print 'SEND:\t\t', msg
#bus.set_filters([{"can_id": motor.can_id, "can_mask": motor.can_id}])
    bus.send(msg)
    msg = bus.recv(CAN_RECV_TIMEOUT)
    print 'RECEIVED:\t', msg
    print '\n\tSet home direction of {0} to {1}\n'.format(motor, direction)

def __set_home_and_encoder(bus, motor):
    """Home and set encoder of motor
        Encoder setting has to do after homing

        Args:
            bus (Bus): CAN bus
            motor (Motor): target motor
    """

    msg = package.home_request(motor)
    print 'SEND:\t\tHoming'
    print 'SEND:\t\t', msg
#bus.set_filters([{"can_id": motor.can_id, "can_mask": motor.can_id}])
    bus.send(msg)
    msg = bus.recv(CAN_RECV_TIMEOUT)
    print 'RECEIVED:\t', msg
    print '\n\t', motor.name, 'at home\n'

    # Set encoder ratio
    encoder_ratio = motor.encoder_ratio
    if encoder_ratio > 0:
        msg = package.encoder_ratio_request(motor, encoder_ratio)
        print 'SEND:\t\tEncoding'
        print 'SEND:\t\t', msg
        bus.send(msg)
        msg = bus.recv(CAN_RECV_TIMEOUT)
        print 'RECEIVED:\t', msg
        print '\n\tSet encoder ratio to', encoder_ratio, '\n'

def __set_positioning(bus, motor):
    """Positioning motor

        Args:
            bus (Bus): CAN bus
            motor (Motor): target motor
    """
    can_message = package.positioning_request(motor,
                                              control_medthod='abs',
                                              pulse=motor.position,
                                              speed=motor.speed,
                                              acceleration=motor.acceleration)
    print 'SEND:\t\tTurning ON'
    print 'SEND:\t\t', can_message
#bus.set_filters([{"can_id": motor.can_id, "can_mask": motor.can_id}])
    bus.send(can_message)
    print '\n\tSet', motor.name, 'to position:', motor.position, '\n'
    msg = bus.recv(CAN_RECV_TIMEOUT)
    print 'RECEIVED:\t', msg
    if not msg:
        return
    val = package.twos_comp_reverse_tuple(msg.data[4:7])
    print '\n\tPositioning', val, '\n'
    #if val == motor.position:
        #print '\n\t', package.current_position_response(msg), '\n'
    #else:
        #msg = 'Target Position error ({0} / {1})'.format(motor.position, val)
        #raise ValueError(msg)

def __read_encoder(bus, motor):
    """Read encoder position

        Args:
            bus (Bus): CAN bus
            motor (Motor): target motor
    """
    can_message = package.current_position_request(motor, 'E')
    print 'SEND:\t\tEncoder positioning'
    print 'SEND:\t\t', can_message
#bus.set_filters([{"can_id": motor.can_id, "can_mask": motor.can_id}])
    bus.send(can_message)
    encoder_msg = bus.recv(CAN_RECV_TIMEOUT)
    if encoder_msg:
        print 'RECEIVED:\t', encoder_msg
        print '\n\t', package.current_position_response(encoder_msg), '\n'

def __start_motor(bus, motor):
    """Start motor

        Args:
            bus (Bus): CAN bus
            motor (Motor): target motor
    """

    #if motor.name == 'Gripper':
        #message = package.gripper_start_request(motor, motor.position > 0)
    #else:
        #message = package.positioning_start_request(motor)
    message = package.positioning_start_request(motor)

    print 'SEND:\t\tStarting'
    print 'SEND:\t\t', message
#bus.set_filters([{"can_id": motor.can_id, "can_mask": motor.can_id}])
    bus.send(message)
    rcv_msg = bus.recv(CAN_RECV_TIMEOUT)
    print 'RECEIVED:\t', rcv_msg

    # Read encoder position
    __read_encoder(bus, motor)

    # Verify error code from start command
    package.read_error(rcv_msg)

    print '\n\t', motor.name, 'at', motor.position, '\n\n'

def __over_output_movement(bus, inp_motor, outp_motor):
    """Rotate over output position to pull out the fiber

        Args:
            bus (Bus): CAN bus
            inp_motor (Motor): Input position motor
            outp_motor (Motor): Output position motor
    """

    motor = outp_motor
    movement = outp_motor.position - inp_motor.position
    if movement > 0:
        target = outp_motor.position + OVER_ROTATION_PULSE
        if target > MAX_ROTATE_POSITION:
            target = MAX_ROTATE_POSITION
        motor.position = target
    elif movement < 0:
        target = outp_motor.position - OVER_ROTATION_PULSE
        if target < MIN_ROTATE_POSITION:
            target = MIN_ROTATE_POSITION
        motor.position = target

    # Positioning motor
    __set_positioning(bus, motor)

    print "over_output_movement", motor
    __start_motor(bus, motor)



def home(bus, factory):
    """Home position with sequence:
        1) Gripper
        2) Extend arm
        3) Rollback
        4) Arm up/down
        5) Rotate

        Args:
            bus (Bus): CAN bus
            factory (MotorFactory): motor factory
    """

    motors = ('gripper',
              'arm_extension',
              'rollback',
              'arm_up',
              'rotate',
             )
    for code in motors:
        motor = factory.create_motor(code)

        # Turn on motor
        __turn_on_motor(bus, motor)

        # Set backlash
        __set_backlash(bus, motor)

        # Set home direction
        __set_home_direction(bus, motor)

        # Home motor
        __set_home_and_encoder(bus, motor)


def start_sequence_debug(action, src, dest, opts):
    """Start actin sequence

        Args:
            action (string): connect / disconnect
            src (integer): east port number
            desc (integer): west port number
            opts (dict): run mode
    """

    print '\n\n start_sequence_debug', action, 'east:', src, 'west:', dest, opts

    #BUS = can.interface.Bus(channel='can0', bustype='socketcan_ctypes')

    #SEQUENCE_FACTORY = SequenceFactory()
    #MOTOR_FACTORY = MotorFactory()

    if action == 'connect':
        connected_cable[dest] = src
    elif action == 'disconnect':
        connected_cable.pop(str(dest), None)
    save_connected()

def start_sequence(action, src, dest, opts):
    """Start action sequence

        Args:
            action (string): connect / disconnect
            src (integer): east port number
            dest (integer): west port number
            opts (dict): run mode
    """

    print '\n\n start_sequence', action, 'east:', src, 'west:', dest, opts

    BUS = can.interface.Bus(channel='can0', bustype='socketcan_ctypes')

    SEQUENCE_FACTORY = SequenceFactory()
    MOTOR_FACTORY = MotorFactory()
    home(BUS, MOTOR_FACTORY)   # TODO: uncomment on host

    STEPS = SEQUENCE_FACTORY.create_sequence(action, east=(src-1), west=(dest-1))
    first_disconnecting_rotation = True
    second_connecting_rotation = False
    connecting_input_motor = None
    i = 0
    for step in STEPS:
        i += 1
        __motor = MOTOR_FACTORY.create_motor(code=step[0], index_position=step[1])
        __pending = step[2]
        print 'ACTION:\t {0} {1}<=>{2} no.{3}: {4}'.format(action, src, dest, i, __motor)

        # Offset pulse from rollback position
        if __motor.name == 'Rotate':
            if action == 'connect':
                #print "\n\n\nROTATION CONNECTING", second_connecting_rotation
                if second_connecting_rotation:
                    outp_motor = MOTOR_FACTORY.create_motor(code=step[0], index_position=step[1])
                    __over_output_movement(BUS, connecting_input_motor, outp_motor)
                    __motor.position += ADAPTER_OFFSET
                else:
                    second_connecting_rotation = True
                    connecting_input_motor = MOTOR_FACTORY.create_motor(code=step[0], index_position=step[1])
                    __motor.position += CONNECTOR_OFFSET

            elif action == 'disconnect':
                if first_disconnecting_rotation:
                    first_disconnecting_rotation = False
                    __motor.position += CONNECTOR_OFFSET


        # Positioning motor
        __set_positioning(BUS, __motor)

        if __pending:
            continue
        elif opts['run']:
            __start_motor(BUS, __motor)
            continue
        elif len(opts['stops']) < 1:
            raw_input('Press enter to continue:')
        elif str(i) in opts['stops']:
            raw_input('Press enter to continue:')

        __start_motor(BUS, __motor)

def send_sequence(action, src, dest, opts):
    """Send sequence to C2000s as standalone mode

        Args:
            action (string): connect / disconnect
            src (integer): east port number
            dest (integer): west port number
            opts (dict): run mode
    """

    print '\n\n send_sequence', action, 'east:', src, 'west:', dest, opts

    s = Standalone(action=action, src=src, dest=dest, opts=opts)

    while s.state != 's_done':
        user_trigger = s.loop()
        if user_trigger:
            answer = raw_input('Press enter to continue:')
            print 'answer', len(answer), answer
            if len(answer) == 1:
                if answer == 1 or answer == 2:
                    s.send_cancellation_continue(answer)
                elif answer == 3:
                    s.send_cancellation_continue(answer)
                    print 'Cancel task, go back to idle'
                    sys.exit()
                else:
                    print 'Input error'
                    sys.exit()
            s.send_start()

    if action == 'connect':
        connected_cable[dest] = src

        notification = 'Connected east port number %s to west port number %s'
        logger.info(notification, src, dest)
        print '\n'
    elif action == 'disconnect':
        connected_cable.pop(dest, None)

        notification = 'Disconnected west port number %s from east port number %s'
        logger.info(notification, dest, src)
        print '\n'

    save_connected()


if __name__ == "__main__":
    HOSTNAME = socket.gethostname()
    print 'on', HOSTNAME

    ACTION, SRC, DEST, OPTS = hello()

    if ACTION == 'random':
        while True:
            act = randint(0, 1)

            ## Full map
            #inp = randint(1, 144)
            #outp = randint(1, 144)

            ## 6 SMUs
            length = len(SMU_6) - 1
            inp = SMU_6[randint(0, length)]
            outp = SMU_6[randint(0, length)]

            send_sequence(action=ACTIONS[act], src=inp, dest=outp, opts={'run':True})

    elif ACTION == 'test':
        for test in TEST_PATTEN:
            start_sequence(action=test[0], src=test[1], dest=test[2], opts={'run':True})

    else:
        send_sequence(action=ACTION, src=SRC, dest=DEST, opts=OPTS)
