"""Error reset

This test should be run on full system since it needs response from clients

   python -m unittest -v package_test

"""

import unittest
#import package
#from can import Message
#from motor_factory import MotorFactory
import error_reset
from client import AXIS_CODE, AXIS_TABLE, CAN_ID1, CAN_ID2

class Reset(unittest.TestCase):

    def setUp(self):
        self.info = AXIS_TABLE['rotate']

    def test_reset_pm(self):

        resp = reset_pm()

        self.assertEqual(resp['status'], 'success')
        self.assertEqual(resp['response']['can_id'], self.info['can_id'])
        data = resp['response']['data']

    def test_reset_robot(self):

        robot_id = 1
        resp = reset_robot(robot_id)

        self.assertEqual(resp['status'], 'success')
        self.assertEqual(resp['info']['home'], True)


if __name__ == '__main__':
    unittest.main()
