# [alpha.py](https://bitbucket.org/qoopooh/rico/src/master/py/cmd/alpha.py)

Since alpha phase, the robot can debug single step of movement via bash shell of hostboard. User can access host via ssh client (e.g. [putty](http://www.putty.org/))

## How to use

Form bash shell, user can type:
```sh
$ ./alpha.py
```
The app will come with instructions
```sh
host:~/git/rico/py/cmd$ ./alpha.py 
on xu
Your action (connect / disconnect): connect
Source port number: 2
Destination port number: 10

Connecting cable from input port number 2 to output port number 10

You can do:
        1) Press enter to debug step by step
        2) Type "run" and enter to excute a whole process
        3) Type sequences (e.g. 3,8,10,11) which you want to break, press enter to continue
: 
```
User has 3 choices to execute robot:
  1. Press Enter: robot will move step by step for each return keys
  2. Type "run": robot will complet whole sequence as usual process
  3. Type step numbers: to stop robot only some of sequences.
