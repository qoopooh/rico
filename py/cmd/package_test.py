"""CAN message builder

   python -m unittest -v package_test

"""

import unittest
import package
from can import Message
from motor_factory import MotorFactory

class PositionModulePackage(unittest.TestCase):

    def setUp(self):
        factory = MotorFactory()
        self.motor = factory.create_motor('rotate')
        self.msb_can_id = (self.motor.can_id >> 8) & 0xFF
        self.lsb_can_id = self.motor.can_id & 0xFF

    def test_read_status(self):
        data = ('?', self.motor.axis_no, 'I', 1, 0, 1, 0, 1)
        msg = Message(arbitration_id=self.motor.can_id,
                data=data, extended_id=False)
        resp = package.read_status(msg)
        self.assertEqual(resp['status'], 'success')
        self.assertEqual(resp['error'], None)
        self.assertEqual(resp['info']['can_id'], self.motor.can_id)
        self.assertEqual(resp['info']['axis'], self.motor.axis_no)
        self.assertEqual(resp['info']['home'], True)
        self.assertEqual(resp['info']['busy'], False)
        self.assertEqual(resp['info']['error_code'], 0)

    def test_read_status_failed(self):
        data = ('?', self.motor.axis_no, 'i', 1, 0, 1, 0, 1)
        msg = Message(arbitration_id=self.motor.can_id,
                data=data, extended_id=False)
        resp = package.read_status(msg)
        self.assertEqual(resp['status'], 'error')
        self.assertNotEqual(resp['error'], None)
        self.assertEqual(resp['info']['can_id'], self.motor.can_id)
        self.assertEqual(resp['info']['axis'], self.motor.axis_no)

class BroadcastPackage(unittest.TestCase):

    def setUp(self):
        factory = MotorFactory()
        self.motor = factory.create_motor('rotate')
        self.msb_can_id = (self.motor.can_id >> 8) & 0xFF
        self.lsb_can_id = self.motor.can_id & 0xFF
        self.positioning_1st_byte = (self.motor.axis_no << 4) | 0x01

    def test_broadcast_start(self):
        msg = package.broadcast_start(self.motor)
        self.assertEqual(msg.data[0], ord('B'))
        self.assertEqual(msg.data[2], ord('*'))

    def test_broadcast_start_with_steps(self):
        msg = package.broadcast_start(self.motor, steps=(1,3,2,4,40,))
        self.assertEqual(msg.data[0], ord('B'))
        self.assertEqual(msg.data[2], ord('*'))
        self.assertEqual(msg.data[3], 0x80)
        self.assertEqual(msg.data[4], 0)
        self.assertEqual(msg.data[5], 0)
        self.assertEqual(msg.data[6], 0)
        self.assertEqual(msg.data[7], 0x0F)

    def test_broadcast_sequence_completed(self):
        data = ('B', 1, 'C', 10)
        msg = Message(arbitration_id=self.motor.can_id,
                data=data, extended_id=False)
        resp = package.broadcast_sequence_completed(msg)
        self.assertEqual(resp['can_id'], self.motor.can_id)
        self.assertEqual(resp['axis_no'], 1)
        self.assertEqual(resp['sequence'], 10)

    def test_broadcast_cancel(self):
        robot_id = 1
        code = 1
        msg = package.broadcast_cancel(robot_id, code)
        self.assertEqual(msg.arbitration_id, 0x00f)
        self.assertEqual(msg.data[0], ord('B'))
        self.assertEqual(msg.data[2], ord('c'))
        self.assertEqual(msg.data[3], code)

    def test_broadcast_start_table(self):
        msg = package.broadcast_start_table(self.motor, 23)
        self.assertEqual(msg.data[0], ord('B'))
        self.assertEqual(msg.data[2], ord('T'))
        self.assertEqual(msg.data[3], 23)

    def test_broadcast_end_table(self):
        msg = package.broadcast_end_table(self.motor, 18)
        self.assertEqual(msg.data[0], ord('B'))
        self.assertEqual(msg.data[2], ord('t'))
        self.assertEqual(msg.data[3], 18)

    def test_broadcast_sequence_1st(self):
        sequence = 9
        msg = package.broadcast_sequence_1st_part(
                motor=self.motor,
                sequence=sequence)
        self.assertEqual(msg.data[0], ord('B'))
        self.assertEqual(msg.data[1], 1)
        self.assertEqual(msg.data[2], ord('S'))
        self.assertEqual(msg.data[3], sequence)
        self.assertEqual(msg.data[4], self.msb_can_id)
        self.assertEqual(msg.data[5], self.lsb_can_id)

    def test_broadcast_sequence_2nd(self):
        msg = package.broadcast_sequence_2nd_part(
                motor=self.motor,
                pulse=0x55AABB,
                speed=0xCCDD,
                acceleration=0xEEFF)
        self.assertEqual(msg.data[0], self.positioning_1st_byte)
        self.assertEqual(msg.data[1], 0x55)
        self.assertEqual(msg.data[2], 0xAA)
        self.assertEqual(msg.data[3], 0xBB)
        self.assertEqual(msg.data[4], 0xCC)
        self.assertEqual(msg.data[5], 0xDD)
        self.assertEqual(msg.data[6], 0xEE)
        self.assertEqual(msg.data[7], 0xFF)

    def test_broadcast_motor_parameters(self):
        axis_no = self.motor.axis_no
        if self.motor.home_cw:
            cw = 1
        else:
            cw = 0
        encoder_ratio = self.motor.encoder_ratio
        backlash = self.motor.backlash
        msg = package.broadcast_motor_parameters(self.motor)
        self.assertEqual(msg.data[0], ord('B'))
        self.assertEqual(msg.data[1], axis_no)
        self.assertEqual(msg.data[2], ord('P'))
        self.assertEqual(msg.data[3], self.msb_can_id)
        self.assertEqual(msg.data[4], self.lsb_can_id)
        self.assertEqual(msg.data[5], cw)
        self.assertEqual(msg.data[6], encoder_ratio)
        self.assertEqual(msg.data[7], backlash)

    def test_broadcast_validation_result_incompleted_download(self):
        robot_no = 1
        error_code = 1
        data = ('B', robot_no, 'V', error_code)
        msg = Message(arbitration_id=self.motor.can_id,
                data=data, extended_id=False)
        resp = package.broadcast_validation_result(msg)
        self.assertEqual(resp['robot_no'], robot_no)
        self.assertEqual(resp['error_code'], error_code)
        self.assertTrue('error_text' in resp)

    def test_broadcast_validation_result_encoder_error(self):
        robot_no = 1
        error_code = 3
        axis_no = 2
        data = ('B', robot_no, 'V', error_code, self.msb_can_id, self.lsb_can_id, axis_no)
        msg = Message(arbitration_id=self.motor.can_id,
                data=data, extended_id=False)
        resp = package.broadcast_validation_result(msg)
        self.assertEqual(resp['robot_no'], robot_no)
        self.assertEqual(resp['error_code'], error_code)
        self.assertTrue('error_text' in resp)
        self.assertEqual(resp['can_id'], self.motor.can_id)
        self.assertEqual(resp['axis_no'], axis_no)

    def test_broadcast_validation_result_no_speed_setting(self):
        robot_no = 1
        error_code = 9
        sequence = 2
        data = ('B', robot_no, 'V', error_code, self.msb_can_id, self.lsb_can_id, sequence)
        msg = Message(arbitration_id=self.motor.can_id,
                data=data, extended_id=False)
        resp = package.broadcast_validation_result(msg)
        self.assertEqual(resp['robot_no'], robot_no)
        self.assertEqual(resp['error_code'], error_code)
        self.assertTrue('error_text' in resp)
        self.assertEqual(resp['can_id'], self.motor.can_id)
        self.assertEqual(resp['sequence'], sequence)

    def test_broadcast_input_port_alignment(self):
        arm_up = 60000
        rotate = 100000
        data = ('B',
                ((arm_up >> 16) & 0xFF),
                'I',
                ((arm_up >> 8) & 0xFF),
                (arm_up & 0xFF),
                ((rotate >> 16) & 0xFF),
                ((rotate >> 8) & 0xFF),
                (rotate & 0xFF))
        msg = Message(arbitration_id=self.motor.can_id,
                data=data, extended_id=False)
        resp = package.broadcast_alignment(msg)
        self.assertEqual(resp['port'], 'input')
        self.assertEqual(resp['arm_up'], arm_up)
        self.assertEqual(resp['rotate'], rotate)

    def test_broadcast_output_port_alignment(self):
        arm_up = -200
        rotate = -20
        data = ('B',
                ((arm_up >> 16) & 0xFF),
                'O',
                ((arm_up >> 8) & 0xFF),
                (arm_up & 0xFF),
                ((rotate >> 16) & 0xFF),
                ((rotate >> 8) & 0xFF),
                (rotate & 0xFF))
        msg = Message(arbitration_id=self.motor.can_id,
                data=data, extended_id=False)
        resp = package.broadcast_alignment(msg)
        self.assertEqual(resp['port'], 'output')
        self.assertEqual(resp['arm_up'], arm_up)
        self.assertEqual(resp['rotate'], rotate)

    def test_broadcast_robot_mode_request(self):
        out = package.broadcast_c2000_modes(self.motor)
        self.assertEqual(len(out), 2)
        msg = out[1]
        self.assertEqual(msg.data[0], ord('|'))
        self.assertEqual(msg.data[1], 0)
        self.assertEqual(msg.data[2], ord('M'))
        self.assertEqual(msg.data[3], ord('R'))

        out = package.broadcast_c2000_modes(self.motor, 'P')
        self.assertTrue(self.motor.can_id in [item.arbitration_id for item in out])
        msg = out[1]
        self.assertEqual(msg.data[0], ord('|'))
        self.assertEqual(msg.data[1], 0)
        self.assertEqual(msg.data[2], ord('M'))
        self.assertEqual(msg.data[3], ord('P'))

    def test_broadcast_robot_mode_response(self):
        robot_no = package.robot_no(self.motor.can_id)
        data = ('?', 0, 'M', 'A',
                self.msb_can_id,
                self.lsb_can_id)

        msg = Message(arbitration_id=self.motor.can_id,
                data=data, extended_id=False)

        resp = package.broadcast_robot_mode_response(msg)
        print msg, resp
        self.assertEqual(resp['robot_no'], robot_no)
        self.assertEqual(resp['can_id'], self.motor.can_id)

class TestStepsBitmask(unittest.TestCase):
    """To run the test: python -m unittest -v broadcast_test
    """

    def setUp(self):
        self.no_stops = [ 0, 0, 0, 0, 0 ]

    def test_none(self):
        out = package.mask_step_bits()
        self.assertEqual(self.no_stops, out)

    def test_run(self):
        steps = ( 1, 3, 5, 2, 21, 20, 30, 31, 40 )
        out = package.mask_step_bits({'run':True, 'stops':steps})
        self.assertEqual(self.no_stops, out)

    def test_no_run(self):
        out = package.mask_step_bits({'stops':()})
        self.assertEqual(self.no_stops, out)

    def test_zero_step(self):
        steps = ( 0, 0, 0 )
        out = package.mask_step_bits({'stops':steps})
        self.assertEqual(self.no_stops, out)

    def test_single_step(self):
        out = package.mask_step_bits({'stops':(8,)})
        self.assertEqual([ 0, 0, 0, 0, 0x80 ], out)

    def test_multiple_steps(self):
        steps = ( 1, 3, 5, 5, 5, 2, 21, 20, 30, 31, 40 )
        out = package.mask_step_bits({'stops':steps})
        self.assertEqual([ 0x80, 0x60, 0x18, 0, 0x17 ], out)

    def test_all_steps(self):
        steps = ( 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
                11, 12, 13, 14, 15 ,16 ,17 ,18 , 19, 20,
                21, 22, 23, 24, 25 ,26 ,27 ,28 , 29, 30,
                31, 32, 33, 34, 35 ,36 ,37 ,38 , 39, 40 )
        out = package.mask_step_bits({'stops':steps})
        self.assertEqual([ 0xFF, 0xFF, 0xFF, 0xFF, 0xFF ], out)



if __name__ == '__main__':
    unittest.main()
