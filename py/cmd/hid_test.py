"""CAN message builder
"""
import unittest
import hid
from can import Message
from motor_factory import MotorFactory


class HidPackage(unittest.TestCase):
    """To run the test: python -m unittest -v hid_test
    """

    def setUp(self):
        self.idle = '\x7F\x7F\0\0\0\0\0\0'
        self.up_2 = '\x7f\x00\x00\x00\x00\x02\x00\x00'
        self.down_right_sel_l1 = '\xff\xff\x00\x00\x00\x10\x01\x00'
        self.left_23 = '\x00\x7f\x00\x00\x00\x06\x00\x00'

    def test_direction(self):
        resp = hid.get_directions(self.idle)
        self.assertEqual(resp, [])

        resp = hid.get_directions(self.up_2)
        self.assertEqual(resp[0], 'UP')

        resp = hid.get_directions(self.down_right_sel_l1)
        self.assertEqual(resp[0], 'DOWN')
        self.assertEqual(resp[1], 'RIGHT')

        resp = hid.get_directions(self.left_23)
        self.assertEqual(resp[0], 'LEFT')

        resp = hid.get_directions('\x00\x00')
        self.assertEqual(resp[0], 'UP')
        self.assertEqual(resp[1], 'LEFT')

    def test_buttons(self):
        resp = hid.get_buttons(self.idle)
        self.assertEqual(resp, [])

        resp = hid.get_buttons(self.up_2)
        self.assertEqual(resp[0], '2')

        resp = hid.get_buttons(self.down_right_sel_l1)
        self.assertEqual(resp[0], 'L1')
        self.assertEqual(resp[1], 'SELECT')

    def test_commands(self):
        resp = hid.get_commands(self.idle)
        self.assertEqual(resp, [])

        resp = hid.get_commands(self.up_2)
        self.assertEqual(resp[0], 'UP')
        self.assertEqual(resp[1], '2')

        resp = hid.get_commands(self.down_right_sel_l1)
        self.assertEqual(resp[0], 'DOWN')
        self.assertEqual(resp[1], 'RIGHT')
        self.assertEqual(resp[2], 'L1')
        self.assertEqual(resp[3], 'SELECT')

        resp = hid.get_commands(self.left_23)
        self.assertEqual(resp[0], 'LEFT')
        self.assertEqual(resp[1], '2')
        self.assertEqual(resp[2], '3')

    def test_jog(self):
        resp = hid.get_jog(self.idle)
        self.assertEqual(resp['gripper'], 'x')
        self.assertEqual(resp['rotate'], 'x')
        self.assertEqual(resp['arm_up'], 'x')
        self.assertEqual(resp['arm_extension'], 'x')
        self.assertEqual(resp['rollback'], 'x')

        resp = hid.get_jog(self.down_right_sel_l1)
        self.assertEqual(resp['gripper'], 'x')
        self.assertEqual(resp['rotate'], '+')
        self.assertEqual(resp['arm_up'], '-')
        self.assertEqual(resp['arm_extension'], '+')
        self.assertEqual(resp['rollback'], 'x')

        resp = hid.get_jog(self.left_23)
        self.assertEqual(resp['gripper'], '-')
        self.assertEqual(resp['rotate'], '-')
        self.assertEqual(resp['arm_up'], 'x')
        self.assertEqual(resp['arm_extension'], 'x')
        self.assertEqual(resp['rollback'], 'x')

        resp = hid.get_jog(self.up_2)
        self.assertEqual(resp['gripper'], '-')
        self.assertEqual(resp['rotate'], 'x')
        self.assertEqual(resp['arm_up'], '+')
        self.assertEqual(resp['arm_extension'], 'x')
        self.assertEqual(resp['rollback'], 'x')


if __name__ == '__main__':
    unittest.main()
