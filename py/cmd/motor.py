"""Motor model definition
"""


class Motor(object):
    """Motor model
    """


    #status = [0, 0, 0, 0]

    def __init__(self, name, can_id, axis_no,
                 robot_id=0,
                 backlash=0,
                 encoder_ratio=0, home_cw=False,
                 position=0, speed=200, acceleration=100):
        """Initialise motor attributes

        Args:
            name (string): motor name
            can_id (integer): CAN message ID
            axis_no (integer): Axis number
            robot_id (integer): Robot ID
            backlash (integer): backlash value
            encoder_ratio (integer): motor encoder ratio
            home_cw (bool): home clockwise direction
            position (integer): pulse position
            speed (integer): motor speed (pulse per second)
            acceleration (integer): motor acceleration (millisecond)
        """

        self.__name = name
        self.robot_id = robot_id
        self.can_id = can_id
        self.axis_no = axis_no

        self.backlash = backlash
        self.position = position
        self.speed = speed # pulse per second
        self.acceleration = acceleration # millisecond
        self.encoder_ratio = encoder_ratio
        self.home_cw = home_cw

    def __str__(self):
        """Show motor name with CAN message ID and axis number
        """

        msg =  'R%d - %s (CAN_ID: %d, Axis: %d)' % (
            self.robot_id, self.name, self.can_id, self.axis_no)

        if self.position == 0:
            return msg

        return '%s position: %d' % ( msg, self.position)

    @property
    def name(self):
        """Official name

        Returns:
            string: name
        """

        return self.__name
