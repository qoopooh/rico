""" Joystick HID


PS3 <Buffer 01 00 00 00 0f 00 82 81
            81 82 00 00 00 00 00 00
            00 00 00 00 00 00 00 00
            00 00 00 00 00 03 ef 14
            00 00 00 00 23 03 77 01
            00 02 00 02 00 02 00 02 00>

Microntek <Buffer 7f 7f 00 00 00 00 00 00>
    Idl -  7f 7f 00 00 00 00 00 00
    1   -  7f 7f 00 00 00 01 00 00
    2   -  7f 7f 00 00 00 02 00 00
    3   -  7f 7f 00 00 00 04 00 00
    4   -  7f 7f 00 00 00 08 00 00
    L1  -  7f 7f 00 00 00 10 00 00
    L2  -  7f 7f 00 00 00 40 00 00
    R1  -  7f 7f 00 00 00 20 00 00
    R2  -  7f 7f 00 00 00 80 00 00
    SL  -  7f 7f 00 00 00 00 01 00
    ST  -  7f 7f 00 00 00 00 02 00

OKER <Buffer 7f 7f 82 7f 7f 0f 00 c0>
    Idl -  7f 7f 82 7f 7f 0f 00 c0
    1   -  7f 7f 82 7f 7f 1f 00 c0
    2   -  7f 7f 82 7f 7f 2f 00 c0
    3   -  7f 7f 82 7f 7f 4f 00 c0
    4   -  7f 7f 82 7f 7f 8f 00 c0
    L1  -  7f 7f 82 7f 7f 0f 01 c0
    L2  -  7f 7f 82 7f 7f 0f 04 c0
    R1  -  7f 7f 82 7f 7f 0f 02 c0
    R2  -  7f 7f 82 7f 7f 0f 08 c0
    SL  -  7f 7f 82 7f 7f 0f 10 c0
    ST  -  7f 7f 82 7f 7f 0f 20 c0
    LA  -  7f 7f 82 7f 7f 0f 40 c0
    RA  -  7f 7f 82 7f 7f 0f 80 c0

Ultimate Gamepad
    L1 - g
    L2 - t
    R1 - h
    R2 - y
    SELECT - m
    START - n
    A - j
    B - k
    X - u
    Y - i
"""

from copy import copy
IDLE = '\x7F\x7F\0\0\0\0\0\0'

JOG = {
    'gripper': 'x',
    'rotate' : 'x',
    'arm_up' : 'x',
    'arm_extension' : 'x',
    'rollback' : 'x',
}

COMMAND = {
    'UP': { 'motor': 'arm_up', 'action': '+', },
    'DOWN': { 'motor': 'arm_up', 'action': '-', },
    'LEFT': { 'motor': 'rotate', 'action': '-', },
    'RIGHT': { 'motor': 'rotate', 'action': '+', },
    '1': { 'motor': 'rollback', 'action': '+', },
    '2': { 'motor': 'rollback', 'action': '-', },
    'L1': { 'motor': 'arm_extension', 'action': '+', },
    'L2': { 'motor': 'arm_extension', 'action': '-', },
    'R1': { 'motor': 'gripper', 'action': '+', },
    'R2': { 'motor': 'gripper', 'action': '-', },
}

# Cheaper one
#BUTTON_LOWER = [
    #'SELECT', 'START', '', '', '', '', '', '',
#]
#BUTTON_UPPER = [
    #'1', '2', '3', '4', 'L1', 'R1', 'L2', 'R2',
#]

# OKER
BUTTON_LOWER = [
    'L1', 'R1', 'L2', 'R2', 'SELECT', 'START', 'LA', 'RA',
]
BUTTON_UPPER = [
    '', '', '', '', '1', '2', '3', '4',
]

DIRECTION = {
    '\x7f\x7f': None,
    '\x7f\0': 'UP',
    '\0\0': 'UP/LEFT',
    '\0\x7f': 'LEFT',
    '\0\xff': 'DOWN/LEFT',
    '\x7f\xff': 'DOWN',
    '\xff\xff': 'DOWN/RIGHT',
    '\xff\x7f': 'RIGHT',
    '\xff\0': 'UP/RIGHT',
}

def get_directions(buf):
    if buf[:2] not in DIRECTION:
        return []

    directions = DIRECTION[buf[:2]]
    if not directions:
        return []
    elif '/' in directions:
        return directions.split('/')
    else:
        return [ directions, ]

def get_buttons(buf):
    buttons = []
    upper = ord(buf[5:6])
    lower = ord(buf[6:7])

    for i in range(8):
        on = upper >> i & 0x01
        if on:
            buttons.append(BUTTON_UPPER[i])

    for i in range(8):
        on = lower >> i & 0x01
        if on:
            buttons.append(BUTTON_LOWER[i])

    return buttons

def get_commands(message):
    cmds = get_directions(message)
    cmds += get_buttons(message)

    return cmds

def get_jog(message):
    jog = copy(JOG)

    cmds = get_commands(message)
    for c in cmds:
        if not c in COMMAND:
            continue

        m = COMMAND[c]

        # check redundant direction
        if (c == '2' and '1' in cmds) \
               or (c == 'L2' and 'L1' in cmds) \
               or (c == 'R2' and 'R1' in cmds):
            jog[m['motor']] = 'x'
        else:
            jog[m['motor']] = m['action']


    return jog

