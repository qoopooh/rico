#!/usr/bin/env python
"""Jog motors
"""


import sys
import readline
import random
from socket import *
from motor import Motor
from completer import MyCompleter
import package
import can
import hid

NO_BUS              = True
SERVER_PORT         = 12000
CAN_RECV_TIMEOUT    = None
AXIS_CODE = ('rotate', 'arm_up', 'arm_extension', 'gripper', 'rollback')

AXIS_TABLE = {
    'rotate': {'name':'Rotate', 'can_id':0x009, 'axis_no':0,
        'backlash':0, 'home_direction':'ccw', 'encoder_ratio':0,
        'speed': 3000, 'acceleration': 200},
    'arm_up': {'name':'Arm up/down', 'can_id':0x009, 'axis_no':1,
        'backlash':0, 'home_direction':'cw', 'encoder_ratio':2,
        'speed': 1200, 'acceleration': 200},
    'arm_extension': {'name':'Extend arm', 'can_id':0x009, 'axis_no':2,
        'backlash':0, 'home_direction':'ccw', 'encoder_ratio':1,
        'speed': 400, 'acceleration': 100},
    'rollback': {'name':'Rollback', 'can_id':0x00A, 'axis_no':0,
        'backlash':0, 'home_direction':'ccw', 'encoder_ratio':1,
        'speed': 100, 'acceleration': 100},
    'gripper': {'name':'Gripper', 'can_id':0x00A, 'axis_no':1,
        'backlash':0, 'home_direction':'ccw', 'encoder_ratio':0,
        'speed': 150, 'acceleration': 200}}

info = AXIS_TABLE['rotate']      # Select a motor according to AXIS_CODE

def send_message(bus, msg, response=False):
    """Send can bus message

    Args:
        bus (Bus): CAN bus
        msg (Message): CAN message
        response (bool): Waiting for response message
    Returns:
        message (Message): CAN message
    """

    if NO_BUS:
        return

    bus.send(msg)
    if response:
        rcv_msg = bus.recv(CAN_RECV_TIMEOUT)
        return rcv_msg

def request_response(bus, msg, response_code='I'):
    """Send can bus message and waiting for specific response

    Args:
        bus (Bus): CAN bus
        msg (Message): CAN message
        response_code (character): specific response code
    Returns:
        message (Message): CAN message
    """

    bus.send(msg)
    rcv_msg = bus.recv(CAN_RECV_TIMEOUT)
    while rcv_msg.data[2] != response_code:
        rcv_msg = bus.recv(CAN_RECV_TIMEOUT)

    return rcv_msg


def init(bus):
    """Setup and home all motors

    Args:
        bus (Bus): CAN bus
    Returns:
        motors (dict): motor instances
    """

    motors = {}
    for motor_code in AXIS_CODE:
        info = AXIS_TABLE[motor_code]     # User selected
        __motor = Motor(name=info['name'],
                        can_id=info['can_id'],
                        axis_no=info['axis_no'],
                        backlash=info['backlash'],
                        encoder_ratio=info['encoder_ratio'],
                        home_cw=info['home_direction'] == 'cw')

        # Switch mode to position module
        msg = package.broadcast_robot_mode(__motor, position_module=True)
        print 'SEND:\t\tPosition Module'
        print 'SEND:\t\t', msg
        send_message(bus, msg)

        # Turn on motor
        msg = package.motor_activate(__motor, turn_on=True)
        print 'SEND:\t\tTurning ON'
        print 'SEND:\t\t', msg
        rcv_msg = send_message(bus, msg, response=True)
        if rcv_msg:
            print 'RECEIVED:\t', rcv_msg
        print '\n\tTurn on {0}\n'.format(__motor)

        # Set backlash
        backlash = __motor.backlash
        if backlash > 0:
            msg = package.backlash_setting_request(__motor, backlash)
            print 'SEND:\t\tBacklashing'
            print 'SEND:\t\t', msg
            rcv_msg = send_message(bus, msg, response=True)
            if rcv_msg:
                print 'RECEIVED:\t', rcv_msg
            print '\n\tSet backlash to', backlash, '\n'

        # Set home direction
        clockwise = __motor.home_cw
        if clockwise:
            direction = 'clockwise'
        else:
            direction = 'counterclockwise'
        msg = package.home_direction_request(__motor, clockwise=clockwise)
        print 'SEND:\t\tHome directioning'
        print 'SEND:\t\t', msg
        rcv_msg = send_message(bus, msg, response=True)
        if rcv_msg:
            print 'RECEIVED:\t', rcv_msg
        print '\n\tSet home direction of {0} to {1}\n'.format(__motor, direction)

        # Home motor
        #if __motor.name != 'Gripper': # for servo gripper
        msg = package.home_request(__motor)
        print 'SEND:\t\tHoming'
        print 'SEND:\t\t', msg
        rcv_msg = send_message(bus, msg, response=True)
        if rcv_msg:
            print 'RECEIVED:\t', rcv_msg
        print '\n\t', __motor.name, 'at home\n'

        # Set encoder ratio NOTE: has to do after homing
        encoder_ratio = __motor.encoder_ratio
        if encoder_ratio > 0:
            msg = package.encoder_ratio_request(__motor, encoder_ratio)
            print 'SEND:\t\tEncoding'
            print 'SEND:\t\t', msg
            rcv_msg = send_message(bus, msg, response=True)
            if rcv_msg:
                print 'RECEIVED:\t', rcv_msg
            print '\n\tSet encoder ratio to', encoder_ratio, '\n'

        # current position request
        msg = package.current_position_request(__motor)
        print 'SEND:\t\t', msg
        rcv_msg = send_message(bus, msg, response=True)
        if rcv_msg:
            print 'RECEIVED:\t', rcv_msg
            print '\n\t', package.current_position_response(rcv_msg), '\n'

        # Jog mode
        msg = package.jog_request(__motor, False)
        print 'SEND:\t\tJog request'
        print 'SEND:\t\t', msg
        send_message(bus, msg)
        print '\n\t', __motor.name, 'in JOG mode\n'

        # save motor instance
        motors[motor_code] = __motor

    return motors

def jog_messages(motors, current, prev):
    messages = []
    for i in current:
        print i, current[i]
        msg = None

        if i not in prev or current[i] != prev[i]:
            m = motors[i]
            if current[i] == 'x':
                msg = package.jog_direction_request(m, stop=True)
            elif current[i] == '+':
                msg = package.jog_direction_request(m, stop=False, cw=True)
            elif current[i] == '-':
                msg = package.jog_direction_request(m, stop=False, cw=False)

        prev[i] = current[i]

        if msg:
            messages.append(msg)

    return messages


if __name__ == "__main__":

    bus = can.interface.Bus(channel='can0', bustype='socketcan_ctypes')

    motors = init(bus)
    if not motors or len(motors) < 1:
        print 'Init failed'
        sys.exit()

    server = socket(AF_INET, SOCK_DGRAM)
    server.bind(('', SERVER_PORT))

    print 'UDP server:', server.getsockname()

    previous_jog = {}

    while True:
        rand = random.randint(0, 10)
        message, address = server.recvfrom(1024)

        a = cmp(message, hid.IDLE)
        if a == 0:
            print 'IDLE', address, message
        else:
            print ' '.join(hex(ord(i)) for i in message)

        resp = hid.get_jog(message)
        messages = jog_messages(motors, resp, previous_jog)

        for msg in messages:
            print 'JOG:\t\t', msg
            send_message(bus, msg)

        print resp, '\n\n'
