#!/usr/bin/env python
#
# Instruction on embest board:
#   ```sh
#   crontab -e
#
#       * * * * * cd /root/git/rico/py/can && python daemon.py &
#   ```
#

import can

import logging
import os
import sys
from time import sleep

# import packages from cmd folder
sys.path.append('../cmd')
import package as p
import readcan as r

PID_FILE = '/tmp/can_daemon.pid'
logger = logging.getLogger('cand')

def init_log():
    """First function before running anything
    """

    # set logger
    logging.basicConfig(level=logging.INFO)

    # create a file handler
    handler = logging.FileHandler('can_daemon.log')
    handler.setLevel(logging.INFO)

    # create a logging format
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)

    # add the handlers to the logger
    logger.addHandler(handler)


def init_process():
    """Check the process if it's running
    """

    # get process id
    pid = str(os.getpid())
    if os.path.isfile(PID_FILE):
        msg = PID_FILE + " already exists, exiting"
        logger.debug(msg)

        print msg
        sys.exit()

    file(PID_FILE, 'w').write(pid)


def cmd_request(cmd, msg):
    if cmd == 'G':
        if msg.data[4]:
            if msg.data[5]:
                gripper = 'grip'
            else:
                gripper = 'ungrip'
        else:
            gripper = 'release'
        logger.info('gripper request: %s', gripper)

    elif cmd == 'H':
        logger.info('Home request: can:%d, axis:%d',
                msg.arbitration_id,
                msg.data[1])

    elif cmd == 'R':
        logger.info('Error reset request: can:%d, axis:%d',
                msg.arbitration_id,
                msg.data[1])

    elif cmd == 'R':
        if msg.data[4]:
            activate = 'ON'
        else:
            activate = 'OFF'
        logger.info('Motor %s: can:%d, axis:%d',
                activate,
                msg.arbitration_id,
                msg.data[1])

    else:
        logger.info('CAN:%d axis:%s request\n%s',
                msg.arbitration_id, msg.data[1], msg)


def cmd_response(cmd, msg):
    if cmd in ('C', 'T', 'E'):
        position_type, pulse = get_position_resp(msg.data[2:])
        logger.info(position_type + ': ' + pulse)
    elif cmd == 'I':
        # home status
        if msg.data[3]:
            logger.info("\tHome status: returned")
        else:
            logger.info("\tHome status: never")

        # busy status
        if msg.data[4]:
            logger.info("\tMotor is moving")

        # position completed status
        if msg.data[5]:
            logger.info("\tMotor position is completed")

        # error status
        error = msg.data[6]
        if error == 1:
            text = 'CAN:%d axis:%s => Over lower limit position' % (
                    msg.arbitration_id, msg.data[1])
        elif error == 2:
            text = 'CAN:%d axis:%s => Over upper limit position' % (
                    msg.arbitration_id, msg.data[1])
        elif error == 3:
            text = 'CAN:%d axis:%s => Motor driver error' % (
                    msg.arbitration_id, msg.data[1])
        elif error == 4:
            text = 'CAN:%d axis:%s => Communication error' % (
                    msg.arbitration_id, msg.data[1])
        elif error == 2:
            text = 'CAN:%d axis:%s => Encoder error' % (
                    msg.arbitration_id, msg.data[1])
        else:
            text = None

        if text:
            logger.error(text)

    else:
        logger.info('CAN:%d axis:%s response\n%s',
                msg.arbitration_id, msg.data[1], msg)

def cmd_broadcast(cmd, msg):

    robot_id = p.robot_no(msg.arbitration_id)
    if cmd == '*':
        logger.info('Broadcast start robot id: %d', robot_id)

    elif cmd == 'C':
        logger.info('Broadcast position completed robot id: %d => can_id:%d, sequence:%d',
                msg.data[1],
                msg.arbitration_id,
                msg.data[3])

    elif cmd == 'E':
        logger.info('Broadcast error reset robot id: %d', robot_id)

    elif cmd == 'H':
        logger.info('Broadcast home robot id: %d', robot_id)

    elif cmd == 'I' or cmd == 'O':
        resp = p.broadcast_alignment(msg)
        logger.info('Broadcast alignment %s robot id:%d, arm_up:%d, rotate:%d',
                resp['port'],
                robot_id,
                resp['arm_up'],
                resp['rotate']
                )

    elif cmd == 'P':
        logger.info('Broadcast motor parameter robot id:%d, axis:%d, can_id:%d, cw:%d, encoder:%d, backlash:%d',
                robot_id,
                msg.data[1],
                hex((msg.data[3] << 8) | msg.data[4]),
                msg.data[5],
                msg.data[6],
                msg.data[7]
                )

    elif cmd == 'S':
        logger.info('Broadcast robot id:%d, sequence:%d',
                robot_id, msg.data[3])

    elif cmd == 'T':
        logger.info('Broadcast table start robot id:%d, total:%d',
                robot_id, msg.data[3])

    elif cmd == 't':
        logger.info('Broadcast table end robot id:%d, total:%d',
                robot_id, msg.data[3])

    elif cmd == 'V':
        resp = p.broadcast_validation_result(msg)
        if 'axis_no' in resp:
            additional_text = ', axis:' + resp['axis_no']
        elif 'sequence' in resp:
            additional_text = ', sequence:' + resp['sequence']
        else:
            additional_text = ''
        logger.info('Broadcast validataion robot id:%d, can_id:%d, %d:%s%s',
                resp['robot_no'],
                resp['can_id'],
                resp['error_code'],
                resp['error_text'],
                additional_text
                )

    else:
        logger.info('Broadcast robot id: %d\n%s', robot_id, msg)


def show_info(msg):
    if msg.dlc > 2:
        cmd = chr(msg.data[2])
    else:
        logger.info('No command %s', msg)
        return

    req = chr(msg.data[0])
    if req == '|':
        logger.info('Host request position module')
        cmd_request(cmd, msg)
    elif req == '?':
        logger.info('Position module response')
        cmd_response(cmd, msg)
    elif req == 'B':
        cmd_broadcast(cmd, msg)
    elif msg.data[0] in ( 0x00, 0x01, 0x02, 0x03,
            0x10, 0x11, 0x12, 0x13,
            0x20, 0x21, 0x22, 0x23 ):
        axis, method, pulse, speed, acc = r.get_positioning_req(msg.data)
        logger.info('Positioning: axis:%d, method:%s, pulse:%d, speed:%d, acc:%d',
                axis, method, pulse, speed, acc)
    else:
        logger.info('Unknown: %s', msg)


def show_robot_info(msg):
    if msg and len(msg.data) > 1:
        motor = r.get_motor(msg.arbitration_id, msg.data[1])
        logger.info(motor)

if __name__ == "__main__":

    init_log()
    init_process()

    try:
        bus = can.interface.Bus(channel='can0', bustype='socketcan_ctypes')
        while True:
            msg = bus.recv()
            robot_id = p.robot_no(msg.arbitration_id)
            if robot_id == r.ROBOT_ID:
                show_robot_info(msg)
            show_info(msg)

    except ValueError as e:
        logger.error(e)

    finally:
        logger.info('Exit script')
        os.unlink(PID_FILE)

