#!/usr/bin/env python

from __future__ import print_function

import can


def send_one():
    '''Define can interface
    can.rc['interface'] = 'socketcan'
    can.rc['channel'] = 'can0'

    You can put it in $HOME/.canrc:

        [default]
        interface = socketcan
        channel = can0

    '''

    #bus = can.interface.Bus()
    #bus = can.interface.Bus(channel='can0', bustype='socketcan_native')
    bus = can.interface.Bus(channel='can0', bustype='socketcan_ctypes')
    msg = can.Message(arbitration_id=0x101,
            data=['!', 0, '?'],
            extended_id=False)
    try:
        bus.send(msg)
        print("Message sent on {}".format(bus.channel_info))
    except can.CanError:
        print("Message NOT sent")

if __name__ == "__main__":
    send_one()

