from __future__ import absolute_import, unicode_literals
from celery import shared_task

@shared_task
def add(x, y):
    out = { 'x': x,
        'y': y,
        'result': x+y }
    return out

@shared_task
def mul(x, y):
    out = { 'x': x,
        'y': y,
        'result': x*y }
    return out

@shared_task
def xsum(numbers):
    out = { 'numbers': numbers,
        'sum': sum(numbers) }
    return out

@shared_task
def robot_start(args):
    from xo288.cmd.api import robot_initial_start

    return robot_initial_start(args)

@shared_task
def robot_debug_continue(args):
    from xo288.cmd.api import robot_continue

    return robot_continue(args)

@shared_task
def robot_error_reset(robot_id, continue_mode):
    from xo288.cmd.error_reset import reset_robot

    return reset_robot(robot_id, continue_mode)

@shared_task
def position_module_error_reset(axis):
    from xo288.cmd.error_reset import reset_pm
    from xo288.cmd.client import AXIS_CODE

    if axis not in AXIS_CODE:
        return {'status': 'error', 'error': 'Incorrect axis' }

    return reset_pm(axis)

@shared_task
def target_pm(axis, position):
    from xo288.cmd.client import target_pm

    res = target_pm(target=position, axis_code=axis)
    data = []
    for b in res.data:
        data.append(b)
    obj = {
        'can_id': res.arbitration_id,
        'data': data,
    }
    return { 'status': 'success', 'response': obj }

@shared_task
def devices():
    from xo288.cmd.client import get_can_ids
    import can

    bus = can.interface.Bus(channel='can0', bustype='socketcan_ctypes')

    out = { 'status': 'success', 'id':  get_can_ids(bus) }
    return out

@shared_task
def modes():
    from xo288.cmd.client import get_c2000_mode
    import can

    bus = can.interface.Bus(channel='can0', bustype='socketcan_ctypes')
    out = { 'status': 'success', 'c2000':  get_c2000_mode(bus) }

    return out

