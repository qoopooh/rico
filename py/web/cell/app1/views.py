# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from celery.result import AsyncResult
import tasks

def index(request):
    return HttpResponse('Hi')

def add(request):
    if 'x' in request.GET:
        x = float(request.GET['x'])
    else:
        x = 0

    if 'y' in request.GET:
        y = float(request.GET['y'])
    else:
        y = 0
    res = tasks.add.delay(x, y)

    return HttpResponse('Hi x:' + str(x) + ' y:' + str(y) + ' ?id=' + res.id + '\n\n' + res.id)

@csrf_exempt
def result(request):
    print ('request', request.GET)
    if 'id' in request.GET:
        i = request.GET['id']
    elif 'id' in request.POST:
        i = request.POST['id']
    else:
        return HttpResponse("No id")

    resp = AsyncResult(i)
    if resp:
        if resp.ready():
            out = resp.get()
            return JsonResponse(out)
        else:
            return JsonResponse({ 'status': 'started'})
    else:
        print('result', str(resp))
        return HttpResponse("done")

    return HttpResponse("No result")

@csrf_exempt
def error_reset(request):

    if request.method == 'GET':
        return HttpResponse("Need POST")

    if request.POST['mode'] == 'position_module':
        if 'axis' not in request.POST:
            return HttpResponse("No axis")

        axis = request.POST['axis']
        res = tasks.position_module_error_reset.delay(axis)
    else:
        if 'robot' not in request.POST:
            return HttpResponse("No robot")

        if 'continue_mode' not in request.POST:
            return HttpResponse("No continue_mode")

        robot = request.POST['robot']
        # mode: continue, restart, reload
        continue_mode  = request.POST['continue_mode']
        res = tasks.robot_error_reset.delay(robot, continue_mode)

    return HttpResponse(res.id)

@csrf_exempt
def target_postion(request):
    if 'axis' in request.GET:
        axis = request.GET['axis']
    elif 'axis' in request.POST:
        i = request.POST['axis']
    else:
        axis = 'gripper'

    if 'position' in request.GET:
        position = int(request.GET['position'])
    elif 'position' in request.POST:
        position = int(request.POST['position'])
    else:
        position = -1

    res = tasks.target_pm.delay(axis, position)
    return HttpResponse(res.id)

def devices(request):
    res = tasks.devices.delay()
    return HttpResponse(res.id)

def modes(request):

    res = tasks.modes.delay()
    return HttpResponse(res.id)

def __connection_param(request):
    """Find out connection parameters

    Args:
        request (Request): GET / POST request

    Returns:
        east (int): east port
        west (int): west port
        options (dict): connection options
    """

    east, west = 0, 0
    options = { 'run': False }
    if request.method == 'POST':
        east = request.POST['east']
        west = request.POST['west']
        if 'stops' in request.POST:
            options['stops'] = request.POST['stops']
        else:
            options['run'] = True
    else:
        east = request.GET['east']
        west = request.GET['west']

        if 'stops' in request.GET:
            options['stops'] = request.GET['stops']
        else:
            options['run'] = True

    return int(east), int(west), options

@csrf_exempt
def connect(request):

    east, west, options = __connection_param(request)

    req = { 'action': 'connect',
        'east': east,
        'west': west,
        'options': options,
    }

    res = tasks.robot_start.delay(req)
    return HttpResponse(res.id)

@csrf_exempt
def disconnect(request):

    east, west, options = __connection_param(request)

    req = { 'action': 'disconnect',
        'east': east,
        'west': west,
        'options': options,
    }

    res = tasks.robot_start.delay(req)
    return HttpResponse(res.id)

@csrf_exempt
def debug(request):
    if 'no' in request.GET:
        current_sequence = int(request.GET['no'])
    else:
        current_sequence = int(request.POST['no'])

    if current_sequence < 1:
        current_sequence = 1
    elif current_sequence > 30:
        current_sequence = 30

    action = None
    east, west = 0, 0
    if 'stops' in request.GET:
        options = { 'run': False,
            'stops': request.GET['stops'],
        }
        action = request.GET['action']
        east = request.GET['east']
        west = request.GET['west']
    elif 'stops' in request.POST:
        options = { 'run': False,
            'stops': request.POST['stops'],
        }
        action = request.POST['action']
        east = request.POST['east']
        west = request.POST['west']

    options['current_sequence'] = current_sequence

    req = { 'action': action,
        'east': int(east),
        'west': int(west),
        'options': options,
    }
    res = tasks.robot_debug_continue.delay(req)
    return HttpResponse(res.id)

