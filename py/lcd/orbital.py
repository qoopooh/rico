#!/usr/bin/env python
# -*- coding: utf-8 -*-

import serial
from time import sleep

#port_name = '/dev/tty.usbserial-DAQ0CVQ'   # mac: FT232
#port_name = '/dev/tty.wchusbserial1420'     # mac: prc uno
port_name = '/dev/tty.SLAB_USBtoUART'       # mac: CP2102

port = serial.Serial(port_name,
        baudrate=19200,
        timeout=5)

cmd = { 'clear' : (chr(254), chr(88)),
    'start_up': (chr(254), chr(64)),
    'brightness': (chr(254), chr(153)),
    'key_brightness': (chr(254), chr(156)),
    'set_led': (chr(254), chr(90)) }

def write(data):
    for i in data:
        port.write(i)
        sleep(.01)

def clear():
    write(cmd['clear'])

def start_up(message):
    write(cmd['start_up'])
    write(message)

def backlight(val=55):
    if val < 0:
        val = 0
    elif val > 255:
        val = 255
    write(cmd['brightness'])
    write(chr(val))

def key_backlight(val=55):
    if val < 0:
        val = 0
    elif val > 255:
        val = 255
    write(cmd['key_brightness'])
    write(chr(val))

def led(number, red=False, green=False):
    if number > 2 or number < 0:
        return

    if red:
        if green:
            state = 3
        else:
            state = 2
    else:
        if green:
            state = 1
        else:
            state = 0

    write(cmd['set_led'])
    write((chr(number), chr(state)))


if __name__ == "__main__":
    led(0)
    led(1, green=True)
    led(2, red=True)

    clear()
    m1 = '   XENOptics    '
    m2 = '                '
    message = m1 + m2
    write(message)
    #start_up(message)
    backlight()
    key_backlight()

