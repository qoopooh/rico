#include <SPI.h>
#include "TimerOne.h"

#define COMMON          0
#define LIGHT_SENS      A0
#define LIGHT_LEVEL     60
#define LIGHT_ENABLE    0
#define LATCH_EN        10

#define STATE_IDLE      0
#define STATE_GRIPPER   1
#define STATE_LED       2


#define NUM_OF_MODULE   1
#define PWM_LEVEL       32
#define TOTAL_PWM_CH    24

#define G_BUSY          6
#define G_IN_POS        7
#define G_ON_STATE      8
#define G_SETON         9

#define G_POS0          2
#define G_POS1          3
#define G_POS_START     4
#define G_HOME_RETURN   5
 


//Global variables
const float R_VOLT_LIMIT = 2.80f;
const float G_VOLT_LIMIT = 3.60f;
const float B_VOLT_LIMIT = 3.30f;
int stayLow = 2;
int lightValue = 0;
int t0on = 0;
int t0Time = 0;
boolean t0enable = false;
boolean t0State = false;
int t1on = 0;
int t1Time = 0;
boolean t1enable = false;
boolean t1State = false;
int t2Hon = 0;
int t2HTime = 0;
String rxData = "";
boolean t2Henable = false;
boolean t2HState = false;
int timeDiv = 0;
int state = STATE_IDLE;
boolean dataRecieved = false;
boolean playState = false;
float lightLvl = 0.0f;
int playSt = 0;
int playTime = 0;
int homeState = 0;
int gripState = 0;
int releaseState = 0;

//RGB data register
int r[NUM_OF_MODULE] = {0};
int g[NUM_OF_MODULE] = {0};
int b[NUM_OF_MODULE] = {0};
int tranPosition[8] = {3,2,1,0,7,6,5,4};
int duty[NUM_OF_MODULE] [TOTAL_PWM_CH] = {0};
int prepareDuty[NUM_OF_MODULE] [TOTAL_PWM_CH] = {0};
int currentDuty[NUM_OF_MODULE] [TOTAL_PWM_CH] = {0};

//Function
int getSunny();
void turnONlamp();
void turnOFFlamp();
void driftOut();
void updateRGB();
void printMainMenu(int act);
void setColor(int module,int no,int r,int g,int b);
void prepareFade(int module,int no,int r,int g,int b);
boolean fadeIn(int module,int no,int r,int g,int b);
boolean fadeOut(int module,int no);
boolean fadeInAllseries();
boolean fadeOutAllseries();
boolean fadeInAll();
boolean fadeOutAll();
boolean framePlay(int t);
boolean highSpeedPlay(int t);
boolean isDisplayed(int module, int no);
int parseInteger(char);
int findTouchXYPosition(char tp);
void waterPlay(int x,int y);
void LED_selfTest(int,int);
bool gripped = true;

//Effect
void waterEffect(char tpCh);
void mushroomEffect(char tpCh);
void fingerEffect(char event,char tpCh);


//playground scene
void sunflower();
void aurora();

void timerIsr() 
{
  if (t0enable == true && (timeDiv % 1000) == 0) //100ms
  {   
    if ( t0on >= t0Time )
    {
      t0State = true;
      t0on = 0;
    }
    else
    {
      t0State = false;
    }
    ++t0on;
  }
  if (t1enable == true && (timeDiv % 100) == 0) //10ms
  {
    if ( t1on >= t1Time )
    {
      t1State = true;
      t1on = 0;
    }
    else
    {
      t1State = false;
    }
    ++t1on;
  }
  if (t2Henable == true && (timeDiv % 10) == 0) //1ms
  {
    if ( t2Hon >= t2HTime )
    {
      t2HState = true;
      t2Hon = 0;
    }
    else
    {
      t2HState = false;
    }
    ++t2Hon;
  }
 
  ++timeDiv;
}

void setup()
{
  Serial.begin(9600);
  
  
  pinMode(LATCH_EN,OUTPUT);
  
  pinMode(G_BUSY,INPUT);  
  pinMode(G_ON_STATE,INPUT); 
  pinMode(G_IN_POS,INPUT); 
  pinMode(G_SETON,INPUT);
  
  pinMode(G_POS0,OUTPUT);  
  pinMode(G_POS1,OUTPUT);  
  pinMode(G_HOME_RETURN,OUTPUT);  
  pinMode(G_POS_START,OUTPUT); 

  digitalWrite(G_POS0,HIGH);
  digitalWrite(G_POS1,HIGH);
  digitalWrite(G_HOME_RETURN,HIGH);
  digitalWrite(G_POS_START,HIGH);

  digitalWrite(LATCH_EN,LOW);
  
  Timer1.initialize(100); //100us overflow
  Timer1.attachInterrupt(timerIsr);
  
  rxData.reserve(32);
  SPI.begin();
  turnOFFlamp();
  delay(100);
}

void loop()
{    
    
    switch(state)
    {
      case STATE_IDLE: // IDLE MODE
      {
        if ( dataRecieved == true )
        {
          if (rxData[0] == 'G')
          {
             state = STATE_GRIPPER;
          }
          else if(rxData[0] == 'L')
          {
            state = STATE_LED;
          }
          dataRecieved = false;
        }
        break;
      }
      case STATE_GRIPPER:  // HOME FRONT MODE
      {
        if(rxData[0] == 'G')
        {
          if (digitalRead(G_ON_STATE) == LOW) //Motor ON
          {
            switch(rxData[1])
            {
              case 'H': //Home
              {               
                switch(homeState)
                {
                  case 0:
                  {
                    if ( digitalRead(G_SETON) == HIGH)
                    {
                      digitalWrite(G_HOME_RETURN,LOW);
                      if(digitalRead(G_BUSY) == LOW) 
                      { 
                        ++homeState;
                      }
                    }
                    else
                    {
                      if (digitalRead(G_ON_STATE) == LOW) 
                      {
                        Serial.print("G,H,1\r\n");
                      }
                      else
                      {
                        Serial.print("G,OFF\r\n");
                      }
                      homeState = 0;
                      rxData = "";
                      state = STATE_IDLE;
                    }
                    break;
                  }
                  case 1:
                  {
                    digitalWrite(G_HOME_RETURN,HIGH);
                    if(digitalRead(G_BUSY) == HIGH && digitalRead(G_IN_POS) == LOW) 
                    { 
                      ++homeState;
                    }
                    break;
                  }
                  case 2:
                  {
                    if (digitalRead(G_ON_STATE) == LOW ) 
                    {
                      Serial.print("G,H,1\r\n");                     
                    }
                    else
                    {
                      Serial.print("G,H,0\r\n");
                    }
                    homeState = 0;
                    rxData = "";
                    state = STATE_IDLE;
                  }             
                }
                break;
              }
              case 'G': //Grip
              {
                switch(gripState)
                {
                  case 0:
                  {
                    if (gripped == false)
                    {
                      digitalWrite(G_POS0,LOW);
                      digitalWrite(G_POS1,HIGH);
                      digitalWrite(G_POS_START,LOW);
                      if(digitalRead(G_BUSY) == LOW) 
                      { 
                        ++gripState;
                      }
                    }
                    else
                    {
                      gripState = 2;
                    }
                    break;
                  }
                  case 1:
                  {
                    digitalWrite(G_POS0,HIGH);
                    digitalWrite(G_POS1,HIGH);
                    digitalWrite(G_POS_START,HIGH);
                    if(digitalRead(G_BUSY) == HIGH && digitalRead(G_IN_POS) == LOW) 
                    { 
                      ++gripState;
                    }
                    break;
                  }
                  case 2:
                  {
                    if (digitalRead(G_ON_STATE) == LOW) 
                    {
                      Serial.print("G,G,1\r\n");
                      gripped = true;
                    }
                    else
                    {
                      Serial.print("G,G,0\r\n");
                      gripped = false;
                    }
                    gripState = 0;
                    rxData = "";
                    state = STATE_IDLE;
                    break;
                  }
                }
                break;
              }
              case 'R': //Release
              {
                switch(releaseState)
                {
                  case 0:
                  {
                    if (gripped == true)
                    {
                      digitalWrite(G_POS0,HIGH);
                      digitalWrite(G_POS1,LOW);
                      digitalWrite(G_POS_START,LOW);
                      if(digitalRead(G_BUSY) == LOW) 
                      { 
                        ++releaseState;
                      }
                    }
                    else
                    {
                      releaseState = 2;
                    }
                    break;
                  }
                  case 1:
                  {
                    digitalWrite(G_POS0,HIGH);
                    digitalWrite(G_POS1,HIGH);
                    digitalWrite(G_POS_START,HIGH);
                    if(digitalRead(G_BUSY) == HIGH && digitalRead(G_IN_POS) == LOW) 
                    { 
                      ++releaseState;
                    }                    break;
                  }
                  case 2:
                  {
                    if (digitalRead(G_ON_STATE) == LOW) 
                    {
                      Serial.print("G,R,1\r\n");
                    }
                    else
                    {
                      Serial.print("G,R,0\r\n");                     
                    }
                    gripped = false;
                    releaseState = 0;
                    rxData = "";
                    state = STATE_IDLE;
                    break;
                  }
                }
                break;
              }
            }
          }
          else
          {
            Serial.write("G,OFF\r\n");
            state = STATE_IDLE;
          }
        }
        break;
      }
      case STATE_LED:  
      {
        if(rxData[0] == 'L')
        {
          switch(rxData[1])
          {
            case 'T': //Thinking
            {
              if (rxData[2] == '1')
              {
                
              }
              else
              {
                
              }
              rxData = "";
              state = STATE_IDLE;
              break;
            }
            case 'P': //Play
            {
              if (rxData[2] == '1')
              {
                byte r = rxData[3];
                byte g = rxData[4];
                byte b = rxData[5];
                /*byte r = 0;
                byte g = 0;
                byte b = 0;
                if (rxData[3] == '1' && rxData[4] == '0' && rxData[5] == '0')
                {
                  r = 32;
                }
                else if(rxData[3] == '0' && rxData[4] == '1' && rxData[5] == '0')
                {
                  g = 32;
                }
                else
                {
                  b = 32;
                }*/
                if ( r > PWM_LEVEL )
                {
                  r = PWM_LEVEL;
                }
                if ( g > PWM_LEVEL )
                {
                  g = PWM_LEVEL;
                }
                if ( b > PWM_LEVEL )
                {
                  b = PWM_LEVEL;
                }
                prepareFade(0,0,r,g,b);
                prepareFade(0,1,r,g,b);
                prepareFade(0,2,r,g,b);
                prepareFade(0,3,r,g,b);
                prepareFade(0,4,r,g,b);
                prepareFade(0,5,r,g,b);
                fadeInAll();
              }
              else
              {
                fadeOutAll();
              }
              Serial.print("L,P,E\r\n");
              rxData = "";
              state = STATE_IDLE;
              break;
            }  
            case 'I': //Initialize
            {
              LED_selfTest(parseInteger(rxData[2]),parseInteger(rxData[3]));
              Serial.print("L,I,E\r\n");
              rxData = "";
              state = STATE_IDLE;
              break;       
            }
            case 'L':
            {
              //Play company logo
              Serial.print("L,L,E\r\n");
              rxData = "";
              state = STATE_IDLE;
            }
          } 
        }
        break;
      }  
    } 
  updateRGB();
}
int parseInteger(char a)
{
  return (a-48);
}
void setColor(int module,int no,int rLvL,int gLvL,int bLvL)
{
  rLvL = (int)((float)rLvL * (R_VOLT_LIMIT/5.0f));
  gLvL = (int)((float)gLvL * (G_VOLT_LIMIT/5.0f));
  bLvL = (int)((float)bLvL * (B_VOLT_LIMIT/5.0f));
  duty[module][no] = rLvL;  
  duty[module][no + 8] = gLvL;
  duty[module][no + 16] = bLvL;
}

boolean isDisplayed(int module, int no)
{
  if (duty[module][no] > 0 || duty[module][no+8] > 0 || duty[module][no+16] > 0)
  {
    return true;
  }
  else
  {
    return false;
  }
}
void prepareFade(int module,int no,int rLvL,int gLvL,int bLvL)
{
  prepareDuty[module][no] = rLvL;  
  prepareDuty[module][no + 8] = gLvL;
  prepareDuty[module][no + 16] = bLvL;
}
void updateRGB()
{
  int i = 0;
  int module = 0;
  int ch = 0;
  for( module = 0 ; module < NUM_OF_MODULE; ++module)
  { 
    for( i = 0 ; i < TOTAL_PWM_CH ; ++i)
    {
      //ch = tranPosition[i % 8];
      ch = i%8;
      if (currentDuty[module][i] < duty[module][i] && currentDuty[module][i] <= PWM_LEVEL)
      {
        if ( i >= 0 && i < 8 )
        {
          if (COMMON == 0)
          {
            bitSet(r[module],ch);
          }
          else
          {
            bitClear(r[module],ch);
          }
        }
        else if ( i >= 8 && i < 16 )
        {
          if (COMMON == 0)
          {
            bitSet(g[module],ch);
          }
          else
          {
            bitClear(g[module],ch);
          }
        }
        else
        {
          if (COMMON == 0)
          {
            bitSet(b[module],ch);
          }
          else
          {
            bitClear(b[module],ch);
          }
        }
        ++currentDuty[module][i];
      }
      else if( currentDuty[module][i] >= duty[module][i] && currentDuty[module][i] <= PWM_LEVEL )
      {
        if ( i >= 0 && i < 8 )
        {
          if (COMMON == 0)
          {
            bitClear(r[module],ch);
          }
          else
          {
            bitSet(r[module],ch);
          }
        }
        else if ( i >= 8 && i < 16 )
        {
          if (COMMON == 0)
          {
            bitClear(g[module],ch);
          }
          else
          {
            bitSet(g[module],ch);
          }
        }
        else
        {
          if (COMMON == 0)
          {
            bitClear(b[module],ch);
          }
          else
          {
            bitSet(b[module],ch);
          }
        }
        ++currentDuty[module][i];
      }
      else
      {
        currentDuty[module][i] = 0;
      }
    }
  }
  driftOut();
}
void driftOut()
{
  digitalWrite(LATCH_EN,LOW);
  for ( int i = (NUM_OF_MODULE-1) ; i >= 0 ; --i)
  {
    SPI.transfer(b[i]);
    SPI.transfer(g[i]);
    SPI.transfer(r[i]);
  }
  digitalWrite(LATCH_EN,HIGH);
}

void turnONlamp()
{
  for (int i = 0; i < NUM_OF_MODULE; ++i)
  {
    for (int j = 0 ; j < TOTAL_PWM_CH ; ++j)
    {
      if ( j < 8 )
      {
        duty[i][j] = 20;
      }
      else
      {
        duty[i][j] = PWM_LEVEL;
      }
    }
  }
  updateRGB();
}

void turnOFFlamp()
{
  for (int i = 0; i < NUM_OF_MODULE; ++i)
  {
    for (int j = 0 ; j < TOTAL_PWM_CH ; ++j)
    {
      duty[i][j] = 0;
    }
  }
  updateRGB();
}

int getSunny()
{
  return analogRead(LIGHT_SENS);
}
void serialEvent() {
  while (Serial.available()) {
    char inChar = (char)Serial.read(); 
    if (inChar != '\n') 
    {
      rxData += inChar;
    }
    else
    {
      dataRecieved = true;
    } 
  }
}
boolean framePlay(int t)
{
  int i = 0;
  t0Time = t;
  t0enable = true;
  t0State = false;
  while(!t0State){Timer1.start();updateRGB();Timer1.stop();}
  return true;
}
boolean highSpeedPlay(int t)
{
  t2HTime = t;
  t2Henable = true;
  t2HState = false;
  while(!t2HState) {Timer1.start();updateRGB();Timer1.stop();}
  return true;
}
boolean fadeIn(int module,int no,int r,int g,int b)
{
  t1Time = 2;
  int i = 0,j = 0,k = 0;
  for ( i = 0, j = 0, k = 0 ; (i <= r) || (j <= g) || (k <= b) ; )
  {
   setColor(module,no,i,j,k);
   
   if ( i <= r )
   {
     ++i;
   }
   if ( j <= g )
   {
     ++j;
   }
   if ( k <= b )
   {
     ++k;
   }
   t1enable = true;
   t1State = false;  
   while(!t1State){Timer1.start();updateRGB();Timer1.stop();}
 } 
  return true;
}
boolean fadeInAllseries()
{
  t1Time = 2;
  int i = 0, j = 0, k = 0;
  int r = 0, g = 0, b = 0;
  int module = 0, no = 0;
  for ( module = 0 ; module < NUM_OF_MODULE ; ++module )
  {
    for ( no = 0 ; no < 8 ; ++no)
    {
      r = prepareDuty[module][no];
      g = prepareDuty[module][no+8];
      b = prepareDuty[module][no+16];
      for ( i = 0, j = 0, k = 0 ; (i <= r) || (j <= g) || (k <= b) ; )
      {
        setColor(module,no,i,j,k);   
        t1enable = true;
        t1State = false;  
         if ( i <= r )
         {
           ++i;
         }
         if ( j <= g )
         {
           ++j;
         }
         if ( k <= b )
         {
           ++k;
         }
         while(!t1State){Timer1.start();updateRGB();Timer1.stop();}
      } 
    }
  } 
  return true;
}
boolean fadeInAll()
{
  int i[NUM_OF_MODULE] [TOTAL_PWM_CH] = {0};
  int j[NUM_OF_MODULE] [TOTAL_PWM_CH] = {0};
  int k[NUM_OF_MODULE] [TOTAL_PWM_CH] = {0};
  int lvl = 0,module = 0,no = 0;
  t1Time = 1;
  for (lvl = 0 ; lvl < PWM_LEVEL; ++lvl)
  {
    for ( module = 0 ; module < NUM_OF_MODULE ; ++module )
    {
      for ( no = 0 ; no < 8 ; ++no)
      {
        if ((i[module][no] <= prepareDuty[module][no]))
        {
          ++i[module][no];
        }
        if (j[module][no+8] <= prepareDuty[module][no+8])
        {
          ++j[module][no+8];
        }
        if (k[module][no+16] <= prepareDuty[module][no+16])
        {
          ++k[module][no+16];
        }
        setColor(module,no,i[module][no],j[module][no+8],k[module][no+16]);
      }
    }
    t1enable = true;
    t1State = false;  
    while(!t1State) {Timer1.start();updateRGB();Timer1.stop();}
  }
  return true;
}
boolean fadeOut(int module,int no)
{
  t1Time = 1;
  int i = duty[module][no];
  int j = duty[module][no+8];
  int k = duty[module][no+16];  
  for( i, j ,k ; ((i>0) || (j>0) || (k>0)) ; )
  {
    if ( i < 0 )
    {
      i = 0;
    }
    else
    {
      --i;
    }
    if ( j < 0 )
    {
      j = 0;
    }
    else
    {
      --j;
    }
    if ( k < 0 )
    {
      k = 0;
    }  
    else
    {
      --k;
    }
    setColor(module,no,i,j,k);
    t1enable = true;
    t1State = false;  
    while(!t1State) {Timer1.start();updateRGB();Timer1.stop();}   
  } 
  return true;
}
boolean fadeOutAllseries()
{
  t1Time = 2;
  int i = 0, j = 0, k = 0;
  int module = 0, no = 0;
  for ( module = 0 ; module < NUM_OF_MODULE ; ++module )
  {
    for ( no = 0 ; no < 8 ; ++no)
    {
      i = duty[module][no];
      j = duty[module][no+8];
      k = duty[module][no+16];  
      for( i, j ,k ; ((i>0) || (j>0) || (k>0)) ; )
      {
        if ( i < 0 )
        {
          i = 0;
        }
        else
        {
          --i;
        }
        if ( j < 0 )
        {
          j = 0;
        }
        else
        {
          --j;
        }
        if ( k < 0 )
        {
          k = 0;
        }  
        else
        {
          --k;
        }
        setColor(module,no,i,j,k);   
        t1enable = true;
        t1State = false;  
        while(!t1State) {Timer1.start();updateRGB();Timer1.stop();}
      } 
    }
  } 
  return true;
}
boolean fadeOutAll()
{
  int lvl = 0,module = 0,no = 0;
  t1Time = 1;
  for (lvl = 0 ; lvl < PWM_LEVEL; ++lvl)
  {
    for ( module = 0 ; module < NUM_OF_MODULE ; ++module )
    {
      for ( no = 0 ; no < 8 ; ++no)
      {
        if ((duty[module][no] > 0))
        {
          --duty[module][no];
        }
        if (duty[module][no+8] > 0)
        {
          --duty[module][no+8];
        }
        if (duty[module][no+16] > 0)
        {
          --duty[module][no+16];
        }
        setColor(module,no,duty[module][no],duty[module][no+8],duty[module][no+16]);
      }
    }
    t1enable = true;
    t1State = false;  
    while(!t1State) {Timer1.start();updateRGB();Timer1.stop();}
  }
  return true;
}
int findTouchXYPosition()
{
  int result = 99;
  int i,j;
  char x[4][4] = { '1', '2', '3' , 'A',
                   '4', '5', '6' , 'B',
                   '7', '8', '9' , 'C',
                   'F', '0', 'E' , 'D'};
   i = random(4);
   j = random(4);
   result = i;
   result <<= 4;
   result |= j;
   return result;   //Return 0x00YX
}
void waterEffect()
{
  int pos = findTouchXYPosition();
  int x = 0;
  int y = 0;
  int t = 7;
  x = pos & 0x0F;
  y = pos & 0xF0;
  y >>= 4;
  turnOFFlamp();
  while(t-- > 0)
  {
    waterPlay(x,y);
  }
  turnOFFlamp();
}
void waterPlay(int x,int y)
{
  int i = 0, j = 0;
  float T1 = 0.2f;
  float w = (2 * 3.14); //X = A cos(w)t 
  float x1 = 0.0f;
  float a = 31.0f;
  while (i < 100)
  {
    x1 = a * cos(w/T1*i);
    if ( i >= 0 && i < 25)
    {
      setColor(0,x,0,0,abs((int)x1));
      switch(y)
      {
        case 0:
        {
          setColor(0,0,0,abs((int)x1),0);
          break;
        }
        case 1:
        {
          setColor(0,4,0,abs((int)x1),0);
          break;
        }
        case 2:
        {
          setColor(1,0,0,abs((int)x1),0);
          break;
        }
        case 3:
        {
          setColor(1,4,0,abs((int)x1),0);
          break;
        }
      }
    }
    else if ( i >= 25 && i < 50)
    {
      setColor(0,x,0,0,abs((int)x1));
      setColor(0,x+4,0,0,abs((int)x1+2));
      switch(y)
      {
        case 0:
        {
          setColor(0,0,0,abs((int)x1),0);
          setColor(0,1,0,abs((int)x1+2),0);
          break;
        }
        case 1:
        {
          setColor(0,4,0,abs((int)x1),0);
          setColor(0,5,0,abs((int)x1+2),0);
          break;
        }
        case 2:
        {
          setColor(1,0,0,abs((int)x1),0);
          setColor(1,1,0,abs((int)x1+2),0);
          break;
        }
        case 3:
        {
          setColor(1,4,0,abs((int)x1),0);
          setColor(1,5,0,abs((int)x1+2),0);
          break;
        }
      }
    }
    else if ( i >= 50 && i < 75)
    {
      setColor(0,x,0,0,abs((int)x1));
      setColor(0,x+4,0,0,abs((int)x1+2));
      setColor(1,x,0,0,abs((int)x1+10));
      switch(y)
      {
        case 0:
        {
          setColor(0,0,0,abs((int)x1),0);
          setColor(0,1,0,abs((int)x1+2),0);
          setColor(0,2,0,abs((int)x1+10),0);
          break;
        }
        case 1:
        {
          setColor(0,4,0,abs((int)x1),0);
          setColor(0,5,0,abs((int)x1+2),0);
          setColor(0,6,0,abs((int)x1+10),0);
          break;
        }
        case 2:
        {
          setColor(1,0,0,abs((int)x1),0);
          setColor(1,1,0,abs((int)x1+2),0);
          setColor(1,2,0,abs((int)x1+10),0);
          break;
        }
        case 3:
        {
          setColor(1,4,0,abs((int)x1),0);
          setColor(1,5,0,abs((int)x1+2),0);
          setColor(1,6,0,abs((int)x1+10),0);
          break;
        }
      }
    }
    else if ( i >= 75 && i < 100)
    {
      setColor(0,x,0,0,abs((int)x1));
      setColor(0,x+4,0,0,abs((int)x1+2));
      setColor(1,x,0,0,abs((int)x1+10));
      setColor(1,x+4,0,0,abs((int)x1+20));
      switch(y)
      {
        case 0:
        {
          setColor(0,0,0,abs((int)x1),0);
          setColor(0,1,0,abs((int)x1+2),0);
          setColor(0,2,0,abs((int)x1+10),0);
          setColor(0,3,0,abs((int)x1+20),0);
          break;
        }
        case 1:
        {
          setColor(0,4,0,abs((int)x1),0);
          setColor(0,5,0,abs((int)x1+2),0);
          setColor(0,6,0,abs((int)x1+10),0);
          setColor(0,7,0,abs((int)x1+20),0);
          break;
        }
        case 2:
        {
          setColor(1,0,0,abs((int)x1),0);
          setColor(1,1,0,abs((int)x1+2),0);
          setColor(1,2,0,abs((int)x1+10),0);
          setColor(1,3,0,abs((int)x1+20),0);
          break;
        }
        case 3:
        {
          setColor(1,4,0,abs((int)x1),0);
          setColor(1,5,0,abs((int)x1+2),0);
          setColor(1,6,0,abs((int)x1+10),0);
          setColor(1,7,0,abs((int)x1+20),0);
          break;
        }
      }
    }
    highSpeedPlay(5);
    ++i;
  }
}
void mushroomEffect()
{
  int pos = findTouchXYPosition();
  int x = 0;
  int y = 0;
  int randR = 0;
  int randG = 0;
  int randB = 0;
  if (pos != 99)
  {
    randR = random(21)+1;
    randG = random(21)+1;
    randB = random(21)+1;
    /*Serial.print("R,G,B : ");
    Serial.print(randR);
    Serial.print(",");
    Serial.print(randG);
    Serial.print(",");
    Serial.println(randB);*/
    x = pos & 0x0F;
    y = pos & 0xF0;
    y >>= 4;
    if ( y == 0 || y == 1 )
    {
      if ( y == 0 )
      {
        if ( isDisplayed(0,x) )
        {
          fadeOut(0,x);
        }
        else
        {
          fadeIn(0,x,randR,randG,randB);
        }
      }
      else
      {
        if ( isDisplayed(0,x+4) )
        {
          fadeOut(0,x+4);
        }
        else
        {
          fadeIn(0,x+4,randR,randG,randB);
        }
      }
    }
    else
    {
      if ( y == 2 )
      {
        if ( isDisplayed(1,x) )
        {
          fadeOut(1,x);
        }
        else
        {
          fadeIn(1,x,randR,randG,randB);
        }
      }
      else
      {
        if ( isDisplayed(1,x+4) )
        {
          fadeOut(1,x+4);
        }
        else
        {
          fadeIn(1,x+4,randR,randG,randB);
        }
      }
    }
  }
}
void fingerEffect(char event,char tpCh)
{
  int pos = findTouchXYPosition();
  int x = 0;
  int y = 0;
  int randR = 0;
  int randG = 0;
  int randB = 0;
  if (pos != 99)
  {
    randR = random(20)+1;
    randG = random(20)+1;
    randB = random(20)+1;
    Serial.print("R,G,B : ");
    Serial.print(randR);
    Serial.print(",");
    Serial.print(randG);
    Serial.print(",");
    Serial.println(randB);
    x = pos & 0x0F;
    y = pos & 0xF0;
    y >>= 4;
    if ( y == 0 || y == 1 )
    {
      if ( y == 0 )
      {
        if ( event == 'R' )
        {
          fadeOut(0,x);
        }
        else
        {
          fadeIn(0,x,randR,randG,randB);
        }
      }
      else
      {
        if ( event == 'R' )
        {
          fadeOut(0,x+4);
        }
        else
        {
          fadeIn(0,x+4,randR,randG,randB);
        }
      }
    }
    else
    {
      if ( y == 2 )
      {
        if ( event == 'R' )
        {
          fadeOut(1,x);
        }
        else
        {
          fadeIn(1,x,randR,randG,randB);
        }
      }
      else
      {
        if ( event == 'R' )
        {
          fadeOut(1,x+4);
        }
        else
        {
          fadeIn(1,x+4,randR,randG,randB);
        }
      }
    }
  }
}

void sunflower()
{
  int st = 0;
  while (st < 10)
  {
    switch(st)
    {
      case 0:
      {
        //fadeOutAll();
        turnOFFlamp();
        prepareFade(0,0,9,4,4);
        prepareFade(0,1,32,25,8);
        prepareFade(0,2,8,2,2);
        prepareFade(0,3,6,2,4);
        prepareFade(0,4,32,29,32);
        prepareFade(0,5,32,10,4);
        prepareFade(0,6,9,9,14);
        prepareFade(0,7,6,4,2);
        prepareFade(1,0,32,30,30);
        prepareFade(1,1,22,29,23);
        prepareFade(1,2,6,2,2);
        prepareFade(1,3,6,2,4);
        prepareFade(1,4,32,29,29);
        prepareFade(1,5,32,27,27);
        prepareFade(1,6,6,2,2);
        prepareFade(1,7,6,2,4);
        if (fadeInAll()){++st;}
        break;
      }
      case 1:
      {
        turnOFFlamp();
        prepareFade(0,0,14,13,18);
        prepareFade(0,1,32,29,25);
        prepareFade(0,2,32,10,2);
        prepareFade(0,3,6,2,4);
        prepareFade(0,4,32,29,32);
        prepareFade(0,5,31,22,14);
        prepareFade(0,6,12,23,27);
        prepareFade(0,7,6,4,4);
        prepareFade(1,0,32,30,30);
        prepareFade(1,1,32,29,25);
        prepareFade(1,2,14,22,9);
        prepareFade(1,3,8,2,4);
        prepareFade(1,4,32,10,2);
        prepareFade(1,5,32,27,27);
        prepareFade(1,6,14,22,9);
        prepareFade(1,7,6,4,2);
        if (fadeInAll()){++st;}
        break;
      }
      case 2:
      {
        turnOFFlamp();
        prepareFade(0,0,32,27,25);
        prepareFade(0,1,32,29,27);
        prepareFade(0,2,32,23,10);
        prepareFade(0,3,32,10,4);
        prepareFade(0,4,25,18,9);
        prepareFade(0,5,32,30,30);
        prepareFade(0,6,32,12,7);
        prepareFade(0,7,10,14,24);
        prepareFade(1,0,32,30,30);
        prepareFade(1,1,32,30,29);
        prepareFade(1,2,29,29,29);
        prepareFade(1,3,6,4,4);
        prepareFade(1,4,32,21,6);
        prepareFade(1,5,32,30,29);
        prepareFade(1,6,32,27,25);
        prepareFade(1,7,10,21,14);
        if (fadeInAll()){++st;}
        break;
      }
      case 3:
      {
        turnOFFlamp();
        prepareFade(0,0,32,27,23);
        prepareFade(0,1,32,30,29);
        prepareFade(0,2,32,30,30);
        prepareFade(0,3,32,17,6);
        prepareFade(0,4,29,27,17);
        prepareFade(0,5,32,30,29);
        prepareFade(0,6,32,30,30);
        prepareFade(0,7,32,17,17);
        prepareFade(1,0,29,27,21);
        prepareFade(1,1,32,30,29);
        prepareFade(1,2,32,30,30);
        prepareFade(1,3,26,27,26);
        prepareFade(1,4,32,10,2);
        prepareFade(1,5,32,29,23);
        prepareFade(1,6,32,29,29);
        prepareFade(1,7,32,27,25);
        if (fadeInAll()){++st;}
        break;
      }
      case 4:
      {
        turnOFFlamp();
        prepareFade(0,0,29,19,4);
        prepareFade(0,1,32,29,27);
        prepareFade(0,2,32,30,30);
        prepareFade(0,3,32,29,29);
        prepareFade(0,4,32,25,17);
        prepareFade(0,5,32,23,10);
        prepareFade(0,6,32,29,32);
        prepareFade(0,7,32,29,25);
        prepareFade(1,0,19,27,23);
        prepareFade(1,1,32,30,29);
        prepareFade(1,2,32,30,30);
        prepareFade(1,3,32,30,30);
        prepareFade(1,4,32,10,2);
        prepareFade(1,5,32,27,25);
        prepareFade(1,6,32,29,29);
        prepareFade(1,7,32,25,25);
        if (fadeInAll()){++st;}
        break;
      }
      case 5:
      {
        turnOFFlamp();
        prepareFade(0,0,19,11,5);
        prepareFade(0,1,32,27,27);
        prepareFade(0,2,18,20,21);
        prepareFade(0,3,32,29,27);
        prepareFade(0,4,29,25,10);
        prepareFade(0,5,32,23,8);
        prepareFade(0,6,32,29,32);
        prepareFade(0,7,32,29,27);
        prepareFade(1,0,19,27,25);
        prepareFade(1,1,22,27,22);
        prepareFade(1,2,32,30,30);
        prepareFade(1,3,32,30,30);
        prepareFade(1,4,32,8,2);
        prepareFade(1,5,32,10,4);
        prepareFade(1,6,32,29,29);
        prepareFade(1,7,32,25,23);
        if (fadeInAll()){++st;}
        break;
      }
      case 6:
      {
        turnOFFlamp();
        prepareFade(0,0,29,19,4);
        prepareFade(0,1,32,29,27);
        prepareFade(0,2,32,30,30);
        prepareFade(0,3,32,29,29);
        prepareFade(0,4,32,25,17);
        prepareFade(0,5,32,23,10);
        prepareFade(0,6,32,29,32);
        prepareFade(0,7,32,29,25);
        prepareFade(1,0,19,27,23);
        prepareFade(1,1,32,30,29);
        prepareFade(1,2,32,30,30);
        prepareFade(1,3,32,30,30);
        prepareFade(1,4,32,10,2);
        prepareFade(1,5,32,27,25);
        prepareFade(1,6,32,29,29);
        prepareFade(1,7,32,25,25);
        if (fadeInAll()){++st;}
        break;
      }
      case 7:
      {
        turnOFFlamp();
        prepareFade(0,0,32,27,23);
        prepareFade(0,1,32,30,29);
        prepareFade(0,2,32,30,30);
        prepareFade(0,3,32,17,6);
        prepareFade(0,4,29,27,17);
        prepareFade(0,5,32,30,29);
        prepareFade(0,6,32,30,30);
        prepareFade(0,7,32,17,17);
        prepareFade(1,0,29,27,21);
        prepareFade(1,1,32,30,29);
        prepareFade(1,2,32,30,30);
        prepareFade(1,3,26,27,26);
        prepareFade(1,4,32,10,2);
        prepareFade(1,5,32,29,23);
        prepareFade(1,6,32,29,29);
        prepareFade(1,7,32,27,25);
        if (fadeInAll()){++st;}
        break;
      }
      case 8:
      {
        turnOFFlamp();
        prepareFade(0,0,32,27,25);
        prepareFade(0,1,32,29,27);
        prepareFade(0,2,32,23,10);
        prepareFade(0,3,32,10,4);
        prepareFade(0,4,25,18,9);
        prepareFade(0,5,32,30,30);
        prepareFade(0,6,32,12,7);
        prepareFade(0,7,10,14,24);
        prepareFade(1,0,32,30,30);
        prepareFade(1,1,32,30,29);
        prepareFade(1,2,29,29,29);
        prepareFade(1,3,6,4,4);
        prepareFade(1,4,32,21,6);
        prepareFade(1,5,32,30,29);
        prepareFade(1,6,32,27,25);
        prepareFade(1,7,10,21,14);
        if (fadeInAll()){++st;}
        break;
      }
      case 9:
      {
        turnOFFlamp();
        prepareFade(0,0,14,13,18);
        prepareFade(0,1,32,29,25);
        prepareFade(0,2,32,10,2);
        prepareFade(0,3,6,2,4);
        prepareFade(0,4,32,29,32);
        prepareFade(0,5,31,22,14);
        prepareFade(0,6,12,23,27);
        prepareFade(0,7,6,4,4);
        prepareFade(1,0,32,30,30);
        prepareFade(1,1,32,29,25);
        prepareFade(1,2,14,22,9);
        prepareFade(1,3,8,2,4);
        prepareFade(1,4,32,10,2);
        prepareFade(1,5,32,27,27);
        prepareFade(1,6,14,22,9);
        prepareFade(1,7,6,4,2);
        if (fadeInAll()){st=99;}
        break;
      }
    }
  }
}
void aurora()
{
  int st = 0;
  while (st < 10)
  {
  switch(st)
    {
      case 0:
      {
        turnOFFlamp();
        prepareFade(0,0,0,0,5);
        prepareFade(0,1,0,0,10);
        prepareFade(0,2,0,0,13);
        prepareFade(0,3,0,21,9);
        prepareFade(0,4,0,0,10);
        prepareFade(0,5,0,1,16);
        prepareFade(0,6,0,6,20);
        prepareFade(0,7,0,27,16);
        prepareFade(1,0,0,1,16);
        prepareFade(1,1,0,17,24);
        prepareFade(1,2,0,22,21);
        prepareFade(1,3,0,0,0);
        prepareFade(1,4,0,0,8);
        prepareFade(1,5,0,0,0);
        prepareFade(1,6,0,0,2);
        prepareFade(1,7,0,0,0);
        if (fadeInAll()){++st;}
        break;
      }
      case 1:
      {
        turnOFFlamp();
        prepareFade(0,0,0,0,5);
        prepareFade(0,1,0,0,10);
        prepareFade(0,2,0,2,11);
        prepareFade(0,3,0,26,12);
        prepareFade(0,4,0,0,10);
        prepareFade(0,5,0,1,16);
        prepareFade(0,6,0,9,17);
        prepareFade(0,7,0,29,18);
        prepareFade(1,0,0,1,16);
        prepareFade(1,1,0,14,24);
        prepareFade(1,2,0,6,20);
        prepareFade(1,3,0,0,2);
        prepareFade(1,4,0,0,10);
        prepareFade(1,5,0,0,0);
        prepareFade(1,6,0,0,0);
        prepareFade(1,7,0,0,0);
        if (fadeInAll()){++st;}
        break;
      }
      case 2:
      {
        turnOFFlamp();
        prepareFade(0,0,0,0,5);
        prepareFade(0,1,0,0,10);
        prepareFade(0,2,0,14,16);
        prepareFade(0,3,0,25,6);
        prepareFade(0,4,0,0,10);
        prepareFade(0,5,0,0,13);
        prepareFade(0,6,0,21,19);
        prepareFade(0,7,0,1,16);
        prepareFade(1,0,0,1,16);
        prepareFade(1,1,0,14,24);
        prepareFade(1,2,0,17,22);
        prepareFade(1,3,0,0,5);
        prepareFade(1,4,0,2,11);
        prepareFade(1,5,0,0,0);
        prepareFade(1,6,0,0,0);
        prepareFade(1,7,0,0,0);
        if (fadeInAll()){++st;}
        break;
      }
      case 3:
      {
        turnOFFlamp();
        prepareFade(0,0,0,0,5);
        prepareFade(0,1,0,0,10);
        prepareFade(0,2,0,21,13);
        prepareFade(0,3,0,0,10);
        prepareFade(0,4,0,0,10);
        prepareFade(0,5,0,0,13);
        prepareFade(0,6,0,29,18);
        prepareFade(0,7,0,0,13);
        prepareFade(1,0,0,1,16);
        prepareFade(1,1,5,18,25);
        prepareFade(1,2,0,25,21);
        prepareFade(1,3,0,0,5);
        prepareFade(1,4,0,2,11);
        prepareFade(1,5,0,0,0);
        prepareFade(1,6,0,0,0);
        prepareFade(1,7,0,0,0);
        if (fadeInAll()){++st;}
        break;
      }
      case 4:
      {
        turnOFFlamp();
        prepareFade(0,0,0,0,5);
        prepareFade(0,1,0,0,13);
        prepareFade(0,2,0,25,16);
        prepareFade(0,3,0,0,10);
        prepareFade(0,4,0,0,10);
        prepareFade(0,5,0,1,16);
        prepareFade(0,6,0,21,19);
        prepareFade(0,7,0,3,19);
        prepareFade(1,0,0,3,19);
        prepareFade(1,1,5,21,26);
        prepareFade(1,2,0,27,23);
        prepareFade(1,3,0,5,9);
        prepareFade(1,4,0,2,11);
        prepareFade(1,5,0,0,0);
        prepareFade(1,6,0,0,0);
        prepareFade(1,7,0,0,0);
        if (fadeInAll()){++st;}
        break;
      }
      case 5:
      {
        turnOFFlamp();
        prepareFade(0,0,0,0,5);
        prepareFade(0,1,0,25,16);
        prepareFade(0,2,0,5,13);
        prepareFade(0,3,0,0,10);
        prepareFade(0,4,0,0,10);
        prepareFade(0,5,0,27,21);
        prepareFade(0,6,0,19,17);
        prepareFade(0,7,0,9,17);
        prepareFade(1,0,0,6,20);
        prepareFade(1,1,2,28,25);
        prepareFade(1,2,0,27,21);
        prepareFade(1,3,0,23,16);
        prepareFade(1,4,0,2,11);
        prepareFade(1,5,0,0,0);
        prepareFade(1,6,0,0,0);
        prepareFade(1,7,0,0,0);
        if (fadeInAll()){++st;}
        break;
      }
      case 6:
      {
        turnOFFlamp();
        prepareFade(0,0,0,21,13);
        prepareFade(0,1,0,13,13);
        prepareFade(0,2,0,9,13);
        prepareFade(0,3,0,2,11);
        prepareFade(0,4,0,2,11);
        prepareFade(0,5,0,23,19);
        prepareFade(0,6,0,25,16);
        prepareFade(0,7,0,21,16);
        prepareFade(1,0,0,9,17);
        prepareFade(1,1,0,29,22);
        prepareFade(1,2,0,27,23);
        prepareFade(1,3,0,16,12);
        prepareFade(1,4,0,9,9);
        prepareFade(1,5,0,0,0);
        prepareFade(1,6,0,0,0);
        prepareFade(1,7,0,0,0);
        if (fadeInAll()){++st;}
        break;
      }
      case 7:
      {
        turnOFFlamp();
        prepareFade(0,0,0,16,12);
        prepareFade(0,1,0,23,16);
        prepareFade(0,2,0,12,16);
        prepareFade(0,3,0,13,13);
        prepareFade(0,4,0,12,16);
        prepareFade(0,5,0,27,21);
        prepareFade(0,6,0,23,16);
        prepareFade(0,7,0,19,14);
        prepareFade(1,0,0,12,19);
        prepareFade(1,1,0,22,24);
        prepareFade(1,2,0,14,19);
        prepareFade(1,3,0,5,17);
        prepareFade(1,4,0,5,9);
        prepareFade(1,5,0,0,0);
        prepareFade(1,6,0,0,0);
        prepareFade(1,7,0,0,0);
        if (fadeInAll()){++st;}
        break;
      }
      case 8:
      {
        turnOFFlamp();
        prepareFade(0,0,0,16,12);
        prepareFade(0,1,0,23,19);
        prepareFade(0,2,0,19,12);
        prepareFade(0,3,0,9,13);
        prepareFade(0,4,0,23,16);
        prepareFade(0,5,0,22,21);
        prepareFade(0,6,0,9,17);
        prepareFade(0,7,0,12,16);
        prepareFade(1,0,0,10,21);
        prepareFade(1,1,0,22,24);
        prepareFade(1,2,0,10,21);
        prepareFade(1,3,0,5,13);
        prepareFade(1,4,0,5,13);
        prepareFade(1,5,0,0,0);
        prepareFade(1,6,0,0,5);
        prepareFade(1,7,0,0,0);
        if (fadeInAll()){++st;}
        break;
      }
      case 9:
      {
        turnOFFlamp();
        prepareFade(0,0,0,9,9);
        prepareFade(0,1,0,17,17);
        prepareFade(0,2,0,2,11);
        prepareFade(0,3,0,13,13);
        prepareFade(0,4,0,16,14);
        prepareFade(0,5,0,17,19);
        prepareFade(0,6,0,12,16);
        prepareFade(0,7,0,12,16);
        prepareFade(1,0,0,17,19);
        prepareFade(1,1,0,17,24);
        prepareFade(1,2,0,14,19);
        prepareFade(1,3,0,5,13);
        prepareFade(1,4,0,5,13);
        prepareFade(1,5,0,0,0);
        prepareFade(1,6,0,5,9);
        prepareFade(1,7,0,0,0);
        if (fadeInAll()){st=99;}
        break;
      }
    }
  }
}
void LED_selfTest(int times,int noOfLED)
{
  int i,j;
  while (times-- > 0)
  {
    for (i = 0 ; i < NUM_OF_MODULE ; ++i)
    {
      for (j = 0 ; j < noOfLED ; ++j)
      {
        Serial.print("I,");
        Serial.print(i);
        Serial.print(",");
        Serial.print(j);
        Serial.print("\r\n");
        fadeIn(i,j,PWM_LEVEL,0,0);
        fadeOut(i,j);
        fadeIn(i,j,0,PWM_LEVEL,0);
        fadeOut(i,j);
        fadeIn(i,j,0,0,PWM_LEVEL);
        fadeOut(i,j);   
      }
    }
  }
}
