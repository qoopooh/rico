#!/usr/bin/env python
# -*- coding: utf-8 -*-

import serial
from time import sleep

#port_name = '/dev/tty.usbserial-DAQ0CVQ'   # mac: FT232
#port_name = '/dev/tty.wchusbserial1420'     # mac: prc uno
port_name = '/dev/tty.SLAB_USBtoUART'       # mac: CP2102

port = serial.Serial(port_name,
        baudrate=19200,
        timeout=5)

if __name__ == "__main__":
    while True:
        data = port.read(1)
        for d in data:
            i = ord(d)
            if i == 254:
                print '\n',
            print i,

