#include <avr/wdt.h>            // library for default watchdog functions
#include <avr/interrupt.h>      // library for interrupts handling
#include <avr/sleep.h>          // library for sleep
#include <avr/power.h>          // library for power control
#include <Wire.h>

// constants
const bool I2C = true;          // A4 (SDA), A5 (SCL)
const int I2C_ADRESS = 0x50;
const long INTERVAL = 1000;
const int LCD_BRIGHTNESS = 36;
const int KEYPAD_BRIGHTNESS = 0;
const int SLEEP_LCD_CMD = 1;
const String COMPANY  = "    XENOptics  \n                ";
const String ROBOT    = "    XSOS-288   \n                ";
const unsigned char CMD_DIM_LCD[]     = { 254, 153 };
const unsigned char CMD_DIM_KEYPAD[]  = { 254, 156 };
const unsigned char CMD_HOME[]  = { 254, 72 };
const unsigned char CMD_CLEAR_SCR[]  = { 254, 88 };
const unsigned char CMD_LED_COLOR[]  = { 254, 90 };
const unsigned char CMD_LED0_OFF[]  = { 254, 90, 0, 0 };
const unsigned char CMD_LED1_OFF[]  = { 254, 90, 1, 0 };
const unsigned char CMD_LED2_OFF[]  = { 254, 90, 2, 0 };
const unsigned char CMD_LCD_DIM_BACKLIGHT[]  = { 254, 153, LCD_BRIGHTNESS };
const unsigned char CMD_KEY_DIM_BACKLIGHT[]  = { 254, 156, KEYPAD_BRIGHTNESS };
const unsigned char CLEAR_BULK[] = {
  254, 90, 0, 0,  // turn off led 0
  254, 90, 1, 0,  // turn off led 1
  254, 90, 2, 0,  // turn off led 2
  254, 153, LCD_BRIGHTNESS,     // dim lcd backlight
  254, 156, KEYPAD_BRIGHTNESS,  // dim keypad backlight
};

// variables
int count_up  = 0;
int count_display  = 0;
int lcd_backlight = LCD_BRIGHTNESS;
int key_backlight = KEYPAD_BRIGHTNESS;
int led_out   = 0;  // 0 - 2
int led_color = 0;  // 0 - 3
unsigned long currentTime;
unsigned long previousTime;
int nbr_remaining;  // how many times remain to sleep before wake up

// interrupt raised by the watchdog firing
// when the watchdog fires during sleep, this function will be executed
// remember that interrupts are disabled in ISR functions
ISR(WDT_vect)
{
  // not hanging, just waiting
  // reset the watchdog
  wdt_reset();

  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(10);
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
}

// function to configure the watchdog: let it sleep 8 seconds before firing
// when firing, configure it for resuming program execution
void configure_wdt(void)
{
  cli();                           // disable interrupts for changing the registers

  MCUSR = 0;                       // reset status register flags

  // Put timer in interrupt-only mode:
  WDTCSR |= 0b00011000;            // Set WDCE (5th from left) and WDE (4th from left) to enter config mode,
  // using bitwise OR assignment (leaves other bits unchanged).
  WDTCSR =  0b01000000 | 0b100001; // set WDIE: interrupt enabled
  // clr WDE: reset disabled
  // and set delay interval (right side of bar) to 8 seconds

  sei();                           // re-enable interrupts

  // reminder of the definitions for the time before firing
  // delay interval patterns:
  //  16 ms:     0b000000
  //  500 ms:    0b000101
  //  1 second:  0b000110
  //  2 seconds: 0b000111
  //  4 seconds: 0b100000
  //  8 seconds: 0b100001
}

// Put the Arduino to deep sleep. Only an interrupt can wake it up.
void set_sleep(int ncycles)
{
  nbr_remaining = ncycles; // defines how many cycles should sleep

  // Set sleep to full power down.  Only external interrupts or
  // the watchdog timer can wake the CPU!
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);

  // Turn off the ADC while asleep.
  power_adc_disable();

  while (nbr_remaining > 0){ // while some cycles left, sleep!

    // Enable sleep and enter sleep mode.
    sleep_mode();

    // CPU is now asleep and program execution completely halts!
    // Once awake, execution will resume at this point if the
    // watchdog is configured for resume rather than restart

    // When awake, disable sleep mode
    sleep_disable();

    // we have slept one time more
    nbr_remaining = nbr_remaining - 1;
  }

  // put everything on again
  power_all_enable();
}

void i2c_write(int len, const unsigned char* data) {
  int i;

  Wire.beginTransmission(I2C_ADRESS);
  for (i = 0; i < len; i++) {
    Wire.write(data[i]);
  }
  Wire.endTransmission();
  delay(64);
}

void i2c_write_val(int len, const unsigned char* cmd, int val) {
  int i;

  Wire.beginTransmission(I2C_ADRESS);
  for (i = 0; i < len; i++) {
    Wire.write(cmd[i]);
  }
  Wire.write(val);
  Wire.endTransmission();
  delay(64);
}

/**
 * Reset all lights
 */
void reset() {
  if (I2C) {
    i2c_write(4, CMD_LED0_OFF);
    i2c_write(4, CMD_LED1_OFF);
    i2c_write(4, CMD_LED2_OFF);
    i2c_write(3, CMD_LCD_DIM_BACKLIGHT);
    i2c_write(3, CMD_KEY_DIM_BACKLIGHT);
    Serial.println("reset");
  } else {
    int len = sizeof(CLEAR_BULK);

    // print out the state of the button:
    for (int i=0; i<len; i++) {
      Serial.write(CLEAR_BULK[i]);
      delay(64);
    }
  }

  lcd_backlight = LCD_BRIGHTNESS;
  key_backlight = KEYPAD_BRIGHTNESS;
  led_out = 0;
  led_color = 0;
}

/**
 * Dim backlight of lcd
 * @param val dimmer value (0 - 255)
 */
void dim_lcd(int val) {
  int len = sizeof(CMD_DIM_LCD);

  if (I2C) {
    i2c_write_val(len, CMD_DIM_LCD, val);
  } else {
    for (int i=0; i<len; i++) {
      Serial.write(CMD_DIM_LCD[i]);
      delay(64);
    }

    Serial.write(val);
  }

  delay(64);
}

/**
 * Dim backlight of keypad
 * @param val dimmer value (0 - 255)
 */
void dim_keypad(int val) {
  int len = sizeof(CMD_DIM_KEYPAD);

  if (I2C) {
    i2c_write_val(len, CMD_DIM_KEYPAD, val);
  } else {
    for (int i=0; i<len; i++) {
      Serial.write(CMD_DIM_KEYPAD[i]);
      delay(64);
    }

    Serial.write(val);
    delay(64);
  }
}

/**
 * Change status led color
 * @param out LED output port number (0 - 2)
 * @param color 0: off
 *              1: green
 *              2: red
 *              3: yellow
 */
void change_led_color(int out, int color) {
  int len = sizeof(CMD_LED_COLOR);

  if (I2C) {
    Wire.beginTransmission(I2C_ADRESS);
    for (int i = 0; i < len; i++) {
      Wire.write(CMD_LED_COLOR[i]);
    }
    Wire.write(out);
    Wire.write(color);
    Wire.endTransmission();
  } else {
    for (int i=0; i<len; i++) {
      Serial.write(CMD_LED_COLOR[i]);
      delay(64);
    }

    Serial.write(out);
    Serial.write(color);
    delay(64);
  }
}

/**
 * Write text to LCD
 * @param text 2 lines of characters
 */
void writeText(String text) {
  int len = sizeof(CMD_HOME);
  if (I2C) {
    //int t_len = text.length();
    //char data[t_len];
    //text.toCharArray(t_len, data);

    i2c_write(len, CMD_CLEAR_SCR);
    //i2c_write(len, CMD_HOME);
    //i2c_write(t_len, data);
    Serial.println("writeText: " + text);
  } else {
    for (int i=0; i<len; i++) {
      Serial.write(CMD_HOME[i]);
      delay(20);
    }

    len = text.length();

    for (int i=0; i<len; i++) {
      Serial.write(text[i]);
      delay(64);
    }
  }
}

/**
 * Handle display
 */
void toggleDisplay() {
  //if (count_up % 2 == 0) {
    //digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
    //delay(20);
    //digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  //}
  count_up++;

  if ((count_up >> 2) & 0x00000001) {
    count_up = 0;
    if (count_display % 2 == 0) {
      writeText(COMPANY);
    } else {
      writeText(ROBOT);
    }
    ++count_display;
  }
}


//////////////////////////////////////////////
//              Arduino API                 //
//////////////////////////////////////////////

void setup() {
  // configure the watchdog
  configure_wdt();
  power_adc_disable();

  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
  if (I2C) {
    Wire.begin();
    //Wire.onReceive(receiveEvent);
    Serial.begin(9600);
  } else {
    // initialize serial communication at 19200 bits per second:
    Serial.begin(19200);
  }

  delay(1000);

  reset();
}

void loop() {

  // put your main code here, to run repeatedly:

  currentTime = millis();
  if (currentTime - previousTime > INTERVAL) {
    toggleDisplay();
    previousTime = currentTime;
  }
}

void serialEvent() {
  while (Serial.available()) {
    char ch = (char) Serial.read();

    switch (ch) {
      case 'E':   // center
        reset();
        break;

      case 'B':   // up
        lcd_backlight += 10;
        if (lcd_backlight > 255)
          lcd_backlight = 255;
        dim_lcd(lcd_backlight);
        break;

      case 'H':   // down
        lcd_backlight -= 10;
        if (lcd_backlight < 0)
          lcd_backlight = 0;
        dim_lcd(lcd_backlight);
        break;

      case 'D':   // left
        key_backlight -= 10;
        if (key_backlight < 0)
          key_backlight = 0;
        dim_keypad(key_backlight);
        break;

      case 'C':   // right
        key_backlight += 10;
        if (key_backlight > 255)
          key_backlight = 255;
        else
        dim_keypad(key_backlight);
        break;

      case 'A':   // top left
        ++led_out;
        if (led_out > 2)
          led_out = 0;
        break;

      case 'G':   // bottom left
        ++led_color;
        if (led_color > 3)
          led_color = 0;

        change_led_color(led_out, led_color);
        break;

      default:
        break;
    }  // switch
  } // while

  //set_sleep(SLEEP_LCD_CMD);
}

void receiveEvent(int bytes) {
  int i, x;

  for (i = 0; i < bytes; i++) {
    x = Wire.read();
    Serial.write(x);
  }
}
