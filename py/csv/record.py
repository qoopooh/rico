#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Load all csv configuration files

This module is domonstration to show how to load csv file as configuration
"""


import csv

MOTORS = {}
SEQUENCES = {}
MOTOR_INFO = []
with open('rotate_position.csv', 'rb') as csvfile:
    print csvfile
    pos = csv.reader(csvfile)
    for row in pos:
        print ', '.join(row)
    MOTORS['rotate'] = pos


with open('arm_up_position.csv', 'rb') as csvfile:
    print '\n', csvfile
    pos = csv.reader(csvfile)
    for row in pos:
        print ', '.join(row)
    MOTORS['arm_up'] = pos

with open('arm_extension_position.csv', 'rb') as csvfile:
    print '\n', csvfile
    pos = csv.reader(csvfile)
    for row in pos:
        print ', '.join(row)
    MOTORS['arm_extension'] = pos

with open('gripper_position.csv', 'rb') as csvfile:
    print '\n', csvfile
    pos = csv.reader(csvfile)
    for row in pos:
        print ', '.join(row)
    MOTORS['gripper'] = pos

with open('rollback_position.csv', 'rb') as csvfile:
    print '\n', csvfile
    pos = csv.reader(csvfile)
    for row in pos:
        print ', '.join(row)
    MOTORS['rollback'] = pos

with open('connect_sequence.csv', 'rb') as csvfile:
    print '\n', csvfile
    pos = csv.reader(csvfile)
    for row in pos:
        print ', '.join(row)
    SEQUENCES['connect'] = pos

with open('disconnect_sequence.csv', 'rb') as csvfile:
    print '\n', csvfile
    pos = csv.reader(csvfile)
    for row in pos:
        print ', '.join(row)
    SEQUENCES['disconnect'] = pos

with open('MOTORS.csv', 'rb') as csvfile:
    MOTOR_INFO_list = csv.reader(csvfile)
    for row in MOTOR_INFO_list:
        MOTOR_INFO.append(row)

if __name__ == "__main__":
    print '\n', 'MOTORS', len(MOTORS)
    for key, value in MOTORS.iteritems():
        print '\t', key
    print '\n', 'SEQUENCES', len(SEQUENCES)
    for key, value in SEQUENCES.iteritems():
        print '\t', key
    print '\n', 'motor information', len(MOTOR_INFO)
    for row in MOTOR_INFO:
        print '\t', row
