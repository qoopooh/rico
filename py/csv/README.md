# Configuration file list
Configuration files are CSV (Comma Separated Values) file format. Engineer can change the value by using spreadsheet software (e.g. MS Excel)

* **motor** - motor configuration e.g. motor name, map file
* **rotate_position** - position mapping of rotate motor
* **arm_extension_position** - position mapping of arm extension motor
* **amr_up_position** - position mapping of arm up / down motor
* **gripper_position** - position mapping of gripper motor
* **rollback_position** - position mapping of rollback motor
* **connect_sequence** - robot sequence for connecting
* **disconnect_sequence** - robot sequence for disconnecting

# Descriptions
## Motor configuration
This file will tell:

1. **Motor code** - code name in software
2. **Motor name** - human readable format
3. **CAN message ID** - C2000 ID with hex format e.g. 0x001
4. **Axis number** - axis number of motor
5. **Backlash** - integer number
6. **Home direction** - set motor direction for home command as *cw* or *ccw*
7. **Encoder ratio**- integer number
8. **Position mapping file** - csv file name

|  Code  |  Name  | CAN ID | Axis No. | Backlash | Home Direction | Encoder Ratio | Map file
|--------|--------|--------|:--------:|:--------:|----------------|:-------------:|---------
| rotate | Rotate |  0x101 |    0     |    0     |      cw        |     0         | rotate_position.csv
| gripper| Gripper|  0x102 |    1     |    0     |      ccw       |     0         | gripper_position.csv


## Position configuration
This file will tell:

1. **position index** - always start at index 0
2. **position pulse number** - target position as pulse number
3. **motor speed** - setting speed (pulse per second)
4. **motor acceleration** - acceleration / deceleration time of motor in millisecond

| index | position | speed | acc |
|:-----:|:--------:|:-----:|:---:|
|   0   |     0    |  4000 | 100 |
|   1   |    6000  |  4000 | 100 |

### Rotate position
It has 144 positions. The index starts from 0 to 143.

### Arm extension position
* index 0: standby position
* index 1: skirt 1 position
* index 2: skirt 2 position
* index 3: adapter position
* index 4: connector position

### Arm up / down position
* index 0: connector (top) position
* index 1: under connector position
* index 2: skirt position
* index 3: adapter position

### Gripper position
* index 0: release position
* index 1: grip position

### Rollback position
* index 0: standby position
* index 1: high speed position
* index 2: stroke position


## Sequence configuration
This file describes robot movement.

1. **connect_sequence** - robot sequence for connecting
2. **disconnect_sequence** - robot sequence for disconnecting

### Target position index of rotate position
Since position index of rotate motor has to handle by user.
The sequence file will leave position index with special meaning:

* index -1: input position
* index -2: output position

| No. |  Motor Code   | Position Index |
|:---:|---------------|:--------------:|
|  1  |   rotate      |      -1        |
|  2  | arm_extension |       3        |
|  3  |   gripper     |       1        |
|  4  | arm_extension |       0        |
|  5  |   rotate      |      -2        |

