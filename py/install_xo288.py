#!/usr/bin/env python
"""xo288 module packaging

Revision history:
0.0.4 - Add home api
0.0.3 - Change robot home order: rollback before up/down
0.0.2 - Change home order: rollback before up/down
0.0.1 - Initial version
"""

import argparse
import os
import shutil

VERSION = (0, 0, 4)
version = '.'.join(map(str, VERSION))


PYTHON = 'python2.7'
MODULE_VERSION = '''from __future__ import absolute_import

VERSION = {}
__version__ = '.'.join(map(str, VERSION))
'''.format(VERSION)


parser = argparse.ArgumentParser(description='Install xo288 package into local library')
parser.add_argument('-l', '--lib', help='lib folder e.g. $HOME/git/rico/py/web/cell/env/lib')
parser.add_argument('-u', '--usr', action='store_true', help='Install package to /usr/lib/python2.7/')

def copy(lib):
    if PYTHON in lib:
        d = os.path.join(lib, 'xo288')
    else:
        d = os.path.join(lib, PYTHON, 'site-packages', 'xo288')

    if not os.path.exists(d):
        os.makedirs(d)

    init = os.path.join(d, '__init__.py')
    if not os.path.exists(init):
        with open(init, 'a'):
            os.utime(init, None)
    with open(init, 'w') as f:
        f.write(MODULE_VERSION)

    for i in ('cmd', 'can', 'csv'):
        target = os.path.join(d, i)
        if os.path.exists(target):
            shutil.rmtree(target)
        shutil.copytree(i, target)

if __name__ == '__main__':
    a = parser.parse_args()

    if a.lib:
        if not os.path.exists(a.lib):
            print('Error: Path is not exists.')
            sys.exit(1)
        copy(a.lib)
        print('set to {} (version {})'.format(a.lib, version))

    elif a.usr:
        copy('/usr/lib/' + PYTHON)
        print('done (version {})'.format(version))

    else:
        parser.print_help()

