#!/usr/bin/env python

import sys
import getopt

def parse_param():
    port = ''
    device = ''
    kernel = ''
    application = ''
    help_text = "flash_programmer.py -p <COMxx> -d <device> -k <kernel> -a <application>"
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hp:d:k:a:", ["port=", "device=", "kernel=", "application="])
    except getopt.GetoptError:
        print help_text
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print help_text
            sys.exit()
        elif opt in ('-p', '--port'):
            port = arg
        elif opt in ('-d', '--device'):
            device = arg
        elif opt in ('-k', '--kernel'):
            kernel = arg
        elif opt in ('-a', '--application'):
            application = arg

    return port, device, kernel, application

if __name__ == '__main__':
    print parse_param()

