This is sandbox repo.

## Rico Board
It is [arm cortext-A9 board](http://www.myirtech.com/list.asp?id=510) that uses TI AM437x cpu.
We try to use this board as host device, it can communicate to client device via [CAN bus](https://en.wikipedia.org/wiki/CAN_bus).

## TODO
1. Update motor sequence of home
1. Run debug mode on alpha.py
1. Logging
1. Check motor response
1. Absoluted position

## Experimental
Testing is mandatory.

### eMMC
    Device Boot      Start         End      Blocks   Id  System
    /dev/mmcblk0p1   *          63      144584       72261    c  W95 FAT32 (LBA)
    /dev/mmcblk0p2          160650     3212999     1526175   83  Linux
    /dev/mmcblk0p3         3213000     7550549     2168775    c  W95 FAT32 (LBA)

