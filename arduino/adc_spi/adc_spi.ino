
// inslude the SPI library:
#include <SPI.h>

const int inPin = 6;
const int chipSelectPin = 7;
const int MAX_CHANNEL = 7;

int success = -1;
byte channel = 0; // max is 15
long analog = 0;
int running = HIGH;
int sw_reading = HIGH;
int sw_previous = HIGH;
long sw_time = 0;
long sw_debounce = 200;
long conv_time = 0;

void setup() {

  Serial.begin(9600);

  pinMode(inPin, INPUT);
  pinMode(chipSelectPin, OUTPUT);
  // initialize SPI: led cannot use
  SPI.begin();
  setupSingleEndMode();
}


void loop() {
  // check breaking
  sw_reading = digitalRead(inPin);
  if (sw_reading == HIGH && sw_previous == LOW && (millis() - sw_time) > sw_debounce) {
    if (running == HIGH)
      running = LOW;
    else
      running = HIGH;

    sw_time = millis();
  }
  sw_previous = sw_reading;

  if (running == LOW)
    return;

  conv_time = millis();

  for (channel = 0; channel <= MAX_CHANNEL; channel++) {
    String ch = String(channel);

    delay(150);
    success = writeInput(channel, &analog);
    String msg = "";
    if (success >= 0) {
      msg = "\tAnalog[" + ch + "] = " + String(analog);
    } else {
      msg = "Failed " + ch;
    }
    Serial.println(msg);
  }
  running = LOW;

  Serial.println("Conversion time for " + String(channel)
      + " channels: " + String(millis() - conv_time) + " ms");

  Serial.println("\nPlease press button on pin 6:");
} // loop


void setupSingleEndMode() {
  delay(100);         // worm up SPI

  // take the chip select low to select the device:
  digitalWrite(chipSelectPin, LOW);

  SPI.transfer(0xB0); // EN, SGL, channel 0
  SPI.transfer(0x80); // External input, Gain = 1, Auto-Calibration
  SPI.transfer(0x00); // DON'T CARE

  // take the chip select high to de-select:
  digitalWrite(chipSelectPin, HIGH);

  delay(164);         // Max conversion time for 1x speed mode
                      // 50 Hz mode => 163.5 ms
}

int writeInput(byte ch, long *analog) {
  int success = -1;

  // Single end, keep previous settings
  byte dataToSend = 0xB0 | ((ch & 0x0E) >> 1);
  if ((ch & 0x01) == 0x01) {
    dataToSend |= 0x08;
  }
  
  // take the chip select low to select the device:
  digitalWrite(chipSelectPin, LOW);

  byte rcv_1st = SPI.transfer(dataToSend);   // Send selected input
  byte rcv_2nd = SPI.transfer(0x00);         // Ignore data
  byte rcv_3rd = SPI.transfer(0x00);         // Ignore data


  // TODO: remove it
  Serial.print(rcv_1st, HEX);
  Serial.print(' ');
  Serial.print(rcv_2nd, HEX);
  Serial.print(' ');
  Serial.print(rcv_3rd, HEX);
  Serial.print(": ");
  // remove it


  if ((rcv_1st & 0x80) == 0x80) {
    Serial.print("Conversion processing: ");
  } else if ((rcv_1st & 0x40) == 0x40) {
    Serial.print("Dummy bit is not zero: ");
  } else {
    byte conj = rcv_1st & 0x30;
    if (conj == 0x30) {
      Serial.print("Overrange value: ");
    } else if (conj == 0x00) {
      Serial.print("Underrange value: ");
    } else {
      if ((rcv_1st & 0x20) == 0x00) {
        Serial.print("Negative value: ");
      }

      long a = 0;
      a = (rcv_1st & 0x1F);
      a = (a << 8) | rcv_2nd;
      a = (a << 4) | (rcv_3rd >> 4);
      *analog = a;

      success = 1;
    }
  }

  // take the chip select high to de-select:
  digitalWrite(chipSelectPin, HIGH);
  
  return success;
}


