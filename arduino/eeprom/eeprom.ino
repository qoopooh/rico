#include <avr/wdt.h>            // library for default watchdog functions
#include <avr/interrupt.h>      // library for interrupts handling
#include <avr/sleep.h>          // library for sleep
#include <avr/power.h>          // library for power control
#include <Wire.h>

// constants
const bool I2C = true;          // A4 (SDA), A5 (SCL)
const int I2C_ADRESS = 0x50;
const long INTERVAL = 1000;
const int LCD_BRIGHTNESS = 36;
const int KEYPAD_BRIGHTNESS = 0;
const int SLEEP_LCD_CMD = 1;
const String COMPANY  = "    XENOptics  \n                ";
const String ROBOT    = "    XSOS-288   \n                ";
const unsigned char CMD_DIM_LCD[]     = { 254, 153 };
const unsigned char CMD_DIM_KEYPAD[]  = { 254, 156 };
const unsigned char CMD_HOME[]  = { 254, 72 };
const unsigned char CMD_CLEAR_SCR[]  = { 254, 88 };
const unsigned char CMD_LED_COLOR[]  = { 254, 90 };
const unsigned char CMD_LED0_OFF[]  = { 254, 90, 0, 0 };
const unsigned char CMD_LED1_OFF[]  = { 254, 90, 1, 0 };
const unsigned char CMD_LED2_OFF[]  = { 254, 90, 2, 0 };
const unsigned char CMD_LCD_DIM_BACKLIGHT[]  = { 254, 153, LCD_BRIGHTNESS };
const unsigned char CMD_KEY_DIM_BACKLIGHT[]  = { 254, 156, KEYPAD_BRIGHTNESS };
const unsigned char CLEAR_BULK[] = {
  254, 90, 0, 0,  // turn off led 0
  254, 90, 1, 0,  // turn off led 1
  254, 90, 2, 0,  // turn off led 2
  254, 153, LCD_BRIGHTNESS,     // dim lcd backlight
  254, 156, KEYPAD_BRIGHTNESS,  // dim keypad backlight
};

// variables
int count_up  = 0;
int count_display  = 0;
int lcd_backlight = LCD_BRIGHTNESS;
int key_backlight = KEYPAD_BRIGHTNESS;
int led_out   = 0;  // 0 - 2
int led_color = 0;  // 0 - 3
unsigned long currentTime;
unsigned long previousTime;
int nbr_remaining;  // how many times remain to sleep before wake up

void readID() {
  Wire.beginTransmission(I2C_ADRESS);
  Wire.write(0);
  Wire.endTransmission();

  Wire.requestFrom(I2C_ADRESS, 2);

  while (Wire.available()) {
    int c = Wire.read();
    Serial.print(c, HEX);
  }
  Serial.println("\nEnd");
}


void writeID(int msb, int lsb) {
  static bool written = false;

  Serial.println("\nwriteID");
  if (written)
    return;

  Wire.beginTransmission(I2C_ADRESS);
  Wire.write(0);
  Wire.write(msb);
  Wire.write(lsb);
  Wire.endTransmission();


  Serial.println("written");
  written = true;
}

//////////////////////////////////////////////
//              Arduino API                 //
//////////////////////////////////////////////

void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);

  Wire.begin();
  //Wire.onReceive(receiveEvent);

  Serial.begin(9600);
  Serial.println("setup");
}

void loop() {
  writeID(0x01, 0x01);
  //writeID(0x5A, 0xF0);

  readID();

  delay(1000);
}

