#!/usr/bin/env node

var myArgs = process.argv.slice(2);
var LED2_PATH = "/sys/class/leds/status_led2";

function writeLED( filename, value, path ){
  var fs = require('fs');
  try {
    fs.writeFileSync( path + filename, value );
  } catch (err) {
    console.log('The Write Failed to the file: ' + path + filename);
  }
}

function removeTrigger() {
  writeLED("/trigger", "none", LED2_PATH);
}

console.log('Starting the LED Node.js Program');
if (myArgs[0] == null) {
  console.log( "There are an incorrect number of arguments" );
  console.log( "  usage is: " + __filename + " command" );
  console.log( "  where command is one of on, off, flash or status." );
  process.exit(2);
}

switch (myArgs[0]) {
  case 'on':
    console.log('Turning the LED on');
    removeTrigger();
    writeLED('/brightness', '1', LED2_PATH);
    break;
  case 'off':
    console.log('Turning the LED off');
    removeTrigger();
    writeLED('/brightness', '0', LED2_PATH);
    break;
  case 'flash':
    console.log('Making the LED flash');
    writeLED('/trigger', 'timer', LED2_PATH);
    writeLED('/delay_on', '50', LED2_PATH);
    writeLED('/delay_off', '50', LED2_PATH);
    break;
  case 'status':
    console.log('Getting the LED status');
    var fs = require('fs');
    fs.readFile(LED2_PATH+'/trigger', 'utf8', function(err, data){
      if (err) { return console.log(err); }
      console.log('data');
    });
    break;
  default:
    console.log('Invalid command!!!');
    break;
}


