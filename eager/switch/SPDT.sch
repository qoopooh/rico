<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="8.1.1">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="VCC">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VCC" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="VCC" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="switch">
<description>&lt;b&gt;Switches&lt;/b&gt;&lt;p&gt;
Marquardt, Siemens, C&amp;K, ITT, and others&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="SKHMPXE010">
<description>&lt;b&gt;6.2 X 6.5mm TACT Switch (SMD)&lt;/b&gt;&lt;p&gt;
Source: http://www3.alps.co.jp/WebObjects/catalog.woa/PDF/E/Switch/Tact/SKHM/SKHM.PDF</description>
<wire x1="-2.7606" y1="2.9981" x2="2.7606" y2="2.9981" width="0.1016" layer="21"/>
<wire x1="2.7606" y1="2.9981" x2="2.7606" y2="0.9944" width="0.1016" layer="21"/>
<wire x1="2.7606" y1="0.9944" x2="3.1762" y2="0.9944" width="0.1016" layer="21"/>
<wire x1="3.1762" y1="0.9944" x2="3.1762" y2="-0.9796" width="0.1016" layer="21"/>
<wire x1="3.1762" y1="-0.9796" x2="2.7606" y2="-0.9796" width="0.1016" layer="21"/>
<wire x1="2.7606" y1="-0.9796" x2="2.7606" y2="-2.9981" width="0.1016" layer="21"/>
<wire x1="2.7606" y1="-2.9981" x2="-2.7606" y2="-2.9981" width="0.1016" layer="21"/>
<wire x1="-2.7606" y1="-2.9981" x2="-2.7606" y2="-1.3358" width="0.1016" layer="21"/>
<wire x1="-2.7606" y1="-1.3358" x2="-2.8794" y2="-1.217" width="0.1016" layer="21"/>
<wire x1="-2.8794" y1="-1.217" x2="-2.8794" y2="1.2022" width="0.1016" layer="21"/>
<wire x1="-2.8794" y1="1.2022" x2="-2.7606" y2="1.321" width="0.1016" layer="21"/>
<wire x1="-2.7606" y1="1.321" x2="-2.7606" y2="2.9981" width="0.1016" layer="21"/>
<wire x1="2.7161" y1="-2.7606" x2="1.0686" y2="-2.7606" width="0.1016" layer="21"/>
<wire x1="1.0686" y1="-2.7606" x2="1.0686" y2="-2.6567" width="0.1016" layer="21"/>
<wire x1="1.0686" y1="-2.6567" x2="-1.0835" y2="-2.6567" width="0.1016" layer="21"/>
<wire x1="-1.0835" y1="-2.6567" x2="-1.0835" y2="-2.9387" width="0.1016" layer="21"/>
<wire x1="-1.128" y1="-2.7606" x2="-2.7012" y2="-2.7606" width="0.1016" layer="21"/>
<wire x1="1.0686" y1="-2.7606" x2="1.0686" y2="-2.9535" width="0.1016" layer="21"/>
<wire x1="2.7161" y1="2.7606" x2="1.0686" y2="2.7606" width="0.1016" layer="21"/>
<wire x1="1.0686" y1="2.7606" x2="1.0686" y2="2.6567" width="0.1016" layer="21"/>
<wire x1="1.0686" y1="2.6567" x2="-1.0835" y2="2.6567" width="0.1016" layer="21"/>
<wire x1="-1.0835" y1="2.6567" x2="-1.0835" y2="2.9387" width="0.1016" layer="21"/>
<wire x1="-1.128" y1="2.7606" x2="-2.7012" y2="2.7606" width="0.1016" layer="21"/>
<wire x1="1.0686" y1="2.7606" x2="1.0686" y2="2.9536" width="0.1016" layer="21"/>
<circle x="0" y="0" radius="1.2764" width="0.1016" layer="21"/>
<circle x="0" y="0" radius="1.6475" width="0.1016" layer="21"/>
<smd name="3" x="-4.2" y="-2.25" dx="1.6" dy="1.4" layer="1"/>
<smd name="4" x="4.2" y="-2.25" dx="1.6" dy="1.4" layer="1"/>
<smd name="5" x="4.2" y="0" dx="1.6" dy="1.4" layer="1"/>
<smd name="2" x="4.2" y="2.25" dx="1.6" dy="1.4" layer="1"/>
<smd name="1" x="-4.2" y="2.25" dx="1.6" dy="1.4" layer="1"/>
<text x="-2.54" y="3.175" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.9925" y1="1.6772" x2="-2.7755" y2="2.4341" layer="51"/>
<rectangle x1="-3.9925" y1="-2.4341" x2="-2.7903" y2="-1.6623" layer="51"/>
<rectangle x1="2.7755" y1="-2.4192" x2="3.9925" y2="-1.6623" layer="51"/>
<rectangle x1="3.2059" y1="-0.371" x2="3.8589" y2="0.3859" layer="51"/>
<rectangle x1="2.7903" y1="1.6772" x2="3.9925" y2="2.4341" layer="51"/>
<rectangle x1="-3.3246" y1="1.6771" x2="-2.7755" y2="2.4341" layer="21"/>
<rectangle x1="-3.3246" y1="-2.4341" x2="-2.7903" y2="-1.6623" layer="21"/>
<rectangle x1="2.7903" y1="1.6771" x2="3.3246" y2="2.4341" layer="21"/>
<rectangle x1="3.191" y1="-0.371" x2="3.3246" y2="0.3859" layer="21"/>
<rectangle x1="2.7755" y1="-2.4192" x2="3.3246" y2="-1.6623" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="NORMOPEN_SHIELD">
<wire x1="-2.54" y1="0" x2="-2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="3.048" y2="1.778" width="0.2032" layer="94"/>
<wire x1="3.048" y1="0.508" x2="3.048" y2="0" width="0.2032" layer="94"/>
<wire x1="0.762" y1="1.016" x2="0.762" y2="1.524" width="0.2032" layer="94"/>
<wire x1="0.762" y1="2.032" x2="0.762" y2="2.794" width="0.2032" layer="94"/>
<wire x1="0.762" y1="2.794" x2="0.762" y2="3.048" width="0.2032" layer="94"/>
<wire x1="0.762" y1="3.556" x2="0.762" y2="4.064" width="0.2032" layer="94"/>
<wire x1="1.27" y1="4.064" x2="0.762" y2="4.064" width="0.2032" layer="94"/>
<wire x1="0.762" y1="4.064" x2="0.254" y2="4.064" width="0.2032" layer="94"/>
<wire x1="0.254" y1="2.286" x2="0.762" y2="2.794" width="0.1524" layer="94"/>
<wire x1="0.762" y1="2.794" x2="1.27" y2="2.286" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.2032" layer="94"/>
<wire x1="3.048" y1="0" x2="5.08" y2="0" width="0.2032" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.286" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-2.286" y1="-5.08" x2="-2.286" y2="-5.588" width="0.1524" layer="94"/>
<wire x1="-3.048" y1="-5.588" x2="-1.524" y2="-5.588" width="0.1524" layer="94"/>
<wire x1="-2.794" y1="-5.842" x2="-1.778" y2="-5.842" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-6.096" x2="-2.032" y2="-6.096" width="0.1524" layer="94"/>
<circle x="-2.54" y="0" radius="0.508" width="0" layer="94"/>
<circle x="5.08" y="0" radius="0.508" width="0" layer="94"/>
<text x="-2.54" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="0" y="-6.35" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1.1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="1.2" x="-5.08" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2.2" x="7.62" y="-2.54" visible="off" length="short" direction="pas" swaplevel="2" rot="R180"/>
<pin name="2.1" x="7.62" y="0" visible="off" length="short" direction="pas" swaplevel="2" rot="R180"/>
<pin name="SH" x="-5.08" y="-5.08" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SKHMP*E010" prefix="S">
<description>&lt;b&gt;6.2 X 6.5mm TACT Switch (SMD)&lt;/b&gt;&lt;p&gt;
Source: http://www3.alps.co.jp/WebObjects/catalog.woa/PDF/E/Switch/Tact/SKHM/SKHM.PDF</description>
<gates>
<gate name="G$1" symbol="NORMOPEN_SHIELD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SKHMPXE010">
<connects>
<connect gate="G$1" pin="1.1" pad="1"/>
<connect gate="G$1" pin="1.2" pad="2"/>
<connect gate="G$1" pin="2.1" pad="3"/>
<connect gate="G$1" pin="2.2" pad="4"/>
<connect gate="G$1" pin="SH" pad="5"/>
</connects>
<technologies>
<technology name="S"/>
<technology name="U"/>
<technology name="W"/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="switch-dil">
<description>&lt;b&gt;DIL Switches and Code Switches&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="CTS-219-05J">
<description>&lt;b&gt;Surface Mount DIP Switch Series 219 SMT "J" Bend&lt;/b&gt;&lt;p&gt;
CTS Electronic Components&lt;br&gt;
Sourc: www.ctscorp.com</description>
<wire x1="-6.985" y1="3.225" x2="-6.43" y2="3.225" width="0.2032" layer="21"/>
<wire x1="-6.985" y1="-3.225" x2="-6.985" y2="3.225" width="0.2032" layer="21"/>
<wire x1="-6.43" y1="-3.225" x2="-5.735" y2="-3.225" width="0.2032" layer="21"/>
<wire x1="-5.735" y1="-3.225" x2="-6.985" y2="-3.225" width="0.2032" layer="21"/>
<wire x1="-5.83" y1="2" x2="-4.33" y2="2" width="0.2032" layer="51"/>
<wire x1="-4.33" y1="2" x2="-4.33" y2="-2" width="0.2032" layer="21"/>
<wire x1="-4.33" y1="-2" x2="-5.83" y2="-2" width="0.2032" layer="51"/>
<wire x1="-5.83" y1="-2" x2="-5.83" y2="2" width="0.2032" layer="21"/>
<wire x1="-6.96" y1="-2" x2="-5.735" y2="-3.225" width="0.2032" layer="21"/>
<wire x1="6.96" y1="-3.225" x2="-6.985" y2="-3.225" width="0.2032" layer="51"/>
<wire x1="6.96" y1="3.225" x2="6.96" y2="-3.225" width="0.2032" layer="21"/>
<wire x1="6.96" y1="-3.225" x2="6.405" y2="-3.225" width="0.2032" layer="21"/>
<wire x1="6.405" y1="3.225" x2="6.96" y2="3.225" width="0.2032" layer="21"/>
<wire x1="-6.985" y1="3.225" x2="6.96" y2="3.225" width="0.2032" layer="51"/>
<wire x1="-1.35" y1="-3.225" x2="-1.215" y2="-3.225" width="0.2032" layer="21"/>
<wire x1="-3.89" y1="-3.225" x2="-3.755" y2="-3.225" width="0.2032" layer="21"/>
<wire x1="-3.29" y1="2" x2="-1.79" y2="2" width="0.2032" layer="51"/>
<wire x1="-1.79" y1="2" x2="-1.79" y2="-2" width="0.2032" layer="21"/>
<wire x1="-1.79" y1="-2" x2="-3.29" y2="-2" width="0.2032" layer="51"/>
<wire x1="-3.29" y1="-2" x2="-3.29" y2="2" width="0.2032" layer="21"/>
<wire x1="-0.75" y1="2" x2="0.75" y2="2" width="0.2032" layer="51"/>
<wire x1="0.75" y1="2" x2="0.75" y2="-2" width="0.2032" layer="21"/>
<wire x1="0.75" y1="-2" x2="-0.75" y2="-2" width="0.2032" layer="51"/>
<wire x1="-0.75" y1="-2" x2="-0.75" y2="2" width="0.2032" layer="21"/>
<wire x1="1.19" y1="-3.225" x2="1.325" y2="-3.225" width="0.2032" layer="21"/>
<wire x1="1.79" y1="2" x2="3.29" y2="2" width="0.2032" layer="51"/>
<wire x1="3.29" y1="2" x2="3.29" y2="-2" width="0.2032" layer="21"/>
<wire x1="3.29" y1="-2" x2="1.79" y2="-2" width="0.2032" layer="51"/>
<wire x1="1.79" y1="-2" x2="1.79" y2="2" width="0.2032" layer="21"/>
<wire x1="3.73" y1="-3.225" x2="3.865" y2="-3.225" width="0.2032" layer="21"/>
<wire x1="4.33" y1="2" x2="5.83" y2="2" width="0.2032" layer="51"/>
<wire x1="5.83" y1="2" x2="5.83" y2="-2" width="0.2032" layer="21"/>
<wire x1="5.83" y1="-2" x2="4.33" y2="-2" width="0.2032" layer="51"/>
<wire x1="4.33" y1="-2" x2="4.33" y2="2" width="0.2032" layer="21"/>
<wire x1="1.35" y1="3.225" x2="1.215" y2="3.225" width="0.2032" layer="21"/>
<wire x1="3.89" y1="3.225" x2="3.755" y2="3.225" width="0.2032" layer="21"/>
<wire x1="-1.19" y1="3.225" x2="-1.325" y2="3.225" width="0.2032" layer="21"/>
<wire x1="-3.73" y1="3.225" x2="-3.865" y2="3.225" width="0.2032" layer="21"/>
<smd name="1" x="-5.08" y="-3.215" dx="1.13" dy="2.44" layer="1"/>
<smd name="2" x="-2.54" y="-3.215" dx="1.13" dy="2.44" layer="1"/>
<smd name="3" x="0" y="-3.215" dx="1.13" dy="2.44" layer="1"/>
<smd name="4" x="2.54" y="-3.215" dx="1.13" dy="2.44" layer="1"/>
<smd name="5" x="5.08" y="-3.215" dx="1.13" dy="2.44" layer="1"/>
<smd name="6" x="5.08" y="3.215" dx="1.13" dy="2.44" layer="1" rot="R180"/>
<smd name="7" x="2.54" y="3.215" dx="1.13" dy="2.44" layer="1" rot="R180"/>
<smd name="8" x="0" y="3.215" dx="1.13" dy="2.44" layer="1" rot="R180"/>
<smd name="9" x="-2.54" y="3.215" dx="1.13" dy="2.44" layer="1" rot="R180"/>
<smd name="10" x="-5.08" y="3.215" dx="1.13" dy="2.44" layer="1" rot="R180"/>
<text x="-7.85" y="-2.5" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="-5.58" y="2.275" size="0.6096" layer="51">ON</text>
<text x="-5.28" y="-2.85" size="0.6096" layer="51">1</text>
<text x="8.85" y="-3" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-2.74" y="-2.85" size="0.6096" layer="51">2</text>
<text x="-0.2" y="-2.85" size="0.6096" layer="51">3</text>
<text x="2.34" y="-2.85" size="0.6096" layer="51">4</text>
<text x="4.88" y="-2.85" size="0.6096" layer="51">5</text>
<rectangle x1="-5.83" y1="-4" x2="-4.33" y2="-3.25" layer="51" rot="R180"/>
<rectangle x1="-5.83" y1="-2" x2="-4.33" y2="-0.5" layer="51"/>
<rectangle x1="-0.75" y1="-4" x2="0.75" y2="-3.25" layer="51" rot="R180"/>
<rectangle x1="-3.29" y1="-4" x2="-1.79" y2="-3.25" layer="51" rot="R180"/>
<rectangle x1="-3.29" y1="-2" x2="-1.79" y2="-0.5" layer="51"/>
<rectangle x1="-0.75" y1="-2" x2="0.75" y2="-0.5" layer="51"/>
<rectangle x1="1.79" y1="-2" x2="3.29" y2="-0.5" layer="51"/>
<rectangle x1="4.33" y1="-2" x2="5.83" y2="-0.5" layer="51"/>
<rectangle x1="1.79" y1="-4" x2="3.29" y2="-3.25" layer="51" rot="R180"/>
<rectangle x1="4.33" y1="-4" x2="5.83" y2="-3.25" layer="51" rot="R180"/>
<rectangle x1="4.33" y1="3.25" x2="5.83" y2="4" layer="51"/>
<rectangle x1="-0.75" y1="3.25" x2="0.75" y2="4" layer="51"/>
<rectangle x1="1.79" y1="3.25" x2="3.29" y2="4" layer="51"/>
<rectangle x1="-3.29" y1="3.25" x2="-1.79" y2="4" layer="51"/>
<rectangle x1="-5.83" y1="3.25" x2="-4.33" y2="4" layer="51"/>
</package>
<package name="CTS-219-05">
<description>&lt;b&gt;Surface Mount DIP Switch Series 219 SMT&lt;/b&gt;&lt;p&gt;
CTS Electronic Components&lt;br&gt;
Sourc: www.ctscorp.com</description>
<wire x1="-6.985" y1="3.225" x2="-6.43" y2="3.225" width="0.2032" layer="21"/>
<wire x1="-6.985" y1="-3.225" x2="-6.985" y2="3.225" width="0.2032" layer="21"/>
<wire x1="-6.43" y1="-3.225" x2="-5.735" y2="-3.225" width="0.2032" layer="21"/>
<wire x1="-5.735" y1="-3.225" x2="-6.985" y2="-3.225" width="0.2032" layer="21"/>
<wire x1="-5.83" y1="2" x2="-4.33" y2="2" width="0.2032" layer="21"/>
<wire x1="-4.33" y1="2" x2="-4.33" y2="-2" width="0.2032" layer="21"/>
<wire x1="-4.33" y1="-2" x2="-5.83" y2="-2" width="0.2032" layer="21"/>
<wire x1="-5.83" y1="-2" x2="-5.83" y2="2" width="0.2032" layer="21"/>
<wire x1="-6.96" y1="-2" x2="-5.735" y2="-3.225" width="0.2032" layer="21"/>
<wire x1="6.96" y1="-3.225" x2="-6.985" y2="-3.225" width="0.2032" layer="51"/>
<wire x1="6.96" y1="3.225" x2="6.96" y2="-3.225" width="0.2032" layer="21"/>
<wire x1="6.96" y1="-3.225" x2="6.405" y2="-3.225" width="0.2032" layer="21"/>
<wire x1="6.405" y1="3.225" x2="6.96" y2="3.225" width="0.2032" layer="21"/>
<wire x1="-6.985" y1="3.225" x2="6.96" y2="3.225" width="0.2032" layer="51"/>
<wire x1="-3.755" y1="3.225" x2="-3.89" y2="3.225" width="0.2032" layer="21"/>
<wire x1="-1.35" y1="-3.225" x2="-1.215" y2="-3.225" width="0.2032" layer="21"/>
<wire x1="-1.215" y1="3.225" x2="-1.35" y2="3.225" width="0.2032" layer="21"/>
<wire x1="-3.89" y1="-3.225" x2="-3.755" y2="-3.225" width="0.2032" layer="21"/>
<wire x1="-3.29" y1="2" x2="-1.79" y2="2" width="0.2032" layer="21"/>
<wire x1="-1.79" y1="2" x2="-1.79" y2="-2" width="0.2032" layer="21"/>
<wire x1="-1.79" y1="-2" x2="-3.29" y2="-2" width="0.2032" layer="21"/>
<wire x1="-3.29" y1="-2" x2="-3.29" y2="2" width="0.2032" layer="21"/>
<wire x1="-0.75" y1="2" x2="0.75" y2="2" width="0.2032" layer="21"/>
<wire x1="0.75" y1="2" x2="0.75" y2="-2" width="0.2032" layer="21"/>
<wire x1="0.75" y1="-2" x2="-0.75" y2="-2" width="0.2032" layer="21"/>
<wire x1="-0.75" y1="-2" x2="-0.75" y2="2" width="0.2032" layer="21"/>
<wire x1="1.19" y1="-3.225" x2="1.325" y2="-3.225" width="0.2032" layer="21"/>
<wire x1="1.325" y1="3.225" x2="1.19" y2="3.225" width="0.2032" layer="21"/>
<wire x1="1.79" y1="2" x2="3.29" y2="2" width="0.2032" layer="21"/>
<wire x1="3.29" y1="2" x2="3.29" y2="-2" width="0.2032" layer="21"/>
<wire x1="3.29" y1="-2" x2="1.79" y2="-2" width="0.2032" layer="21"/>
<wire x1="1.79" y1="-2" x2="1.79" y2="2" width="0.2032" layer="21"/>
<wire x1="3.73" y1="-3.225" x2="3.865" y2="-3.225" width="0.2032" layer="21"/>
<wire x1="3.865" y1="3.225" x2="3.73" y2="3.225" width="0.2032" layer="21"/>
<wire x1="4.33" y1="2" x2="5.83" y2="2" width="0.2032" layer="21"/>
<wire x1="5.83" y1="2" x2="5.83" y2="-2" width="0.2032" layer="21"/>
<wire x1="5.83" y1="-2" x2="4.33" y2="-2" width="0.2032" layer="21"/>
<wire x1="4.33" y1="-2" x2="4.33" y2="2" width="0.2032" layer="21"/>
<smd name="1" x="-5.08" y="-4.3" dx="1.13" dy="2.44" layer="1"/>
<smd name="2" x="-2.54" y="-4.3" dx="1.13" dy="2.44" layer="1"/>
<smd name="3" x="0" y="-4.3" dx="1.13" dy="2.44" layer="1"/>
<smd name="4" x="2.54" y="-4.3" dx="1.13" dy="2.44" layer="1"/>
<smd name="5" x="5.08" y="-4.3" dx="1.13" dy="2.44" layer="1"/>
<smd name="6" x="5.08" y="4.3" dx="1.13" dy="2.44" layer="1" rot="R180"/>
<smd name="7" x="2.54" y="4.3" dx="1.13" dy="2.44" layer="1" rot="R180"/>
<smd name="8" x="0" y="4.3" dx="1.13" dy="2.44" layer="1" rot="R180"/>
<smd name="9" x="-2.54" y="4.3" dx="1.13" dy="2.44" layer="1" rot="R180"/>
<smd name="10" x="-5.08" y="4.3" dx="1.13" dy="2.44" layer="1" rot="R180"/>
<text x="-7.85" y="-2.5" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="-5.58" y="2.275" size="0.6096" layer="21">ON</text>
<text x="-5.28" y="-2.85" size="0.6096" layer="21">1</text>
<text x="8.85" y="-3" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-2.74" y="-2.85" size="0.6096" layer="21">2</text>
<text x="-0.2" y="-2.85" size="0.6096" layer="21">3</text>
<text x="2.34" y="-2.85" size="0.6096" layer="21">4</text>
<text x="4.88" y="-2.85" size="0.6096" layer="21">5</text>
<rectangle x1="-5.83" y1="-4" x2="-4.33" y2="-3.25" layer="51" rot="R180"/>
<rectangle x1="-5.83" y1="-2" x2="-4.33" y2="-0.5" layer="21"/>
<rectangle x1="-0.75" y1="-4" x2="0.75" y2="-3.25" layer="51" rot="R180"/>
<rectangle x1="-3.29" y1="-4" x2="-1.79" y2="-3.25" layer="51" rot="R180"/>
<rectangle x1="-3.29" y1="-2" x2="-1.79" y2="-0.5" layer="21"/>
<rectangle x1="-0.75" y1="-2" x2="0.75" y2="-0.5" layer="21"/>
<rectangle x1="1.79" y1="-2" x2="3.29" y2="-0.5" layer="21"/>
<rectangle x1="4.33" y1="-2" x2="5.83" y2="-0.5" layer="21"/>
<rectangle x1="1.79" y1="-4" x2="3.29" y2="-3.25" layer="51" rot="R180"/>
<rectangle x1="4.33" y1="-4" x2="5.83" y2="-3.25" layer="51" rot="R180"/>
<rectangle x1="4.33" y1="3.25" x2="5.83" y2="4" layer="51"/>
<rectangle x1="-0.75" y1="3.25" x2="0.75" y2="4" layer="51"/>
<rectangle x1="1.79" y1="3.25" x2="3.29" y2="4" layer="51"/>
<rectangle x1="-3.29" y1="3.25" x2="-1.79" y2="4" layer="51"/>
<rectangle x1="-5.83" y1="3.25" x2="-4.33" y2="4" layer="51"/>
<rectangle x1="-5.33" y1="4" x2="-4.83" y2="4.95" layer="51" rot="R180"/>
<rectangle x1="-5.33" y1="-4.95" x2="-4.83" y2="-4" layer="51"/>
<rectangle x1="-2.79" y1="4" x2="-2.29" y2="4.95" layer="51" rot="R180"/>
<rectangle x1="-2.79" y1="-4.95" x2="-2.29" y2="-4" layer="51"/>
<rectangle x1="-0.25" y1="4" x2="0.25" y2="4.95" layer="51" rot="R180"/>
<rectangle x1="-0.25" y1="-4.95" x2="0.25" y2="-4" layer="51"/>
<rectangle x1="2.29" y1="4" x2="2.79" y2="4.95" layer="51" rot="R180"/>
<rectangle x1="2.29" y1="-4.95" x2="2.79" y2="-4" layer="51"/>
<rectangle x1="4.83" y1="4" x2="5.33" y2="4.95" layer="51" rot="R180"/>
<rectangle x1="4.83" y1="-4.95" x2="5.33" y2="-4" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="DIP05">
<wire x1="6.985" y1="5.08" x2="-6.985" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.985" y1="5.08" x2="-6.985" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-6.985" y1="-5.08" x2="6.985" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="6.985" y1="-5.08" x2="6.985" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-4.572" y1="-2.54" x2="-5.588" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-5.588" y1="2.54" x2="-4.572" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-4.572" y1="2.54" x2="-4.572" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-5.588" y1="-2.54" x2="-5.588" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-2.54" x2="-3.048" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-3.048" y1="2.54" x2="-2.032" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="2.54" x2="-2.032" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-3.048" y1="-2.54" x2="-3.048" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0.508" y1="-2.54" x2="-0.508" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-0.508" y1="2.54" x2="0.508" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0.508" y1="2.54" x2="0.508" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-0.508" y1="-2.54" x2="-0.508" y2="2.54" width="0.1524" layer="94"/>
<wire x1="3.048" y1="-2.54" x2="2.032" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.032" y1="2.54" x2="3.048" y2="2.54" width="0.1524" layer="94"/>
<wire x1="3.048" y1="2.54" x2="3.048" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.032" y1="-2.54" x2="2.032" y2="2.54" width="0.1524" layer="94"/>
<wire x1="5.588" y1="-2.54" x2="4.572" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="4.572" y1="2.54" x2="5.588" y2="2.54" width="0.1524" layer="94"/>
<wire x1="5.588" y1="2.54" x2="5.588" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="4.572" y1="-2.54" x2="4.572" y2="2.54" width="0.1524" layer="94"/>
<text x="-7.62" y="-5.08" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="9.525" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<text x="-5.334" y="-4.064" size="0.9906" layer="94" ratio="14">1</text>
<text x="-3.048" y="-4.064" size="0.9906" layer="94" ratio="14">2</text>
<text x="-0.508" y="-4.064" size="0.9906" layer="94" ratio="14">3</text>
<text x="2.032" y="-4.064" size="0.9906" layer="94" ratio="14">4</text>
<text x="4.699" y="-4.064" size="0.9906" layer="94" ratio="14">5</text>
<text x="-5.842" y="3.048" size="0.9906" layer="94" ratio="14">ON</text>
<rectangle x1="-5.334" y1="-2.286" x2="-4.826" y2="0" layer="94"/>
<rectangle x1="-2.794" y1="-2.286" x2="-2.286" y2="0" layer="94"/>
<rectangle x1="-0.254" y1="-2.286" x2="0.254" y2="0" layer="94"/>
<rectangle x1="2.286" y1="-2.286" x2="2.794" y2="0" layer="94"/>
<rectangle x1="4.826" y1="-2.286" x2="5.334" y2="0" layer="94"/>
<pin name="6" x="5.08" y="7.62" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="7" x="2.54" y="7.62" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="8" x="0" y="7.62" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="9" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="10" x="-5.08" y="7.62" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="1" x="-5.08" y="-7.62" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="2" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="3" x="0" y="-7.62" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="4" x="2.54" y="-7.62" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="5" x="5.08" y="-7.62" visible="pad" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="219-05*" prefix="S">
<description>&lt;b&gt;Surface Mount DIP Switch Series 219 SMT&lt;/b&gt;&lt;p&gt;
CTS Electronic Components&lt;br&gt;
Sourc: www.ctscorp.com</description>
<gates>
<gate name="G$1" symbol="DIP05" x="0" y="0"/>
</gates>
<devices>
<device name="J" package="CTS-219-05J">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="" package="CTS-219-05">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="docu-dummy">
<description>Dummy symbols</description>
<packages>
</packages>
<symbols>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="5.08" y2="0" width="0.1524" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="R" prefix="R">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-amp-quick">
<description>&lt;b&gt;AMP Connectors, Type QUICK&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="02P">
<description>&lt;b&gt;AMP QUICK CONNECTOR&lt;/b&gt;</description>
<wire x1="-2.286" y1="2.54" x2="-2.286" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.778" x2="-0.381" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="1.778" x2="-0.254" y2="1.651" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="1.651" x2="0" y2="1.397" width="0.1524" layer="21"/>
<wire x1="0" y1="1.397" x2="0.254" y2="1.651" width="0.1524" layer="21"/>
<wire x1="0.254" y1="1.651" x2="0.381" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-2.159" x2="-0.254" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-2.286" x2="-2.286" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-2.159" x2="-0.254" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-2.159" x2="0.254" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-2.286" x2="0.254" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="0.381" y1="1.778" x2="2.286" y2="1.778" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.778" x2="2.286" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.778" x2="-2.286" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.778" x2="-2.286" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.778" x2="-1.905" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.778" x2="2.286" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.778" x2="1.905" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.778" x2="2.286" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.778" x2="1.905" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.778" x2="-0.635" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.778" x2="-1.905" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.778" x2="0.635" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.524" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.778" x2="0.635" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.524" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-1.524" x2="0.635" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-1.524" x2="0.254" y2="1.651" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.524" x2="-0.254" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-1.524" x2="0.254" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-1.524" x2="-0.254" y2="1.651" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.524" x2="2.286" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.524" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.524" x2="2.286" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.524" x2="-2.286" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.524" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.524" x2="-2.286" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="2.54" x2="-1.524" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.54" x2="-1.524" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="2.54" x2="-1.016" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="2.667" x2="-1.524" y2="2.667" width="0.1524" layer="21"/>
<wire x1="1.016" y1="2.54" x2="1.016" y2="2.667" width="0.1524" layer="21"/>
<wire x1="1.524" y1="2.54" x2="2.286" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.524" y1="2.54" x2="1.524" y2="2.667" width="0.1524" layer="21"/>
<wire x1="1.524" y1="2.667" x2="1.016" y2="2.667" width="0.1524" layer="21"/>
<wire x1="0.254" y1="2.413" x2="0.254" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.254" y1="2.54" x2="1.016" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.254" y1="2.413" x2="-0.254" y2="2.413" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="2.413" x2="-0.254" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="2.54" x2="-1.016" y2="2.54" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.9144" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="0.9144" shape="long" rot="R90"/>
<text x="-0.762" y="0.9398" size="0.9906" layer="21" ratio="12" rot="R90">1</text>
<text x="-2.286" y="2.9464" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.8354" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.6002" y1="-0.3302" x2="-0.9398" y2="0.3302" layer="51"/>
<rectangle x1="0.9398" y1="-0.3302" x2="1.6002" y2="0.3302" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="M02">
<wire x1="3.81" y1="-2.54" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.54" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="M02" prefix="SL" uservalue="yes">
<description>&lt;b&gt;AMP QUICK CONNECTOR&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="M02" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="02P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="P+1" library="supply1" deviceset="VCC" device=""/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="S1" library="switch" deviceset="SKHMP*E010" device="" technology="S" value="Press to boot TF card"/>
<part name="SPDT" library="switch-dil" deviceset="219-05*" device="" value="TS3A24159"/>
<part name="R1" library="docu-dummy" deviceset="R" device=""/>
<part name="R2" library="docu-dummy" deviceset="R" device=""/>
<part name="R3" library="docu-dummy" deviceset="R" device=""/>
<part name="A9" library="con-amp-quick" deviceset="M02" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="P+1" gate="VCC" x="50.8" y="63.5"/>
<instance part="GND1" gate="1" x="68.58" y="12.7"/>
<instance part="S1" gate="G$1" x="27.94" y="27.94"/>
<instance part="SPDT" gate="G$1" x="68.58" y="45.72" rot="R270"/>
<instance part="R1" gate="G$1" x="43.18" y="50.8" rot="R180"/>
<instance part="R2" gate="G$1" x="104.14" y="48.26" rot="R180"/>
<instance part="R3" gate="G$1" x="104.14" y="45.72" rot="R180"/>
<instance part="A9" gate="G$1" x="134.62" y="48.26" rot="R180"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="68.58" y1="15.24" x2="68.58" y2="17.78" width="0.1524" layer="91"/>
<pinref part="SPDT" gate="G$1" pin="6"/>
<wire x1="68.58" y1="30.48" x2="88.9" y2="30.48" width="0.1524" layer="91"/>
<wire x1="88.9" y1="30.48" x2="88.9" y2="40.64" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="SH"/>
<wire x1="88.9" y1="40.64" x2="76.2" y2="40.64" width="0.1524" layer="91"/>
<wire x1="22.86" y1="22.86" x2="22.86" y2="17.78" width="0.1524" layer="91"/>
<wire x1="22.86" y1="17.78" x2="68.58" y2="17.78" width="0.1524" layer="91"/>
<wire x1="68.58" y1="17.78" x2="68.58" y2="30.48" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="1.2"/>
<wire x1="22.86" y1="22.86" x2="22.86" y2="25.4" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="1.1"/>
<wire x1="22.86" y1="25.4" x2="22.86" y2="27.94" width="0.1524" layer="91"/>
<pinref part="SPDT" gate="G$1" pin="5"/>
<wire x1="60.96" y1="40.64" x2="50.8" y2="40.64" width="0.1524" layer="91"/>
<wire x1="50.8" y1="40.64" x2="50.8" y2="30.48" width="0.1524" layer="91"/>
<wire x1="50.8" y1="30.48" x2="68.58" y2="30.48" width="0.1524" layer="91"/>
<pinref part="SPDT" gate="G$1" pin="10"/>
<wire x1="76.2" y1="50.8" x2="88.9" y2="50.8" width="0.1524" layer="91"/>
<wire x1="88.9" y1="50.8" x2="88.9" y2="40.64" width="0.1524" layer="91"/>
<label x="50.8" y="40.64" size="1.778" layer="95"/>
<label x="78.74" y="40.64" size="1.778" layer="95"/>
<label x="78.74" y="50.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="SPDT" gate="G$1" pin="1"/>
<pinref part="P+1" gate="VCC" pin="VCC"/>
<wire x1="60.96" y1="50.8" x2="50.8" y2="50.8" width="0.1524" layer="91"/>
<wire x1="50.8" y1="50.8" x2="50.8" y2="60.96" width="0.1524" layer="91"/>
<wire x1="50.8" y1="50.8" x2="48.26" y2="50.8" width="0.1524" layer="91"/>
<label x="53.34" y="50.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="IN" class="0">
<segment>
<pinref part="S1" gate="G$1" pin="2.1"/>
<wire x1="38.1" y1="50.8" x2="35.56" y2="50.8" width="0.1524" layer="91"/>
<wire x1="35.56" y1="50.8" x2="35.56" y2="43.18" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="2.2"/>
<wire x1="35.56" y1="43.18" x2="35.56" y2="27.94" width="0.1524" layer="91"/>
<wire x1="35.56" y1="27.94" x2="35.56" y2="25.4" width="0.1524" layer="91"/>
<pinref part="SPDT" gate="G$1" pin="4"/>
<wire x1="60.96" y1="43.18" x2="35.56" y2="43.18" width="0.1524" layer="91"/>
<pinref part="SPDT" gate="G$1" pin="8"/>
<wire x1="76.2" y1="45.72" x2="86.36" y2="45.72" width="0.1524" layer="91"/>
<wire x1="86.36" y1="45.72" x2="86.36" y2="71.12" width="0.1524" layer="91"/>
<wire x1="86.36" y1="71.12" x2="35.56" y2="71.12" width="0.1524" layer="91"/>
<wire x1="35.56" y1="71.12" x2="35.56" y2="50.8" width="0.1524" layer="91"/>
<label x="40.64" y="43.18" size="1.778" layer="95"/>
<label x="78.74" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="SPDT" gate="G$1" pin="3"/>
<wire x1="60.96" y1="45.72" x2="48.26" y2="45.72" width="0.1524" layer="91"/>
<wire x1="48.26" y1="45.72" x2="48.26" y2="27.94" width="0.1524" layer="91"/>
<wire x1="48.26" y1="27.94" x2="96.52" y2="27.94" width="0.1524" layer="91"/>
<wire x1="96.52" y1="27.94" x2="96.52" y2="45.72" width="0.1524" layer="91"/>
<wire x1="96.52" y1="45.72" x2="99.06" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="SPDT" gate="G$1" pin="9"/>
<wire x1="76.2" y1="48.26" x2="99.06" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DSS_DATA2" class="0">
<segment>
<pinref part="A9" gate="G$1" pin="1"/>
<wire x1="109.22" y1="48.26" x2="127" y2="48.26" width="0.1524" layer="91"/>
<label x="111.76" y="48.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="DSS_DATA0" class="0">
<segment>
<pinref part="A9" gate="G$1" pin="2"/>
<wire x1="109.22" y1="45.72" x2="127" y2="45.72" width="0.1524" layer="91"/>
<label x="111.76" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
