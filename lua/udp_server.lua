#!/usr/bin/env lua

-- Setup UDP socket. Bind to localhost, port 9999.
-- 'luarocks install luasocket'
-- print(package.path..'\n'..package.cpath)

local socket = require 'socket'
local udp = socket.udp()
udp:settimeout(0) -- indicates not to wait.
udp:setsockname('*', 9999)

local data        = ''
local mac         = ''
local msg_or_ip
local port_or_nil
local tempC       = ''
local tempF       = 0.0

print 'Beginning server loop'

while true do
  data, msg_or_ip, port_or_nil = udp:receivefrom() -- read UDP
  if data then
    mac, tempC = string.match(data, '(.+):(.+)')
    tempF = (tonumber(tempC)/5)*9 + 32
    print ('temp of '..mac..': '..tempF..'F')
    os.execute('echo '..tempF..'>/tmp/'..mac)
  elseif msg_or_ip ~= 'timeout' then
    error('Unknown network error: '..tostring(msg))
  end

  socket.sleep(0.01)  -- sleep .01 sec
end

