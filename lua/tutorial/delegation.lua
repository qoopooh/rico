#!/usr/bin/env lua

Point = { x=0, y=0 }
function Point:move (dx, dy)
  self.x = self.x + dx
  self.y = self.y + dy
end

function Point:new (o)
  setmetatable(o, {__index=self})
  return o
end

p = Point:new{x=5}
p:move(10,10)
print(p.x, p.y)
