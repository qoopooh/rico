#!/usr/bin/env lua

for k, v in pairs(_G) do
  if type(v) == 'string' then
    print(k..':'..v)
  else
    print(k..':'..type(v))
  end

end
