#!/usr/bin/env lua

-- Setup UDP socket. Bind to localhost, port 9999.
-- 'luarocks install luasocket'
-- print(package.path..'\n'..package.cpath)

local host = arg[1] or '192.168.60.73'
local mac = arg[2] or 'mac_address'
local temp = arg[3] or 25
local socket = require('socket')
local udp = assert(socket.udp())

assert(udp:sendto(mac..':'..temp, host, 9999))
